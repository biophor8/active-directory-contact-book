/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;
using namespace adbook;

ConnectionSettings CreateConnectionSettings()
{
    ConnectionSettings sut;
    QString domain = "domain", login = "login", passwordString = "abcd123";
    sut.ConnectAsCurrentUser( false );
    sut.ConnectCurrentDomain( false );
    sut.SetAddress( domain );
    sut.SetLogin( login );
    Password password;
    if (!password.TryToSet( passwordString )) {
        throw std::runtime_error( "password.TryToSet() failed" );
    }
    sut.SetPassword( password );
    return sut;
}

TEST(ConnectionSettingsTests, Conversion_to_AdbookConnectionParams)
{
    // Arrange
    using namespace qmladbook;
    ConnectionSettings sut;
    QString domain = "domain", login = "login", passwordString = "abcd123";
    sut.ConnectAsCurrentUser(false);
    sut.ConnectCurrentDomain(false);
    sut.SetAddress(domain);
    sut.SetLogin(login);
    Password password;
    if (!password.TryToSet( passwordString )) {
        FAIL();
    }
    sut.SetPassword(password);

    // Act
    adbook::ConnectionParams params = sut;

    // Assert
    EXPECT_EQ(params.GetAddress(), domain.toStdWString());
    EXPECT_EQ(params.GetLogin(), login.toStdWString());
    EXPECT_EQ(params.GetPassword(), passwordString.toStdWString());
    EXPECT_EQ(params.Get_ConnectAsCurrentUser(), sut.ConnectAsCurrentUser());
    EXPECT_EQ(params.Get_ConnectDomainYouLoggedIn(), sut.ConnectCurrentDomain());
}

TEST( ConnectionSettingsTests, Checking_equality )
{
    // Arrange
    ConnectionSettings sut1 = CreateConnectionSettings();
    ConnectionSettings sut2 = sut1;

    // Act & Assert
    ASSERT_EQ( (sut1 == sut2), true );
}

TEST( ConnectionSettingsTests, Checking_inequality1 )
{
    // Arrange
    ConnectionSettings sut1 = CreateConnectionSettings();
    ConnectionSettings sut2 = sut1;
    sut1.SetLogin( "login1" );
    sut2.SetLogin( "login2" );

    // Act & Assert
    ASSERT_EQ( (sut1 != sut2), true );
}

TEST( ConnectionSettingsTests, Checking_inequality2 )
{
    // Arrange
    ConnectionSettings sut1 = CreateConnectionSettings();
    sut1.SetLogin( "login1" );
    ConnectionSettings sut2 = CreateConnectionSettings();
    sut2.SetLogin( "login2" );

    // Act & Assert
    ASSERT_EQ( (sut1 != sut2), true );
}

TEST( ConnectionSettingsTests, Checking_inequality3 )
{
    // Arrange
    ConnectionSettings sut1 = CreateConnectionSettings();
    ConnectionSettings sut2 = CreateConnectionSettings();

    // These objects are different because their encrypted passwords are different.
    // Act & Assert
    ASSERT_EQ( (sut1 != sut2), true );
}


