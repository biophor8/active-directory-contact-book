/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;

TEST( SearchFilterTests, Default_constructed_SearchFilter )
{
    // Arrange
    SearchFilter sut;

    // Act & Assert
    ASSERT_EQ( sut.GetType().GetId(), SearchFilterTypeId::Invalid );
    ASSERT_EQ( sut.GetType().GetUiName().isEmpty(), true );

    ASSERT_EQ( sut.GetRule().GetId(), SearchFilterRuleId::Invalid );
    ASSERT_EQ( sut.GetRule().GetUiName().isEmpty(), true );

    ASSERT_EQ( sut.GetValue().isEmpty(), true );
}

TEST( SearchFilterTests, SearchFilter_constructed_with_valid_arguments )
{
    // Arrange
    SearchFilterType type( SearchFilterTypeId::CommonName, "common name" );
    SearchFilterRule rule( SearchFilterRuleId::ExactMatch, "exact match" );
    QString value{ "filter value" };
    SearchFilter sut(type, rule, value);

    // Act & Assert
    ASSERT_EQ( sut.GetRule(), rule );
    ASSERT_EQ( sut.GetType(), type );
    ASSERT_EQ( sut.GetValue(), value );
}

TEST( SearchFilterTests, Compairing_identical_SearchFilters )
{
    // Arrange
    SearchFilterType type( SearchFilterTypeId::CommonName, "common name" );
    SearchFilterRule rule( SearchFilterRuleId::ExactMatch, "exact match" );
    QString value{ "filter value" };
    SearchFilter sut1( type, rule, value );
    SearchFilter sut2( type, rule, value );

    // Act & Assert
    ASSERT_EQ( sut1, sut2 );
}

TEST( SearchFilterTests, Compairing_different_SearchFilters )
{
    // Arrange
    SearchFilterType type( SearchFilterTypeId::CommonName, "common name" );
    SearchFilterType type2( SearchFilterTypeId::DisplayName, "display name" );
    SearchFilterRule rule( SearchFilterRuleId::ExactMatch, "exact match" );
    SearchFilterRule rule2( SearchFilterRuleId::Contains, "contains" );
    QString value{ "filter value" };
    QString value2{ "another filter value" };
    SearchFilter sut1( type, rule, value );
    SearchFilter sut2( type2, rule, value );
    SearchFilter sut3( type, rule2, value );
    SearchFilter sut4( type, rule, value2);

    // Act & Assert
    ASSERT_NE( sut1, sut2 );
    ASSERT_NE( sut1, sut3 );
    ASSERT_NE( sut1, sut4 );
}

