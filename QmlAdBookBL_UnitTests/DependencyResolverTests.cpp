/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;
using namespace adbook;

using namespace qmladbook;

class DependencyResolverTests : public ::testing::Test
{
public:
    void SetUp() override {
        CoInitialize( nullptr );
    }
    void TearDown() override {
        CoUninitialize();
    }
};


TEST_F(DependencyResolverTests, Test)
{
    // Arrange
    DependencyResolver sut;

    // Act & Assert
    auto adFactory = sut.GetAdFactory();
    ASSERT_NE( adFactory, nullptr );
    auto connector = adFactory->CreateConnector();
    ASSERT_NE( connector, nullptr );
    auto searcher = adFactory->CreateSearcher();
    ASSERT_NE( searcher, nullptr );

    ASSERT_NE( sut.GetClipboardService(), nullptr );
    ASSERT_NE( sut.GetContactFinder(), nullptr );

    // The QGuiApplication object must be constructed before calling this method
    // I don't know how to create this object in the test environment
    //AContactPhotoProvider * contactPhotoProvider = sut.GetContactPhotoProvider();   // returns raw pointer.
    //ASSERT_NE( contactPhotoProvider, nullptr );
    //delete contactPhotoProvider;

    ASSERT_NE( sut.GetContactsKeeper(), nullptr );
    ASSERT_NE( sut.GetContactUpdater(), nullptr );
    ASSERT_NE( sut.GetDataExchanger(), nullptr );
    ASSERT_NE( sut.GetFileSystemService(), nullptr );
    ASSERT_NE( sut.GetSettingsChecker(), nullptr );
    ASSERT_NE( sut.GetSettingsKeeper(), nullptr );
}
