/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"
#include "TestSuiteBase.h"

using namespace qmladbook;

class AttrEditorViewModelTests : public TestSuiteBase
{
public:
    void SetUp() override {
        CoInitialize( nullptr );
    }

    void TearDown() override {
        CoUninitialize();
    }
};

TEST_F(AttrEditorViewModelTests, Loading_data_from_ContactDetailsModel)
{
    // Arrange
    AttrEditorViewModel sut;
    ContactDetailsModel contactDetailsModel;
    Contact contact = CreateContactForTesting();
    contactDetailsModel.SetContact( contact );

    const int cdmRowCount = contactDetailsModel.rowCount();

    for (int cdmI = 0; cdmI < cdmRowCount; ++cdmI)
    {
        // Act
        sut.load( &contactDetailsModel, cdmI );

        // Assert
        ASSERT_EQ( sut.currentValue(), contactDetailsModel.GetAttrValue( cdmI ) );
        ASSERT_EQ( sut.uiName(), contactDetailsModel.GetAttrUiName( cdmI ) );
        ASSERT_FALSE( sut.ldapName().isEmpty() );
        ASSERT_FALSE( sut.ownerDn().isEmpty() );
        ASSERT_FALSE( sut.attributeOid().isEmpty() );
        ASSERT_NE( sut.maxLength(), 0 );
        ASSERT_FALSE( sut.attrDescription().isEmpty() );
        ASSERT_EQ( sut.isCommonName(), (contactDetailsModel.GetAttrId( cdmI ) == adbook::Attributes::CommonName) );
    }
}

TEST_F( AttrEditorViewModelTests, Emit_notification_signal_when_currentValue_property_is_changed )
{
    // Arrange
    AttrEditorViewModel sut;

    bool currentValueChangedSignalEmited = false;
    QObject::connect( &sut, &AttrEditorViewModel::currentValueChanged,
        [&currentValueChangedSignalEmited] {currentValueChangedSignalEmited = true; }
    );

    // Act
    sut.setCurrentValue( "new value" );

    // Assert
    ASSERT_EQ( currentValueChangedSignalEmited, true );
}

TEST_F( AttrEditorViewModelTests, Emit_notification_signal_when_newValue_property_is_changed )
{
    // Arrange
    AttrEditorViewModel sut;

    bool newValueChangedSignalEmited = false;
    QObject::connect( &sut, &AttrEditorViewModel::newValueChanged,
        [&newValueChangedSignalEmited] {newValueChangedSignalEmited = true; }
    );

    // Act
    sut.setNewValue( "new value" );

    // Assert
    ASSERT_EQ( newValueChangedSignalEmited, true );
}

TEST_F( AttrEditorViewModelTests, Emit_notification_signal_when_ldapName_property_is_changed )
{
    // Arrange
    AttrEditorViewModel sut;

    bool ldapNameChangedSignalEmited = false;
    QObject::connect( &sut, &AttrEditorViewModel::ldapNameChanged,
        [&ldapNameChangedSignalEmited] {ldapNameChangedSignalEmited = true; }
    );

    // Act
    sut.setLdapName( "new value" );

    // Assert
    ASSERT_EQ( ldapNameChangedSignalEmited, true );
}

TEST_F( AttrEditorViewModelTests, Emit_notification_signal_when_ownerDn_property_is_changed )
{
    // Arrange
    AttrEditorViewModel sut;

    bool ownerDnChangedSignalEmited = false;
    QObject::connect( &sut, &AttrEditorViewModel::ownerDnChanged,
        [&ownerDnChangedSignalEmited] {ownerDnChangedSignalEmited = true; }
    );

    // Act
    sut.setOwnerDn( "new value" );

    // Assert
    ASSERT_EQ( ownerDnChangedSignalEmited, true );
}

TEST_F( AttrEditorViewModelTests, Emit_notification_signal_when_uiName_property_is_changed )
{
    // Arrange
    AttrEditorViewModel sut;

    bool uiNameChangedSignalEmited = false;
    QObject::connect( &sut, &AttrEditorViewModel::uiNameChanged,
        [&uiNameChangedSignalEmited] {uiNameChangedSignalEmited = true; }
    );

    // Act
    sut.setUiName( "new value" );

    // Assert
    ASSERT_EQ( uiNameChangedSignalEmited, true );
}

TEST_F( AttrEditorViewModelTests, Emit_notification_signal_when_attributeOid_property_is_changed )
{
    // Arrange
    AttrEditorViewModel sut;

    bool attributeOidChangedSignalEmited = false;
    QObject::connect( &sut, &AttrEditorViewModel::attributeOidChanged,
        [&attributeOidChangedSignalEmited] {attributeOidChangedSignalEmited = true; }
    );

    // Act
    sut.setAttributeOid( "new value" );

    // Assert
    ASSERT_EQ( attributeOidChangedSignalEmited, true );
}

TEST_F( AttrEditorViewModelTests, Emit_notification_signal_when_attrDescription_property_is_changed )
{
    // Arrange
    AttrEditorViewModel sut;

    size_t attrDescriptionChangedSignalEmited = 0;
    QObject::connect( &sut, &AttrEditorViewModel::attrDescriptionChanged,
        [&attrDescriptionChangedSignalEmited] { ++attrDescriptionChangedSignalEmited; }
    );

    // Act
    sut.setLdapName( "ldap name" );
    sut.setUiName( "ui name" );
    sut.setAttributeOid( "1.2.3.4" );

    // Assert
    ASSERT_EQ( attrDescriptionChangedSignalEmited, 3 );
}

TEST_F( AttrEditorViewModelTests, Emit_notification_signal_when_isCommonName_property_is_changed )
{
    // Arrange
    AttrEditorViewModel sut;

    bool isCommonNameChangedSignalEmited = false;
    QObject::connect( &sut, &AttrEditorViewModel::isCommonNameChanged,
        [&isCommonNameChangedSignalEmited] {isCommonNameChangedSignalEmited = true; }
    );

    // Act
    ContactDetailsModel contactDetailsModel;
    Contact contact = CreateContactForTesting();
    contactDetailsModel.SetContact( contact );

    // Act
    // the signal isCommonNameChanged is emited when AttrEditorViewModel is loaded from ContactDetailsModel
    sut.load( &contactDetailsModel, 0 );

    // Assert
    ASSERT_EQ( isCommonNameChangedSignalEmited, true );
}

