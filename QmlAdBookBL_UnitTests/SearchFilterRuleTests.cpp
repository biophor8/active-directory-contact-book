/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;

TEST( SearchFilterRulesTests, Default_constructed_SearchFilterType )
{
    // Arrange
    SearchFilterRule sut;

    // Act & Assert
    ASSERT_EQ( sut.GetId(), SearchFilterRuleId::Invalid );
    ASSERT_EQ( sut.GetUiName().isEmpty(), true );
}

TEST( SearchFilterRulesTests, Construction_with_valid_arguments )
{
    // Arrange
    QString uiName{ "exact match" };
    SearchFilterRule sut( SearchFilterRuleId::ExactMatch, uiName );

    // Act & Assert
    ASSERT_EQ( sut.GetId(), SearchFilterRuleId::ExactMatch );
    ASSERT_EQ( sut.GetUiName(), uiName );
}

TEST( SearchFilterRulesTests, Construction_with_empty_display_name_throws_exception )
{
    // Arrange & Act & Assert
    QString emptyUiName;
    ASSERT_ANY_THROW( SearchFilterRule( SearchFilterRuleId::ExactMatch, emptyUiName ) );
}

TEST( SearchFilterRulesTests, SearchFilterRules_with_identical_ids_are_considered_equal )
{
    // Arrange
    SearchFilterRule sut1( SearchFilterRuleId::ExactMatch, "exact match" );
    SearchFilterRule sut2( SearchFilterRuleId::ExactMatch, "ExactMatch" );

    // Act & Assert
    ASSERT_EQ( sut1.GetId(), sut2.GetId() );
}



