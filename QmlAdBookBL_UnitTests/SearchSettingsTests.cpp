/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;

TEST( SearchSettingsTests, Checking_equality )
{
    // Arrange
    SearchSettings sut1, sut2;
    QByteArray serializedFilter{ "Serialized filters test replacement" };
    sut1.AllFilteringRulesMustBeMet( true );
    sut1.EnableSearchFilters( true );
    sut1.SetSerializedFilters( serializedFilter );

    sut2.AllFilteringRulesMustBeMet( true );
    sut2.EnableSearchFilters( true );
    sut2.SetSerializedFilters( serializedFilter );

    // Act
    bool equal = sut1 == sut2;

    // Assert
    ASSERT_EQ( equal, true );
}

TEST( SearchSettingsTests, Checking_inequality )
{
    // Arrange
    SearchSettings sut1, sut2, sut3, sut4;
    QByteArray serializedFilter{ "Serialized filters test replacement" };
    sut1.AllFilteringRulesMustBeMet( true );
    sut1.EnableSearchFilters( true );
    sut1.SetSerializedFilters( serializedFilter );

    sut2.AllFilteringRulesMustBeMet( false );
    sut2.EnableSearchFilters( true );
    sut2.SetSerializedFilters( serializedFilter );

    sut3.AllFilteringRulesMustBeMet( true );
    sut3.EnableSearchFilters( false );
    sut3.SetSerializedFilters( serializedFilter );

    sut4.AllFilteringRulesMustBeMet( true );
    sut4.EnableSearchFilters( true );
    sut4.SetSerializedFilters( "" );

    // Act && Assert
    ASSERT_EQ( (sut1 == sut2), false );
    ASSERT_EQ( (sut1 == sut3), false );
    ASSERT_EQ( (sut1 == sut4), false );
}

