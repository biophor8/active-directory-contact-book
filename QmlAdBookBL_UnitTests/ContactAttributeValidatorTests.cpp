/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;

class ContactAttributeValidatorTests : public ::testing::Test
{
public:
    void SetUp() override {
        CoInitialize( nullptr );
    }
    void TearDown() override {
        CoUninitialize();
    }
};

TEST_F( ContactAttributeValidatorTests, Checking_valid_value)
{
    // Arrange
    ContactAttributeValidator sut;
    QString validCommonName = "John Dow";

    // Act
    AttributeCheckResult checkResult = sut.CheckBaseConstraints( adbook::Attributes::CommonName, validCommonName );

    // Assert
    ASSERT_EQ( checkResult, AttributeCheckResult::Ok );
}

TEST_F( ContactAttributeValidatorTests, Checking_value_with_invalid_format )
{
    // Arrange
    ContactAttributeValidator sut;
    QString invalidCommonName = "/John Dow";

    // Act
    AttributeCheckResult checkResult = sut.CheckBaseConstraints( adbook::Attributes::CommonName, invalidCommonName );

    // Assert
    ASSERT_EQ( checkResult, AttributeCheckResult::InvalidFormat );
}

TEST_F( ContactAttributeValidatorTests, Checking_empty_common_name )
{
    // Arrange
    ContactAttributeValidator sut;
    QString emptyCommonName = "";

    // Act
    AttributeCheckResult checkResult = sut.CheckBaseConstraints( adbook::Attributes::CommonName, emptyCommonName );

    // Assert
    ASSERT_EQ( checkResult, AttributeCheckResult::Empty );
}

TEST_F( ContactAttributeValidatorTests, Checking_too_big_value )
{
    // Arrange
    ContactAttributeValidator sut;
    QString tooBigGivenName = QString(65, '$');

    // Act
    AttributeCheckResult checkResult = sut.CheckBaseConstraints( adbook::Attributes::GivenName, tooBigGivenName );

    // Assert
    ASSERT_EQ( checkResult, AttributeCheckResult::TooBig );
}

