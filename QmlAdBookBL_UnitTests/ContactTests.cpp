/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;
using namespace adbook;

bool AreContactAndAdPersonEqual( const Contact & contact, const AdPersonDesc & adp )
{
    auto & attrTraits = Attributes::GetInstance();
    for (auto id : attrTraits.GetAttrIds()) {
        if (attrTraits.IsString( id )) {
            QString contactAttrValue = std::get<QString>( contact.GetAttr( id ) );
            const wchar_t * adpAttrValue = adp.GetStringAttrPtr( attrTraits.GetLdapAttrNamePtr( id ) );
            if (contactAttrValue != adpAttrValue) {
                return false;
            }
        }
        else {
            BinaryAttrVal contactAttrValue = std::get<BinaryAttrVal>( contact.GetAttr( id ) );
            BinaryAttrVal adpAttrValue = adp.GetBinaryAttr( attrTraits.GetLdapAttrNamePtr( id ) );
            if (contactAttrValue != adpAttrValue) {
                return false;
            }
        }
    }
    return true;
}

AdPersonDesc CreateAdPersonForTesting()
{
    AdPersonDesc adp;
    auto & attrTraits = Attributes::GetInstance();

    for (auto id : attrTraits.GetAttrIds()) {
        if (attrTraits.IsString( id )) {
            QString stringAttrValue = QString( "attr value %1" ).arg( id );
            adp.SetStringAttr( attrTraits.GetLdapAttrNamePtr( id ), stringAttrValue.toStdWString() );
        }
        else {
            BinaryAttrVal binaryAttrValue{ id };
            adp.SetBinaryAttr( attrTraits.GetLdapAttrNamePtr( id ), binaryAttrValue );
        }
    }
    return adp;
}

Contact CreateContactForTesting()
{
    Contact contact;
    auto & attrTraits = Attributes::GetInstance();
    auto attrIds = attrTraits.GetAttrIds();
    for (auto id : attrIds) {
        if (attrTraits.IsString( id )) {
            QString stringAttrValue = QString( "attr value %1" ).arg( id );
            contact.SetAttr( id, stringAttrValue );
        }
        else {
            BinaryAttrVal binaryAttrValue{ id };
            contact.SetAttr( id, binaryAttrValue );
        }
    }
    return contact;
}

TEST( ContactTests, Contact_can_be_constructed_from_an_existing_AdPerson )
{
    // Arrange
    AdPersonDesc adp = CreateAdPersonForTesting();

    // Act
    Contact contact( adp );

    // Assert
    ASSERT_EQ( AreContactAndAdPersonEqual( contact, adp ), true );
}

TEST( ContactTests, Contact_can_be_converted_to_AdPerson )
{
    // Arrange
    Contact contact = CreateContactForTesting();

    // Act
    AdPersonDesc adp = static_cast<AdPersonDesc>(contact);

    // Assert
    ASSERT_EQ( AreContactAndAdPersonEqual( contact, adp ), true );
}

TEST( ContactTests, Reading_and_writing_all_AdBookAttributes )
{
    // Arrange
    Contact contact;
    auto & attrTraits = Attributes::GetInstance();
    auto attrIds = attrTraits.GetAttrIds();

    // Act
    for (auto id : attrIds)
    {
        if (attrTraits.IsString( id ))
        {
            QString valueToWrite = QString( "attr value %1" ).arg( id );
            contact.SetAttr( id, valueToWrite );
            QString readValue = contact.GetAttrAsString( id );
            // Assert
            EXPECT_EQ( valueToWrite, readValue );
        }
        else
        {
            BinaryAttrVal valueToWrite{ id };
            contact.SetAttr( id, valueToWrite );
            BinaryAttrVal readValue = contact.GetAttrAsBinary( id );
            // Assert
            EXPECT_EQ( valueToWrite, readValue );
        }
    }
}


