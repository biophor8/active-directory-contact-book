/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"
#include "TestSuiteBase.h"

using namespace qmladbook;

TEST( LdapRequestCreatorTests, No_rules )
{
    // Arrange
    QVector<SearchFilter> filters;

    // Act
    QString request = LdapRequestCreator::Create( filters, true );
    QString controlValue = "(objectCategory=person)";

    // Assert
    ASSERT_EQ( request, controlValue );

}

TEST( LdapRequestCreatorTests, One_simple_rule )
{
    // Arrange
    QVector<SearchFilter> filters;
    SearchFilter sf;
    sf.SetFilterType( SearchFilterType( SearchFilterTypeId::Email, "Email" ) );
    sf.SetFilterRule( SearchFilterRule( SearchFilterRuleId::Contains, "contains" ) );
    sf.SetValue( "msft.com" );
    filters.push_back( sf );

    // Act
    QString request = LdapRequestCreator::Create( filters, true );
    QString controlValue = "(&(mail=*msft.com*)(objectCategory=person))";

    // Assert
    ASSERT_EQ( request, controlValue );
}


TEST( LdapRequestCreatorTests, Two_simple_rules )
{
    // Arrange
    QVector<SearchFilter> filters;
    SearchFilter sf;
    sf.SetFilterType( SearchFilterType( SearchFilterTypeId::Company, "company" ) );
    sf.SetFilterRule( SearchFilterRule( SearchFilterRuleId::EndsWith, "exact match" ) );
    sf.SetValue( "MSFT" );
    filters.push_back( sf );

    sf.SetFilterType( SearchFilterType( SearchFilterTypeId::EmployeeID, "empid" ) );
    sf.SetFilterRule( SearchFilterRule( SearchFilterRuleId::ExactMatch, "exact match" ) );
    sf.SetValue( "1234" );
    filters.push_back( sf );

    // Act
    QString request = LdapRequestCreator::Create( filters, true );
    QString controlValue = "(&(&(company=*MSFT)(employeeID=1234))(objectCategory=person))";

    // Assert
    ASSERT_EQ( request, controlValue );
}

TEST( LdapRequestCreatorTests, One_group_rule )
{
    // Arrange
    QVector<SearchFilter> filters;
    SearchFilter sf;
    sf.SetFilterType( SearchFilterType( SearchFilterTypeId::Names, "names" ) );
    sf.SetFilterRule( SearchFilterRule( SearchFilterRuleId::ExactMatch, "exact match" ) );
    sf.SetValue( "John Wick" );
    filters.push_back( sf );

    // Act
    QString request = LdapRequestCreator::Create( filters, true );
    QString controlValue = "(&(|(cn=John Wick)(displayName=John Wick)(givenName=John Wick)(sn=John Wick))(objectCategory=person))";

    // Assert
    ASSERT_EQ( request, controlValue );
}

TEST( LdapRequestCreatorTests, One_simple_rule_and_one_group_rule )
{
    // Arrange
    QVector<SearchFilter> filters;
    SearchFilter sf;

    sf.SetFilterType( SearchFilterType( SearchFilterTypeId::Company, "company" ) );
    sf.SetFilterRule( SearchFilterRule( SearchFilterRuleId::ExactMatch, "exact match" ) );
    sf.SetValue( "Google" );
    filters.push_back( sf );

    sf.SetFilterType( SearchFilterType( SearchFilterTypeId::Names, "names" ) );
    sf.SetFilterRule( SearchFilterRule( SearchFilterRuleId::ExactMatch, "exact match" ) );
    sf.SetValue( "John Wick" );
    filters.push_back( sf );

    // Act
    QString request = LdapRequestCreator::Create( filters, true );
    QString controlValue = "(&(&(company=Google)(|(cn=John Wick)(displayName=John Wick)(givenName=John Wick)(sn=John Wick)))(objectCategory=person))";

    // Assert
    ASSERT_EQ( request, controlValue );
}


TEST(LdapRequestCreatorTests, Two_simple_rules_and_two_group_rules )
{
    // Arrange
    QVector<SearchFilter> filters;
    SearchFilter sf;
    sf.SetFilterType( SearchFilterType( SearchFilterTypeId::Company, "company" ) );
    sf.SetFilterRule( SearchFilterRule( SearchFilterRuleId::ExactMatch, "exact match" ) );
    sf.SetValue( "MSFT" );
    filters.push_back( sf );

    sf.SetFilterType( SearchFilterType( SearchFilterTypeId::Names, "names" ) );
    sf.SetFilterRule( SearchFilterRule( SearchFilterRuleId::ExactMatch, "exact match" ) );
    sf.SetValue( "John Wick" );
    filters.push_back( sf );

    sf.SetFilterType( SearchFilterType( SearchFilterTypeId::Phones, "phones" ) );
    sf.SetFilterRule( SearchFilterRule( SearchFilterRuleId::BeginsWith, "begins with" ) );
    sf.SetValue( "555" );
    filters.push_back( sf );

    sf.SetFilterType( SearchFilterType( SearchFilterTypeId::EmployeeID, "empid" ) );
    sf.SetFilterRule( SearchFilterRule( SearchFilterRuleId::ExactMatch, "exact match" ) );
    sf.SetValue( "1234" );
    filters.push_back( sf );

    // Act
    QString request = LdapRequestCreator::Create( filters, true );
    QString controlValue = "(&(&(company=MSFT)(|(cn=John Wick)(displayName=John Wick)(givenName=John Wick)(sn=John Wick))(|(homePhone=555*)(mobile=555*)(telephoneNumber=555*))(employeeID=1234))(objectCategory=person))";

    // Assert
    ASSERT_EQ( request, controlValue );

}
