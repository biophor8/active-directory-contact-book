/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;

TEST( SettingsDlgViewModelTests, Emit_notification_signal_when_connectCurrentDomain_property_is_changed )
{
    // Arrange
    SettingsDlgViewModel sut;

    bool connectCurrentDomainChangedSignalEmited = false;
    bool b = QObject::connect( &sut, &SettingsDlgViewModel::connectCurrentDomainChanged,
        [&connectCurrentDomainChangedSignalEmited] {connectCurrentDomainChangedSignalEmited = true; }
    );

    // Act
    sut.setConnectCurrentDomain( false );

    // Assert
    ASSERT_EQ( connectCurrentDomainChangedSignalEmited, true );
}

TEST( SettingsDlgViewModelTests, Emit_notification_signal_when_connectAsCurrentUser_property_is_changed )
{
    // Arrange
    SettingsDlgViewModel sut;

    bool connectAsCurrentUserChangedSignalEmited = false;
    bool b = QObject::connect( &sut, &SettingsDlgViewModel::connectAsCurrentUserChanged,
        [&connectAsCurrentUserChangedSignalEmited] {connectAsCurrentUserChangedSignalEmited = true; }
    );

    // Act
    sut.setConnectAsCurrentUser( false );

    // Assert
    ASSERT_EQ( connectAsCurrentUserChangedSignalEmited, true );
}

TEST( SettingsDlgViewModelTests, Emit_notification_signal_when_address_property_is_changed )
{
    // Arrange
    SettingsDlgViewModel sut;

    bool addressChangedSignalEmited = false;
    bool b = QObject::connect( &sut, &SettingsDlgViewModel::addressChanged,
        [&addressChangedSignalEmited] {addressChangedSignalEmited = true; }
    );

    // Act
    sut.setAddress( "address" );

    // Assert
    ASSERT_EQ( addressChangedSignalEmited, true );
}

TEST( SettingsDlgViewModelTests, Emit_notification_signal_when_login_property_is_changed )
{
    // Arrange
    SettingsDlgViewModel sut;

    bool loginChangedSignalEmited = false;
    bool b = QObject::connect( &sut, &SettingsDlgViewModel::loginChanged,
        [&loginChangedSignalEmited] {loginChangedSignalEmited = true; }
    );

    // Act
    sut.setLogin( "login" );

    // Assert
    ASSERT_EQ( loginChangedSignalEmited, true );
}

TEST( SettingsDlgViewModelTests, Emit_notification_signal_when_password_property_is_changed )
{
    // Arrange
    SettingsDlgViewModel sut;

    bool passwordChangedSignalEmited = false;
    bool b = QObject::connect( &sut, &SettingsDlgViewModel::passwordChanged,
        [&passwordChangedSignalEmited] {passwordChangedSignalEmited = true; }
    );

    // Act
    sut.setPassword( "password" );

    // Assert
    ASSERT_EQ( passwordChangedSignalEmited, true );
}

