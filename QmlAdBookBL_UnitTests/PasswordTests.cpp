/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;

class PasswordTests : public ::testing::Test {
public:
    void SetUp() override {
        CoInitialize(nullptr);
    }
    void TearDown() override {
        CoUninitialize();
    }
};

TEST_F(PasswordTests, Decrypted_password_must_be_equal_to_initial_password)
{
    // Arrange
    QString plainTextPassword1 = "Passw0rd";
    QString plainTextPassword2;
    Password sut1;
    Password sut2;

    // Act
    bool passwordSet = sut1.TryToSet(plainTextPassword1);
    QString encryptedSerializedPassword = sut1.Serialize();

    sut2.Deserialize(encryptedSerializedPassword);
    bool passwordGot = sut2.TryToGetAsPlainTextString(plainTextPassword2);

    // Assert
    ASSERT_TRUE(passwordSet);
    ASSERT_TRUE(passwordGot);
    ASSERT_EQ(plainTextPassword1, plainTextPassword2);
}


