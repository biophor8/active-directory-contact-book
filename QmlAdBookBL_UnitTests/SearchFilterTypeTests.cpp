/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;

TEST( SearchFilterTypeTests, Default_constructed_SearchFilterType )
{
    // Arrange
    SearchFilterType sut;

    // Act & Assert
    ASSERT_EQ( sut.GetId(), SearchFilterTypeId::Invalid );
    ASSERT_EQ( sut.GetUiName().isEmpty(), true );
}

TEST( SearchFilterTypeTests, Construction_with_valid_arguments )
{
    // Arrange
    QString uiName{ "common name" };
    SearchFilterType sut( SearchFilterTypeId::CommonName, uiName );

    // Act & Assert
    ASSERT_EQ( sut.GetId(), SearchFilterTypeId::CommonName );
    ASSERT_EQ( sut.GetUiName(), uiName );
}

TEST( SearchFilterTypeTests, Construction_with_empty_display_name_throws_exception )
{
    // Arrange & Act & Assert
    QString emptyUiName;
    ASSERT_ANY_THROW( SearchFilterType( SearchFilterTypeId::CommonName, emptyUiName ) );
}

TEST( SearchFilterTypeTests, SearchFilterTypes_with_identical_ids_are_considered_equal )
{
    // Arrange
    SearchFilterType sut1( SearchFilterTypeId::CommonName, "common name");
    SearchFilterType sut2( SearchFilterTypeId::CommonName, "CommonName" );

    // Act & Assert
    ASSERT_EQ( sut1.GetId(), sut2.GetId() );
}



