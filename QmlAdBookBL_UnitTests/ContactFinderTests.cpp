#include "gtest/gtest.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"
#include "TestSuiteBase.h"

using namespace qmladbook;
using namespace adbook;

class ContactFinderTests : public TestSuiteBase {
public:
    void SetUp() override {
        CoInitialize( nullptr );
    }
    void TearDown() override {
        CoUninitialize();
    }
};

TEST_F(ContactFinderTests, Connecting_notification_signals)
{
    // Arrange
    auto resolver = qmladbook::GetDependencyResolver();
    ContactFinder sut(resolver->GetAdFactory());

    // Act
    auto contactFoundCallback = []() { };
    bool contactFoundConnected = QObject::connect(&sut, &ContactFinder::contactFound,
        contactFoundCallback
    );
    auto searchStartedCallback = []() { };
    bool searchStartedConnected = QObject::connect(&sut, &ContactFinder::searchStarted,
        searchStartedCallback
    );
    auto searchFinishedCallback = []() { };
    bool searchFinishedConnected = QObject::connect(&sut, &ContactFinder::searchFinished,
        searchFinishedCallback
    );
    auto errorOccurredCallback = [](QString) { };
    bool errorOccurredConnected = QObject::connect(&sut, &ContactFinder::errorOccurred,
        errorOccurredCallback
    );

    // Assert
    bool allSignalsConnected = contactFoundConnected && searchStartedConnected && searchFinishedConnected  &&
        errorOccurredConnected;

    ASSERT_TRUE(allSignalsConnected);
}

