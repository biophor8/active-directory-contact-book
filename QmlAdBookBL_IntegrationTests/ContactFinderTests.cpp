/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"
#include "TestSuiteBase.h"

using namespace adbook;
using namespace qmladbook;

class ContactFinderTests : public TestSuiteBase { };

TEST_F( ContactFinderTests, Checking_notification_callbacks )
{
    // Arrange
    auto resolver = GetDependencyResolver();
    ContactFinder sut( resolver->GetAdFactory() );

    LdapRequestBuilder requestBuilder;
    requestBuilder.AddObjectCategoryRule();
    requestBuilder.AddRule( L"cn", LdapRequestBuilder::ExactMatch, L"Ramon Linden" );
    requestBuilder.AddAND();
    ConnectionSettings connectionSettings = GetConnectionSettingsForTestAdLdsInstance();

    bool startedCallbackCalled = false;
    bool finishedCallbackCalled = false;
    bool errorOccurredCallbackCalled = false;
    bool contactFoundCallbackCalled = false;

    auto contactFoundCallback = [&contactFoundCallbackCalled]() { contactFoundCallbackCalled = true; };
    QObject::connect( &sut, &ContactFinder::contactFound, contactFoundCallback );

    auto searchStartedCallback = [&startedCallbackCalled]() { startedCallbackCalled = true; };
    QObject::connect( &sut, &ContactFinder::searchStarted, searchStartedCallback );

    auto searchFinishedCallback = [&finishedCallbackCalled]() { finishedCallbackCalled = true; };
    QObject::connect( &sut, &ContactFinder::searchFinished, searchFinishedCallback );

    auto errorOccurredCallback = [&errorOccurredCallbackCalled]( QString ) { errorOccurredCallbackCalled = true; };
    QObject::connect( &sut, &ContactFinder::errorOccurred, errorOccurredCallback );

    QString ldapRequest = QString::fromStdWString( requestBuilder.Get() );

    // Act
    sut.Start( ldapRequest, connectionSettings );
    sut.Wait();

    // Assert
    ASSERT_NO_THROW( sut.PropogateLastSearchException() );
    ASSERT_TRUE( startedCallbackCalled );
    ASSERT_TRUE( finishedCallbackCalled );
    ASSERT_FALSE( errorOccurredCallbackCalled );
    ASSERT_TRUE( contactFoundCallbackCalled );
}

TEST_F(ContactFinderTests, Searching_for_existing_Contact)
{
    // Arrange
    LdapRequestBuilder requestBuilder;
    requestBuilder.AddObjectCategoryRule();
    requestBuilder.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"Ramon Linden");
    requestBuilder.AddAND();
    ConnectionSettings connectionSettings = GetConnectionSettingsForTestAdLdsInstance();

    auto resolver = GetDependencyResolver();
    ContactFinder sut(resolver->GetAdFactory());

    bool errorOccurredCallbackCalled = false;
    size_t numContactsFound = 0;
    bool ramonLindenFound = false;

    auto contactFoundCallback = [&]() {
        ++numContactsFound;
        if (auto contacts = sut.Peek(); !contacts.empty()) {
            if (contacts.front().GetAttrAsString( Attributes::CommonName ) == L"Ramon Linden") {
                ramonLindenFound = true;
            }
        }
    };
    QObject::connect(&sut, &ContactFinder::contactFound, contactFoundCallback );

    auto errorOccurredCallback = [&errorOccurredCallbackCalled](QString ) { errorOccurredCallbackCalled = true; };
    QObject::connect(&sut, &ContactFinder::errorOccurred, errorOccurredCallback );

    QString ldapRequest = QString::fromStdWString(requestBuilder.Get());

    // Act
    sut.Start(ldapRequest, connectionSettings);
    sut.Wait();

    // Assert
    ASSERT_NO_THROW(sut.PropogateLastSearchException());
    ASSERT_TRUE( ramonLindenFound );
    ASSERT_EQ( numContactsFound, 1 );
    ASSERT_FALSE(errorOccurredCallbackCalled);
}

TEST_F(ContactFinderTests, Searching_finds_nothing_when_specified_Contact_doesnt_exist)
{
    // Arrange
    LdapRequestBuilder requestBuilder;
    requestBuilder.AddObjectCategoryRule();
    requestBuilder.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"Invalid Person");
    requestBuilder.AddAND();
    ConnectionSettings connectionSettings = GetConnectionSettingsForTestAdLdsInstance();
    ContactFinder sut(GetDependencyResolver()->GetAdFactory());

    bool errorOccurredCallbackCalled = false;
    size_t numContactsFound = 0;

    auto contactFoundCallback = [&numContactsFound]() { ++numContactsFound; };
    QObject::connect(&sut, &ContactFinder::contactFound, contactFoundCallback );

    auto errorOccurredCallback = [&errorOccurredCallbackCalled](QString) { errorOccurredCallbackCalled = true; };
    QObject::connect(&sut, &ContactFinder::errorOccurred, errorOccurredCallback );

    QString ldapRequest = QString::fromStdWString(requestBuilder.Get());

    // Act
    sut.Start(ldapRequest, connectionSettings);
    sut.Wait();

    // Assert
    ASSERT_NO_THROW(sut.PropogateLastSearchException());
    ASSERT_FALSE(errorOccurredCallbackCalled);
    ASSERT_EQ(numContactsFound, 0);
}

TEST_F(ContactFinderTests, Throws_exception_when_invalid_search_filter_passed_in)
{
    // Arrange
    auto resolver = GetDependencyResolver();
    ContactFinder sut( resolver->GetAdFactory() );

    ConnectionSettings connectionSettings = GetConnectionSettingsForTestAdLdsInstance();

    bool errorOccurredCallbackCalled = false;
    size_t numContactsFound = 0;

    auto contactFoundCallback = [&numContactsFound]() { ++numContactsFound; };
    QObject::connect(&sut, &ContactFinder::contactFound, contactFoundCallback );

    auto errorOccurredCallback = [&errorOccurredCallbackCalled](QString) { errorOccurredCallbackCalled = true; };
    QObject::connect(&sut, &ContactFinder::errorOccurred, errorOccurredCallback );

    QString ldapRequest = "INVALID SEARCH FILTER";

    // Act
    sut.Start(ldapRequest, connectionSettings);
    sut.Wait();

    // Assert
    ASSERT_THROW(sut.PropogateLastSearchException(), HrError);
    ASSERT_TRUE(errorOccurredCallbackCalled);
    ASSERT_EQ(numContactsFound, 0);
}

