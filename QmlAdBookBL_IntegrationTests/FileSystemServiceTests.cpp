/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/
#include <QDir>
#include <QFile>
#include <QFileInfo>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;

class FileSystemServiceTests : public ::testing::Test
{
    void SetUp() override {

    }
    void TearDown() override {
    }

};

TEST_F( FileSystemServiceTests, Reading_the_file )
{
    // Arrange
    QString fileName = "tmp";
    QByteArray data = "bla";
    FileSystemService sut;
    QFile file( fileName );
    file.open(QIODevice::WriteOnly );
    file.write( data );
    QFileInfo fileInfo( file );
    file.close();
    QString filePath = fileInfo.absoluteFilePath();
    QDir dir( fileInfo.absolutePath() );

    // Act
    QByteArray sutData = sut.ReadFile( filePath );

    // Assert
    ASSERT_EQ( sutData, data );

    // Cleanup
    dir.remove( fileInfo.absoluteFilePath() );
}

TEST_F( FileSystemServiceTests, Reading_the_file_using_invalid_filePath_throws_exception )
{
    // Arrange
    FileSystemService sut;

    // Act & Assert
    ASSERT_ANY_THROW( sut.ReadFile( "invalid_file_path") );
}

TEST_F( FileSystemServiceTests, Determining_whether_the_file_has_jpeg_format )
{
    // Arrange
    FileSystemService sut;
    QFile jpegResource( ":/gremlin.jpg" );
    jpegResource.open( QIODevice::ReadOnly );
    QByteArray jpegData = jpegResource.readAll();
    jpegResource.close();

    QFile jpegOnDisk( "gremlin.jpg" );
    jpegOnDisk.open( QIODevice::WriteOnly );
    jpegOnDisk.write( jpegData );
    QFileInfo jpegOnDiskInfo(jpegOnDisk);
    QString filePath = jpegOnDiskInfo.absoluteFilePath();
    jpegOnDisk.close();
    QDir dir( jpegOnDiskInfo.absolutePath() );

    // Act & Assert
    ASSERT_TRUE( sut.IsJpegFile( filePath ) );

    // Cleanup
    dir.remove( filePath );
}

TEST_F( FileSystemServiceTests, Determining_whether_the_file_doesnt_have_jpeg_format )
{
    // Arrange
    QString fileName = "tmp";
    QByteArray data = "bla";
    FileSystemService sut;
    QFile file( fileName );
    file.open( QIODevice::WriteOnly );
    file.write( data );
    QFileInfo fileInfo( file );
    file.close();
    QString filePath = fileInfo.absoluteFilePath();
    QDir dir( fileInfo.absolutePath() );

    // Act & Assert
    ASSERT_FALSE( sut.IsJpegFile( filePath ) );

    // Cleanup
    dir.remove( filePath );
}

TEST_F( FileSystemServiceTests, Determining_whether_the_file_doesnt_have_jpeg_format2 )
{
    // Arrange
    FileSystemService sut;

    // Act & Assert
    ASSERT_ANY_THROW( sut.IsJpegFile( "invalid_file_path" ));
}


TEST_F( FileSystemServiceTests, Getting_the_fileSize )
{
    // Arrange
    QString fileName = "tmp";
    QByteArray data = "bla";
    FileSystemService sut;
    QFile file( fileName );
    file.open( QIODevice::WriteOnly );
    file.write( data );
    QFileInfo fileInfo(file);
    file.close();
    QString filePath = fileInfo.absoluteFilePath();
    QDir dir( fileInfo.absolutePath() );

    // Act
    auto fileSize = sut.GetFileSize( filePath );

    // Assert
    ASSERT_EQ( fileSize, data.size() );

    // Cleanup
    dir.remove( filePath );
}

TEST_F( FileSystemServiceTests, Getting_the_fileSize_using_invalid_filePath_throws_exception )
{
    // Arrange
    FileSystemService sut;

    // Act & Assert
    ASSERT_ANY_THROW( sut.GetFileSize( "invalid_file_path") );
}

