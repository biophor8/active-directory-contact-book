#pragma once

#include <memory>
#include "gtest/gtest.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;

class DependencyResolverForTesting : public DependencyResolver
{
public:
    AContactPhotoProvider * GetContactPhotoProvider() override;
};

