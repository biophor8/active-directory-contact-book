/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"
#include "TestSuiteBase.h"

using namespace qmladbook;
using namespace adbook;
using namespace testing;

class MockFileSystemService : public AFileSystemService
{
public:
    MOCK_METHOD (QByteArray, ReadFile, (const QString & filePath), (override) );
    MOCK_METHOD (bool, IsJpegFile, (const QString & filePath), (override) );
    MOCK_METHOD (__int64, GetFileSize, (const QString & filePath), (override) );
};

class ContactUpdaterTests : public TestSuiteBase
{
public:
    void LoadTestContact()
    {
        ContactFinder finder( _dependencyResolver->GetAdFactory() );
        LdapRequestBuilder requestBuilder;
        requestBuilder.AddObjectCategoryRule();
        requestBuilder.AddRule( L"cn", LdapRequestBuilder::ExactMatch, L"Kristie Etherton" );
        requestBuilder.AddAND();
        QString ldapRequest = QString::fromStdWString( requestBuilder.Get() );
        auto contactFoundCallback = [this, &finder]() {
            _testContact = std::move( finder.Peek().front() );   // must be exactly one user with the name 'Kristie Etherton'
        };
        QObject::connect( &finder, &ContactFinder::contactFound, contactFoundCallback );
        finder.Start( ldapRequest, _connectionSettings );
        finder.Wait();
        finder.PropogateLastSearchException();

        _connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance().toStdWString() );
        _connector->Connect( _connectionParams );
        _testContactPhoto = _connector->DownloadBinaryAttr( L"thumbnailPhoto" );
        _testContactDn = _testContact.GetDn();
    }

    void RestoreTestContact()
    {
        _connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance().toStdWString() );
        _connector->Connect( _connectionParams );
        _connector->Rename( L"Kristie Etherton" );
        _connector->UploadStringAttr( L"department", L"" );
        _connector->UploadBinaryAttr( L"thumbnailPhoto", _testContactPhoto );
    }

    void SetUp() override
    {
        CoInitialize( nullptr );

        _dependencyResolver = GetDependencyResolver();
        _factory = _dependencyResolver->GetAdFactory();
        _settingsKeeper = _dependencyResolver->GetSettingsKeeper();
        _connectionSettings = GetConnectionSettingsForTestAdLdsInstance();
        _connectionParams = _connectionSettings;
        _fileSystemService = _dependencyResolver->GetFileSystemService();

        _settingsKeeper->SetConnectionSettings( _connectionSettings );
        _connector = _factory->CreateConnector();

        LoadTestContact();
    }

    void TearDown() override
    {
        RestoreTestContact();

        _connector = nullptr;
        _factory = nullptr;
        _settingsKeeper = nullptr;
        _fileSystemService = nullptr;
        _dependencyResolver = nullptr;

        CoUninitialize();
    }

protected:
    std::shared_ptr<AFactory> _factory;
    std::shared_ptr<AConnector> _connector;
    std::shared_ptr<ADependencyResolver> _dependencyResolver;
    std::shared_ptr<ASettingsKeeper> _settingsKeeper;
    std::shared_ptr<AFileSystemService> _fileSystemService;
    ConnectionSettings _connectionSettings;
    ConnectionParams _connectionParams;
    Contact _testContact;
    QString _testContactDn;
    BinaryAttrVal _testContactPhoto;
};

TEST_F( ContactUpdaterTests, Throws_exception_when_string_attribute_is_obsolete )
{
    // Arrange
    ContactUpdater sut ( _factory, _settingsKeeper, _fileSystemService );

    _connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance().toStdWString() );
    _connector->Connect( _connectionParams );
    std::wstring departmentNameUploadedByAnotherPerson = L"blah blah blah";
    _connector->UploadStringAttr( L"department", departmentNameUploadedByAnotherPerson );

    // Act & Assert
    // sut.Update() must throw ObsoleteAttributeValue exception and
    // download actual Attributes::Department from the server to _testContact
    ASSERT_THROW( sut.Update( _testContact, Attributes::Department, "new departmet name" ), ObsoleteAttributeValue );
    ASSERT_EQ( _testContact.GetAttrAsString( Attributes::Department ), departmentNameUploadedByAnotherPerson );
}

TEST_F( ContactUpdaterTests, Throws_exception_when_photo_is_obsolete )
{
    // Arrange
    auto mockFileSystemService = std::make_shared<MockFileSystemService>();
    EXPECT_CALL( *mockFileSystemService, IsJpegFile ).Times( 1 ).WillOnce(Return(true));
    QByteArray fakePhotoData( 10, 10 );
    EXPECT_CALL( *mockFileSystemService, ReadFile ).Times( 1 ).WillOnce( Return( fakePhotoData ) );
    QString fakePhotoPath = QString( "c:\\photo.jpg" );

    _connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance().toStdWString() );
    _connector->Connect( _connectionParams );
    BinaryAttrVal photoUploadedByAnotherPerson = BinaryAttrVal( {1,2,3,4,5,6} );
    _connector->UploadBinaryAttr( L"thumbnailPhoto", photoUploadedByAnotherPerson );

    ContactUpdater sut ( _factory, _settingsKeeper, mockFileSystemService );

    // Act & Assert
    // sut.Update() must throw ObsoleteAttributeValue exception and
    // download actual Attributes::ThumbnailPhoto from the server to _testContact
    ASSERT_THROW( sut.UpdatePhoto( _testContact, fakePhotoPath), ObsoleteAttributeValue);
    ASSERT_EQ(_testContact.GetAttrAsBinary( Attributes::ThumbnailPhoto), photoUploadedByAnotherPerson );
}

TEST_F( ContactUpdaterTests, Throws_exception_when_CommonName_is_empty )
{
    // Arrange
    ContactUpdater sut ( _factory, _settingsKeeper, _fileSystemService );

    // Act & Assert
    ASSERT_THROW( sut.Update( _testContact, Attributes::CommonName, "" ), EmptyCommonName );
}

TEST_F( ContactUpdaterTests, Throws_exception_when_string_attribute_is_too_big )
{
    // Arrange
    ContactUpdater sut ( _factory, _settingsKeeper, _fileSystemService );
    size_t departmentMaxLen = Attributes::GetInstance().GetAttrMaxLength( Attributes::Department );
    QString newDepartmentValue;
    newDepartmentValue.resize( departmentMaxLen + 1, 'x' );

    // Act & Assert
    ASSERT_THROW( sut.Update( _testContact, Attributes::Department, newDepartmentValue ), TooBigValue );
}

TEST_F( ContactUpdaterTests, Throws_exception_when_photo_file_is_too_big )
{
    // Arrange
    QString fakePhotoPath = QString( "c:\\photo.jpg" );
    size_t photoMaxLen = Attributes::GetInstance().GetAttrMaxLength( Attributes::ThumbnailPhoto );
    QByteArray fakePhotoData;
    fakePhotoData.resize( photoMaxLen + 1 );

    auto mockFileSystemService = std::make_shared<MockFileSystemService>();
    EXPECT_CALL( *mockFileSystemService, IsJpegFile ).Times( 1 ).WillOnce( Return( true ) );
    EXPECT_CALL( *mockFileSystemService, ReadFile ).Times( 1 ).WillOnce( Return( fakePhotoData ) );

    ContactUpdater sut ( _factory, _settingsKeeper, mockFileSystemService );

    // Act & Assert
    ASSERT_THROW( sut.UpdatePhoto( _testContact, fakePhotoPath ), TooBigValue );
}

TEST_F( ContactUpdaterTests, Throws_exception_when_photo_file_has_invalid_format )
{
    // Arrange
    QString fakePhotoPath = QString( "c:\\photo.jpg" );

    auto mockFileSystemService = std::make_shared<MockFileSystemService>();
    EXPECT_CALL( *mockFileSystemService, IsJpegFile ).Times( 1 ).WillOnce( Return( false ) );

    ContactUpdater sut ( _factory, _settingsKeeper, mockFileSystemService );

    // Act & Assert
    ASSERT_THROW( sut.UpdatePhoto( _testContact, fakePhotoPath ), InvalidFormat );
}

TEST_F( ContactUpdaterTests, Throws_exception_if_CommonName_has_invalid_format )
{
    // Arrange
    ContactUpdater sut ( _factory, _settingsKeeper, _fileSystemService );

    // Act & Assert
    ASSERT_THROW( sut.Update( _testContact, Attributes::CommonName, "#invalid name" ), InvalidFormat );
}

TEST_F( ContactUpdaterTests, Throws_exception_when_contact_with_specified_name_already_exists )
{
    // Arrange
    ContactUpdater sut ( _factory, _settingsKeeper, _fileSystemService );

    // Act & Assert
    ASSERT_THROW( sut.Update( _testContact, Attributes::CommonName, "Tina Ayers" ), ContactAlreadyExists );
}

TEST_F( ContactUpdaterTests, Throws_exception_when_contact_with_specified_name_could_not_be_found )
{
    // Arrange
    ContactUpdater sut ( _factory, _settingsKeeper, _fileSystemService );
    QString newDn = "CN=FAKE PERSON,CN=users,DC=testdata,DC=adbook,DC=local";
    _testContact.SetAttr( Attributes::Dn, newDn );

    // Act & Assert
    ASSERT_THROW( sut.Update( _testContact, Attributes::Department, "xyz" ), ThereIsNoSuchContactOnTheServer );
}
