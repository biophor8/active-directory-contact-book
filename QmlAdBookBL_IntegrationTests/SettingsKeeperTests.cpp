/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

using namespace qmladbook;

class SettingsKeeperTests : public ::testing::Test
{
protected:
    ConnectionSettings CreateConnectionSettingsForTest() const
    {
        ConnectionSettings cs;
        cs.SetAddress( "address" );
        cs.SetLogin( "login" );

        Password password;
        password.TryToSet( "Pa$$w0rd" );
        cs.SetPassword( password );

        cs.ConnectAsCurrentUser( false );
        return cs;
    }

    SearchSettings CreateSearchSettingsForTest() const
    {
        SearchSettings ss;
        ss.AllFilteringRulesMustBeMet( true );
        ss.EnableSearchFilters( true );
        ss.SetSerializedFilters( "string replacement for the real search filters" );
        return ss;
    }
protected:
    QString _orgName{"ACME ORG"}, _appName{"ACME APP"};
};

TEST_F( SettingsKeeperTests, Load_method_must_load_correct_settings_when_args_are_correct )
{
    // Arrange
    SettingsKeeper sut_for_saving;
    SettingsKeeper sut_for_loading;
    auto connectionSettings = CreateConnectionSettingsForTest();
    auto searchSettings = CreateSearchSettingsForTest();
    sut_for_saving.SetConnectionSettings( connectionSettings );
    sut_for_saving.SetSearchSettings( searchSettings );

    // Act
    sut_for_saving.Save( _orgName, _appName );
    sut_for_loading.Load( _orgName, _appName );

    // Assert
    ASSERT_EQ( sut_for_saving.GetConnectionSettings(), sut_for_loading.GetConnectionSettings() );
    ASSERT_EQ( sut_for_saving.GetSearchSettings(), sut_for_loading.GetSearchSettings() );
}

TEST_F( SettingsKeeperTests, Load_method_must_load_default_settings_when_args_are_incorrect )
{
    // Arrange
    SettingsKeeper sut_for_loading;
    ConnectionSettings defaultConnectionSettings;
    SearchSettings defaultSearchSettings;

    // Act
    sut_for_loading.Load( "invalid org name", "invalid app name");

    // Assert
    ASSERT_EQ( sut_for_loading.GetConnectionSettings(), defaultConnectionSettings );
    ASSERT_EQ( sut_for_loading.GetSearchSettings(), defaultSearchSettings );
}

