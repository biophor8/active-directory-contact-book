/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"
#include "TestSuiteBase.h"

using namespace qmladbook;

class SettingsCheckerTests : public TestSuiteBase
{
public:
    void SetUp() override {
        CoInitialize( nullptr );
    }
    void TearDown() override {
        CoUninitialize();
    }
protected:
};

TEST_F (SettingsCheckerTests, Checking_valid_connection_settings)
{
    // Arrange
    SettingsChecker sut(adbook::GetFactory());
    ConnectionSettings cs = GetConnectionSettingsForTestAdLdsInstance();

    // Act
    ASettingsChecker::CheckResult checkResult = sut.Check( cs );

    // Assert
    ASSERT_EQ( ASettingsChecker::CheckResultStatus::Ok, checkResult.status );

}

TEST_F ( SettingsCheckerTests, Checking_invalid_connection_settings )
{
    // Arrange
    SettingsChecker sut( adbook::GetFactory() );
    ConnectionSettings cs = GetInvalidConnectionSettings();

    // Act
    ASettingsChecker::CheckResult checkResult = sut.Check( cs );

    // Assert
    ASSERT_EQ( ASettingsChecker::CheckResultStatus::FailedToConnect, checkResult.status );
    ASSERT_EQ( checkResult.details.isEmpty(), false );
}


