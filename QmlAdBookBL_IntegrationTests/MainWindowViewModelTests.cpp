/*
Copyright (C) 2015-2021 Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/
#include <QGuiApplication>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"

#include "TestSuiteBase.h"
#include "DependencyResolverForTesting.h"

using namespace qmladbook;
using namespace adbook;
using namespace testing;

class MainWindowViewModelTests : public TestSuiteBase
{
public:
    void SetUp() override {
        _testDependencyResolver = std::make_shared< DependencyResolverForTesting>();
        _originalDependencyResolver = SetDependencyResolver( _testDependencyResolver );
    }
    void TearDown() override {
        SetDependencyResolver( _originalDependencyResolver );
    }
private:
    std::shared_ptr<ADependencyResolver> _originalDependencyResolver;
    std::shared_ptr<ADependencyResolver> _testDependencyResolver;
};

class MockClipboardService : public AClipboardService
{
public:
    MOCK_METHOD( void, PutText, (const QString & data), (override) );
    MOCK_METHOD( QString, GetText, (), (override) );
};

class MockFileSystemService : public AFileSystemService
{
public:
    MOCK_METHOD ( QByteArray, ReadFile, (const QString & filePath), (override) );
    MOCK_METHOD ( bool, IsJpegFile, (const QString & filePath), (override) );
    MOCK_METHOD ( __int64, GetFileSize, (const QString & filePath), (override) );
};


TEST_F(MainWindowViewModelTests, Adding_new_search_filter)
{
    // Arrange
    MainWindowViewModel sut;

    int searchFilterTypeIndex = 0;
    int searchFilterRuleIndex = 0;
    QString filterValue = "Stanley";

    // Act
    bool filterAdded = sut.addNewFilter(searchFilterTypeIndex, searchFilterRuleIndex, filterValue);

    // Assert
    ASSERT_TRUE(filterAdded);

    auto filtersModel = sut.searchFiltersModel();
    ASSERT_TRUE(filtersModel->rowCount() == 1);

    SearchFilterType filterType = sut.searchFilterTypesModel()->Get(searchFilterTypeIndex);
    SearchFilterRule filterRule = sut.searchFilterRulesModel()->Get(searchFilterRuleIndex);

    SearchFilter searchFilter = filtersModel->Get(0); // get the filter just added

    ASSERT_TRUE(searchFilter.GetValue() == filterValue);
    ASSERT_TRUE(searchFilter.GetType() == filterType);
    ASSERT_TRUE(searchFilter.GetRule() == filterRule);
}

TEST_F(MainWindowViewModelTests, Removing_existing_search_filter)
{
    // Arrange
    MainWindowViewModel sut;

    int searchFilterTypeIndex = 0;
    int searchFilterRuleIndex = 0;
    QString filterValue = "Stanley";
    sut.addNewFilter(searchFilterTypeIndex, searchFilterRuleIndex, filterValue);

    // Act
    bool filterRemoved = sut.removeSelectedFilter(0);

    // Assert
    ASSERT_TRUE(filterRemoved);

    auto filtersModel = sut.searchFiltersModel();
    ASSERT_TRUE(filtersModel->rowCount() == 0);
}

TEST_F(MainWindowViewModelTests, Connecting_notification_signals)
{
    // Arrange
    MainWindowViewModel sut;

    // Act
    bool searchStartedConnected = QObject::connect(&sut, &MainWindowViewModel::searchStarted, []() {});
    bool searchFinishedConnected = QObject::connect(&sut, &MainWindowViewModel::searchFinished, [](bool errorOccurred) {});
    bool contactFoundConnected = QObject::connect(&sut, &MainWindowViewModel::contactFound, []() {});

    // Assert
    bool allSignalsConnected = searchStartedConnected && searchFinishedConnected && contactFoundConnected;
    ASSERT_TRUE(allSignalsConnected);
}

TEST_F(MainWindowViewModelTests, Searching_when_filters_are_not_specified)
{
    // Arrange
    MainWindowViewModel sut;

    auto contactsModel = sut.contactsModel();
    contactsModel->Clear();
    int rowCountBeforeSearch = contactsModel->rowCount();

    auto connectionSettings = GetConnectionSettingsForTestAdLdsInstance();
    auto settingsKeeper = GetDependencyResolver()->GetSettingsKeeper();
    settingsKeeper->SetConnectionSettings(connectionSettings);

    bool searchStartedSignalReceived = false;
    QObject::connect( &sut, &MainWindowViewModel::searchStarted,
        [&searchStartedSignalReceived]() { searchStartedSignalReceived = true; }
    );

    bool errorOccurredDuringSearch = false;
    bool searchFinishedSignalReceived = false;
    QObject::connect( &sut, &MainWindowViewModel::searchFinished,
        [&searchFinishedSignalReceived, &errorOccurredDuringSearch](bool errorOccurred) {
            errorOccurredDuringSearch = errorOccurred;
            searchFinishedSignalReceived = true;
        }
    );

    bool contactFoundSignalReceived = false;
    QObject::connect( &sut, &MainWindowViewModel::contactFound,
        [&contactFoundSignalReceived]() { contactFoundSignalReceived = true; }
    );

    // Act
    bool searchStarted = sut.startSearching();
    sut.WaitUntilSearchIsFinished();
    int rowCountAfterSearch = contactsModel->rowCount();

    // Assert
    ASSERT_TRUE(searchStarted);
    ASSERT_FALSE(errorOccurredDuringSearch);
    ASSERT_TRUE(searchStartedSignalReceived);
    ASSERT_TRUE(searchFinishedSignalReceived);
    ASSERT_TRUE(contactFoundSignalReceived);
    ASSERT_EQ(rowCountBeforeSearch, 0);
    ASSERT_NE(rowCountAfterSearch, 0);
}

TEST_F( MainWindowViewModelTests, Searching_when_filters_are_specified )
{
    // Arrange
    MainWindowViewModel  sut;
    sut.enableSearchFilters( true );

    sut.addNewFilter(
        0,  // 'Names'
        0,  // 'Contains'
        "Kristie Etherton"
    );

    auto contactsModel = sut.contactsModel();
    contactsModel->Clear();
    int rowCountBeforeSearch = contactsModel->rowCount();

    auto connectionSettings = GetConnectionSettingsForTestAdLdsInstance();
    auto settingsKeeper = GetDependencyResolver()->GetSettingsKeeper();
    settingsKeeper->SetConnectionSettings( connectionSettings );

    // Act
    bool searchStarted = sut.startSearching();
    sut.WaitUntilSearchIsFinished();
    int rowCountAfterSearch = contactsModel->rowCount();
    auto contact = contactsModel->GetContact( 0 );

    // Assert
    ASSERT_EQ( contact.GetAttrAsString( Attributes::CommonName ), "Kristie Etherton" );
    ASSERT_EQ( rowCountBeforeSearch, 0 );
    ASSERT_EQ( rowCountAfterSearch, 1 );
}


TEST_F(MainWindowViewModelTests, Error_description_must_be_provided_when_error_occurred_during_search)
{
    // Arrange
    MainWindowViewModel sut;

    auto invalidConnectionSettings = GetInvalidConnectionSettings();
    auto settingsKeeper = GetDependencyResolver()->GetSettingsKeeper();
    settingsKeeper->SetConnectionSettings(invalidConnectionSettings);

    bool lastErrorChanged = false;
    QObject::connect(&sut, &MainWindowViewModel::lastErrorChanged,
        [&lastErrorChanged]() { lastErrorChanged = true; }
    );

    // Act
    bool searchStarted = sut.startSearching();
    sut.WaitUntilSearchIsFinished();

    // Assert
    ASSERT_TRUE(lastErrorChanged);
    ASSERT_FALSE(searchStarted);
    QString lastError = sut.lastError();
    ASSERT_FALSE(lastError.isEmpty());
}

TEST_F( MainWindowViewModelTests, Coping_selected_contacts_attribute_to_the_clipboard )
{
    // Arrange
    std::shared_ptr<MockClipboardService> mockClipboardService = std::make_shared< MockClipboardService>();

    MainWindowViewModel sut;
    sut.SetClipboardService( mockClipboardService );

    auto connectionSettings = GetConnectionSettingsForTestAdLdsInstance();
    auto settingsKeeper = GetDependencyResolver()->GetSettingsKeeper();
    settingsKeeper->SetConnectionSettings( connectionSettings );

    bool searchStarted = sut.startSearching();
    sut.WaitUntilSearchIsFinished();
    int contactIndex = 0;

    sut.handleContactSelected( contactIndex );
    auto commonName = sut.contactDetailsModel()->GetAttrValue( adbook::Attributes::CommonName );
    int commonNameRow = sut.contactDetailsModel()->GetAttrRow( adbook::Attributes::CommonName );

    // Act & Assert
    EXPECT_CALL( *mockClipboardService, PutText(commonName) ).Times( 1 );
    sut.copySelectedContactAttr( commonNameRow );
}

TEST_F(MainWindowViewModelTests, Contacts_details_must_be_updated_when_user_selects_another_contact)
{
    MainWindowViewModel sut;

    auto connectionSettings = GetConnectionSettingsForTestAdLdsInstance();
    auto settingsKeeper = GetDependencyResolver()->GetSettingsKeeper();
    settingsKeeper->SetConnectionSettings(connectionSettings);

    // Act
    bool searchStarted = sut.startSearching();
    sut.WaitUntilSearchIsFinished();
    int contactIndex = 0;
    sut.handleContactSelected(contactIndex);

    // Assert
    auto contactFromContactList = sut.contactsModel()->GetContact(0);
    auto contactFromContactDetails = sut.contactDetailsModel()->GetContact();
    ASSERT_EQ(contactFromContactList, contactFromContactDetails);
}

TEST_F( MainWindowViewModelTests, Sorting )
{
    // Arrange
    MainWindowViewModel sut;

    auto connectionSettings = GetConnectionSettingsForTestAdLdsInstance();
    auto settingsKeeper = GetDependencyResolver()->GetSettingsKeeper();
    settingsKeeper->SetConnectionSettings( connectionSettings );

    // Act
    bool searchStarted = sut.startSearching();
    sut.WaitUntilSearchIsFinished();

    sut.sort(
        "CommonName",   // see ContactsModel::roleNames()
        Qt::AscendingOrder
    );
    auto firstContactFromContactList = sut.contactsModel()->GetContact( 0 );
    auto lastContactFromContactList = sut.contactsModel()->GetContact( 4 );

    // Assert
    // correct order after sorting:
    // 0. Kristie Etherton
    // 1. Ramon Linden
    // 2. Stacey Garcia
    // 3. Stanley Ellis
    // 4. Tina Ayers
    // see the function CreateTestUserAccounts() in the file
    //      \AdBookBL_IntegrationTestsPrepareEnvironment\AdBookBL_IntegrationTestsPrepareEnvironment.cpp
    QString commonName = firstContactFromContactList.GetAttrAsString( adbook::Attributes::CommonName );
    ASSERT_EQ( commonName, "Kristie Etherton");
    commonName = lastContactFromContactList.GetAttrAsString( adbook::Attributes::CommonName );
    ASSERT_EQ( commonName, "Tina Ayers" );
}

TEST_F( MainWindowViewModelTests, Checking_photo_file_when_file_is_valid )
{
    // Arrange
    auto mockFileSystemService = std::make_shared<MockFileSystemService>();
    MainWindowViewModel sut;
    sut.SetFileSystemService( mockFileSystemService );

    QString fakePhotoPath = QString( "c:\\photo.jpg" );
    EXPECT_CALL( *mockFileSystemService, IsJpegFile( fakePhotoPath ) ).Times( 1 ).WillOnce( Return( true ) );

    int fakePhotoDataSize = Attributes::GetInstance().GetAttrMaxLength( Attributes::ThumbnailPhoto );
    EXPECT_CALL( *mockFileSystemService, GetFileSize( fakePhotoPath ) ).Times( 1 ).WillOnce( Return( fakePhotoDataSize ));

    // Act
    bool validPhotoFile = sut.isValidPhotoFile( fakePhotoPath );

    // Assert
    ASSERT_TRUE( validPhotoFile );
}

TEST_F( MainWindowViewModelTests, Checking_photo_file_when_file_doesnt_have_valid_format )
{
    // Arrange
    auto mockFileSystemService = std::make_shared<MockFileSystemService>();
    MainWindowViewModel sut;
    sut.SetFileSystemService( mockFileSystemService );

    QString fakePhotoPath = QString( "c:\\photo.jpg" );
    EXPECT_CALL( *mockFileSystemService, IsJpegFile( fakePhotoPath ) ).Times( 1 ).WillOnce( Return( false ) );

    // Act
    bool validPhotoFile = sut.isValidPhotoFile( fakePhotoPath );

    // Assert
    ASSERT_FALSE( validPhotoFile );
}

TEST_F( MainWindowViewModelTests, Checking_photo_file_when_file_is_too_big )
{
    // Arrange
    auto mockFileSystemService = std::make_shared<MockFileSystemService>();
    MainWindowViewModel sut;
    sut.SetFileSystemService( mockFileSystemService );

    QString fakePhotoPath = QString( "c:\\photo.jpg" );
    EXPECT_CALL( *mockFileSystemService, IsJpegFile( fakePhotoPath ) ).Times( 1 ).WillOnce( Return( true ) );

    int fakePhotoDataSize = Attributes::GetInstance().GetAttrMaxLength( Attributes::ThumbnailPhoto ) + 1;
    EXPECT_CALL( *mockFileSystemService, GetFileSize( fakePhotoPath ) ).Times( 1 ).WillOnce( Return( fakePhotoDataSize ) );

    // Act
    bool validPhotoFile = sut.isValidPhotoFile( fakePhotoPath );

    // Assert
    ASSERT_FALSE( validPhotoFile );
}

