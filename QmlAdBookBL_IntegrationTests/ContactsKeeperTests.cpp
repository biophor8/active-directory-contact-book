#include <vector>
#include "gtest/gtest.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"
#include "TestSuiteBase.h"

using namespace qmladbook;

class ContactsKeeperTests : public TestSuiteBase {
protected:
    static void PopulateContactsModelForTesting(ContactsModel & model);
};

void ContactsKeeperTests::PopulateContactsModelForTesting(ContactsModel & model)
{
    model.Clear();
    for (auto && c : CreateContactsForTesting(10)) {
        model.AddContact(std::move(c));
    }
}

TEST_F(ContactsKeeperTests, Saving_and_loading)
{
    // Arrange
    ContactsModel contactsModel1, contactsModel2, emptyModel;
    ContactsKeeper sut;
    PopulateContactsModelForTesting(contactsModel1);

    // Act
    sut.Save( &emptyModel );
    sut.Load( &emptyModel );
    sut.Save( &contactsModel1 );
    sut.Load( &contactsModel2 );

    // Assert
    ASSERT_EQ( emptyModel.rowCount(), 0 );
    ASSERT_EQ(contactsModel1, contactsModel2);
}

TEST_F( ContactsKeeperTests, Save_throws_exception_when_null_pointer_passed_in )
{
    // Arrange
    ContactsKeeper sut;

    // Act & Assert
    ASSERT_ANY_THROW(sut.Save( nullptr ));
}

TEST_F( ContactsKeeperTests, Load_throws_exception_when_null_pointer_passed_in )
{
    // Arrange
    ContactsKeeper sut;

    // Act & Assert
    ASSERT_ANY_THROW( sut.Load( nullptr ) );
}