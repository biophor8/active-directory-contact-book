/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include <QCoreApplication>
#include "gtest/gtest.h"

#include "../AdBookBL/export.h"
#include "../QmlAdBookBL/export_for_unit_tests.h"
#include "TestSuiteBase.h"

using namespace qmladbook;

class SettingsDlgViewModelTests : public TestSuiteBase {
public:
    void SetUp() override {
        QCoreApplication::setApplicationName("SettingsDlgViewModelTests_app");
        QCoreApplication::setOrganizationName("SettingsDlgViewModelTests_org");
    }
    void TearDown() override {
    }
};

TEST_F(SettingsDlgViewModelTests, Saving_and_loading_settings)
{
    // Arrange
    SettingsDlgViewModel sut_initial_empty_state;
    sut_initial_empty_state.save();

    SettingsDlgViewModel sut_for_saving, sut_for_loading;
    QString login = "login";
    QString password = "password";
    QString address = "address";
    sut_for_saving.setConnectAsCurrentUser(false);
    sut_for_saving.setConnectCurrentDomain(false);
    sut_for_saving.setLogin(login);
    sut_for_saving.setPassword(password);
    sut_for_saving.setAddress(address);

    // Act
    sut_for_saving.save();
    sut_for_loading.load();

    // Assert
    ASSERT_EQ( sut_for_loading.address(), address );
    ASSERT_EQ( sut_for_loading.login(), login );
    ASSERT_EQ( sut_for_loading.password(), password );
    ASSERT_FALSE( sut_for_loading.connectAsCurrentUser() );
    ASSERT_FALSE( sut_for_loading.connectCurrentDomain() );
}

TEST_F(SettingsDlgViewModelTests, Testing_valid_settings)
{
    // Arrange
    SettingsDlgViewModel sut;
    sut.setConnectCurrentDomain(false);
    sut.setConnectAsCurrentUser(true);
    sut.setAddress( GetLdapUrlForTestAdLdsInstance() );

    // Act
    const bool testResult = sut.testSettings();
    QString errorDescription = sut.testSettingsDetails();

    // Assert
    ASSERT_TRUE( testResult );
    ASSERT_TRUE( errorDescription.isEmpty() );
}

TEST_F(SettingsDlgViewModelTests, Testing_invalid_settings)
{
    // Arrange
    SettingsDlgViewModel sut;
    sut.setConnectCurrentDomain(false);
    sut.setConnectAsCurrentUser(true);
    sut.setAddress( GetInvalidLdapUrlForTest() );

    // Act
    const bool testResult = sut.testSettings();
    QString errorDescription = sut.testSettingsDetails();

    // Assert
    ASSERT_FALSE( testResult );
    ASSERT_FALSE( errorDescription.isEmpty() );
}

TEST_F(SettingsDlgViewModelTests, Emit_notification_signal_when_testSettingsDetails_are_changed)
{
    // Arrange
    SettingsDlgViewModel sut;
    sut.setConnectCurrentDomain(false);
    sut.setConnectAsCurrentUser(true);
    sut.setAddress( GetLdapUrlForTestAdLdsInstance() );

    bool testSettingsDetailsChangedCalled = false;
    bool testSettingsDetailsChangedConnected = QObject::connect(
        &sut, &SettingsDlgViewModel::testSettingsDetailsChanged,
        [&testSettingsDetailsChangedCalled]() { testSettingsDetailsChangedCalled = true; }
    );

    // Act
    sut.testSettings();

    // Assert
    ASSERT_TRUE(testSettingsDetailsChangedConnected);
    ASSERT_TRUE(testSettingsDetailsChangedCalled);
}







