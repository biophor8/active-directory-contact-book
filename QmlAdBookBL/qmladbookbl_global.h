#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(QMLADBOOKBL_EXPORTS)
#  define QMLADBOOKBL_API Q_DECL_EXPORT
# else
#  define QMLADBOOKBL_API Q_DECL_IMPORT
# endif
#else
# define QMLADBOOKBL_API
#endif
