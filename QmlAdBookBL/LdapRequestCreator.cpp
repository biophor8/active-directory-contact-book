/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include "LdapRequestCreator.h"

namespace qmladbook
{

std::vector<adbook::Attributes::AttrId> FilterTypeIdToAttrIds(
    SearchFilterTypeId filterId
)
{
    std::vector<adbook::Attributes::AttrId> ret;

    switch (filterId)
    {
        case SearchFilterTypeId::AnyTextAttribute:
        {
            auto & attrTraits = adbook::Attributes::GetInstance();
            for (auto attrId : attrTraits.GetAttrIds()) {
                if (attrTraits.IsEditableString( attrId )) {
                    ret.push_back( attrId );
                }
            }
        }
        break;

        case SearchFilterTypeId::CommonName:
            ret.push_back( adbook::Attributes::CommonName );
            break;

        case SearchFilterTypeId::Company:
            ret.push_back( adbook::Attributes::Company );
            break;

        case SearchFilterTypeId::Department:
            ret.push_back( adbook::Attributes::Department );
            break;

        case SearchFilterTypeId::DisplayName:
            ret.push_back( adbook::Attributes::DisplayName );
            break;

        case SearchFilterTypeId::Email:
            ret.push_back( adbook::Attributes::Email );
            break;

        case SearchFilterTypeId::EmployeeID:
            ret.push_back( adbook::Attributes::EmpId );
            break;

        case SearchFilterTypeId::GivenName:
            ret.push_back( adbook::Attributes::GivenName );
            break;

        case SearchFilterTypeId::HomePhone:
            ret.push_back( adbook::Attributes::HomePhone );
            break;

        case SearchFilterTypeId::LocalityOrCity:
            ret.push_back( adbook::Attributes::Locality );
            break;

        case SearchFilterTypeId::MobilePhone:
            ret.push_back( adbook::Attributes::MobilePhone );
            break;

        case SearchFilterTypeId::Names:
            ret.push_back( adbook::Attributes::CommonName );
            ret.push_back( adbook::Attributes::DisplayName );
            ret.push_back( adbook::Attributes::GivenName );
            ret.push_back( adbook::Attributes::SurName );
            break;

        case SearchFilterTypeId::Phones:
            ret.push_back( adbook::Attributes::HomePhone );
            ret.push_back( adbook::Attributes::MobilePhone );
            ret.push_back( adbook::Attributes::WorkPhone );
            break;

        case SearchFilterTypeId::StateOrProvince:
            ret.push_back( adbook::Attributes::State );
            break;

        case SearchFilterTypeId::SurName:
            ret.push_back( adbook::Attributes::SurName );
            break;

        case SearchFilterTypeId::Title:
            ret.push_back( adbook::Attributes::Title );
            break;

        case SearchFilterTypeId::WorkPhone:
            ret.push_back( adbook::Attributes::WorkPhone );
            break;

        default:
            throw adbook::HrError( E_INVALIDARG, L"filterId", __FUNCTIONW__ );
    }
    return ret;
}

adbook::LdapRequestBuilder::MatchingRule FilterRuleIdToLdapMatchingRule(
    SearchFilterRuleId ruleId
)
{
    switch (ruleId)
    {
        case SearchFilterRuleId::BeginsWith:
            return adbook::LdapRequestBuilder::BeginWith;

        case SearchFilterRuleId::Contains:
            return adbook::LdapRequestBuilder::Contains;

        case SearchFilterRuleId::EndsWith:
            return adbook::LdapRequestBuilder::EndWith;

        case SearchFilterRuleId::ExactMatch:
            return adbook::LdapRequestBuilder::ExactMatch;
    }
    return adbook::LdapRequestBuilder::InvalidMatchingRule;
}

QString LdapRequestCreator::Create (
    const QVector<SearchFilter> & filters,
    bool allRulesAreMandatory
)
{
    QString ldapRequest;
    adbook::LdapRequestBuilder ldapRequestBuilder;

    for (int i = 0, numFilters = filters.size(); i < numFilters; ++i)
    {
        const SearchFilter & filter = filters.at( i );
        const SearchFilterType type = filter.GetType();
        const SearchFilterTypeId filterId = type.GetId();
        auto attrIds = FilterTypeIdToAttrIds( filterId );

        const SearchFilterRule rule = filter.GetRule();
        const QString value = filter.GetValue();
        const SearchFilterRuleId ruleId = rule.GetId();
        auto matchingRule = FilterRuleIdToLdapMatchingRule( ruleId );

        ldapRequestBuilder.AddAnyOfRule( attrIds, matchingRule, value.toStdWString() );
    }

    if (filters.size() >= 2)
    {
        if (allRulesAreMandatory) {
            ldapRequestBuilder.AddAND();
        }
        else {
            ldapRequestBuilder.AddOR();
        }
    }
    ldapRequestBuilder.AddObjectCategoryRule();
    if (!filters.isEmpty()) {
        ldapRequestBuilder.AddAND();
    }
    ldapRequest = QString::fromStdWString( ldapRequestBuilder.Get() );
    return ldapRequest;
}

QString LdapRequestCreator::Create()
{
    adbook::LdapRequestBuilder ldapRequestBuilder;
    ldapRequestBuilder.AddObjectCategoryRule();
    return QString::fromStdWString( ldapRequestBuilder.Get() );

}

}
