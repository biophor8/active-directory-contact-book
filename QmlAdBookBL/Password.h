#pragma once
#include <QByteArray>
#include "qmladbookbl_global.h"

namespace qmladbook
{
class QMLADBOOKBL_API Password final
{
public:
    Password();
    Password( const Password & );
    Password( Password && ) noexcept;
    Password & operator = ( Password && ) noexcept;
    Password & operator = ( const Password & );
    ~Password();

    bool TryToSet( const QString & plainTextPassword );
    bool TryToGetAsPlainTextString( QString & plainTextString ) const;

    QString Serialize() const;
    void Deserialize( const QString & serializedPassword );

    bool IsEmpty() const;

    friend bool operator == ( const Password & p1, const Password & p2 ) {
        return p1._encodedData == p2._encodedData;
    }
private:
    std::vector<BYTE> _encodedData;
};

}
