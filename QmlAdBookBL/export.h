#pragma once
#include <memory>
#include <variant>
#include <mutex>
#include <vector>
#include <map>
#include <QVector>
#include <QString>

#include "qmladbookbl_global.h"

#include "Contact.h"
#include "Password.h"
#include "LdapRequestCreator.h"
#include "ConnectionSettings.h"
#include "ADependencyResolver.h"
#include "SearchFilterType.h"
#include "SearchFilterTypesModel.h"
#include "SearchFilterRule.h"
#include "SearchFilterRulesModel.h"
#include "SearchFiltersModel.h"
#include "ContactDetailsModel.h"
#include "MainWindowViewModel.h"
#include "SettingsDlgViewModel.h"
#include "AttrEditorViewModel.h"




