#pragma once
#include "qmladbookbl_global.h"

namespace qmladbook
{
class QMLADBOOKBL_API AttrTraits
{
public:
    static AttrTraits & Instance();

    unsigned int GetMaxLength( const adbook::Attributes::AttrId id );
    QString GetLdapName( const adbook::Attributes::AttrId id );
    QString GetOID( const adbook::Attributes::AttrId id );

private:
    adbook::Attributes & adAttr{ adbook::Attributes::GetInstance() };
};
}

