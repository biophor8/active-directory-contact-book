#pragma once
#pragma warning(disable: 4458)  // for gdi+
#include <windows.h>
#include <gdiplus.h>
#include <objidl.h>
#include <memory>
#include <variant>
#include <mutex>
#include <vector>
#include <QObject>
#include <QTime>
#include <QQuickImageProvider>

#include "qmladbookbl_global.h"

#include "ADependencyResolver.h"
#include "SearchFilterType.h"
#include "SearchFilterTypesModel.h"
#include "SearchFilterRule.h"
#include "SearchFilterRulesModel.h"
#include "SearchFiltersModel.h"
#include "Contact.h"
#include "LdapRequestCreator.h"
#include "Password.h"
#include "ConnectionSettings.h"
#include "Contact.h"
#include "MainWindowViewModel.h"
#include "SettingsDlgViewModel.h"
#include "ContactDetailsModel.h"
#include "ContactsModel.h"
#include "ContactFinder.h"
#include "AttrEditorViewModel.h"
#include "SettingsKeeper.h"
#include "ContactsKeeper.h"
#include "SettingsChecker.h"
#include "DependencyResolver.h"
#include "FileSystemService.h"
#include "AContactPhotoProvider.h"
#include "ContactUpdater.h"





