/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "SearchFilter.h"
#include "qmladbookbl_global.h"

namespace qmladbook
{
class QMLADBOOKBL_API SearchSettings final
{
public:
    void EnableSearchFilters( bool enable );
    bool SearchFiltersEnabled() const;

    void AllFilteringRulesMustBeMet( bool all );
    bool AllFilteringRulesMustBeMet() const;

    void SetSerializedFilters( const QByteArray & serializedFilters );
    QByteArray GetSerializedFilters() const;

    static bool SearchFiltersEnabled_DefaultValue();
    static bool AllFilteringRulesMustBeMet_DefaultValue();

    friend bool operator == ( const SearchSettings & ss1, const SearchSettings & ss2 )
    {
        return ss1._allFilteringRulesMustBeMet == ss2._allFilteringRulesMustBeMet
            && ss1._searchFiltersEnabled == ss2._searchFiltersEnabled
            && ss1._serializedFilters == ss2._serializedFilters;
    }

    friend bool operator != ( const SearchSettings & ss1, const SearchSettings & ss2 )
    {
        return !(ss1 == ss2);
    }
private:
    bool _searchFiltersEnabled{ false };
    bool _allFilteringRulesMustBeMet{ false };
    QByteArray _serializedFilters;
};
}


