/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

TEST( AdPersonDescTests, Writing_string_attribute )
{
    // Arrange
    adbook::AdPersonDesc adp;
    // Act
    adp.SetStringAttr( L"cn", L"John Dow" );
    // Assert
    ASSERT_TRUE( adp.IsAttributeSet( L"cn" ) );
}

TEST( AdPersonDescTests, Reading_string_attribute )
{
    // Arrange
    adbook::AdPersonDesc adp;

    // Act
    adp.SetStringAttr( L"cn", L"John Dow" );

    // Assert
    ASSERT_EQ( adp.GetStringAttr( L"cn" ),  L"John Dow" );
    ASSERT_STREQ( adp.GetStringAttrPtr( L"cn" ), L"John Dow" );
}

TEST( AdPersonDescTests, Writing_binary_attribute )
{
    // Arrange
    adbook::AdPersonDesc adp;
    std::vector<byte> data = { 1,2,3 };

    // Act
    adp.SetBinaryAttr( L"thumbnailPhoto", data );

    // Assert
    ASSERT_TRUE( adp.IsAttributeSet( L"thumbnailPhoto" ) );
}

TEST( AdPersonDescTests, Reading_binary_attribute )
{
    // Arrange
    adbook::AdPersonDesc adp;
    std::vector<byte> data = { 1,2,3 };

    // Act
    adp.SetBinaryAttr( L"thumbnailPhoto", data );
    auto dataRead = adp.GetBinaryAttr( L"thumbnailPhoto" );
    size_t numBytesRead = 0;
    auto dataReadPtr = adp.GetBinaryAttrPtr( L"thumbnailPhoto", numBytesRead );

    // Assert
    ASSERT_EQ( dataRead, data );
    ASSERT_EQ( data.size(), numBytesRead );
    ASSERT_EQ( memcmp( dataReadPtr, data.data(), numBytesRead ), 0 );
}

TEST( AdPersonDescTests, Compairing_string_attributes )
{
    // Arrange
    adbook::AdPersonDesc adp1, adp2, adp3;
    adp1.SetStringAttr( L"cn", L"John Dow" );
    adp2.SetStringAttr( L"cn", L"Bart Cow" );
    adp3.SetStringAttr( L"cn", L"John Dow" );   // identical to adp1.cn

    // Act
    // NOTE: the meaning of the return value is similar to https://en.cppreference.com/w/cpp/algorithm/lexicographical_compare
    bool compareResult1 = adp1.LexicographicalCompareStringAttrs( adp2, adbook::Attributes::CommonName );
    bool compareResult2 = adp1.LexicographicalCompareStringAttrs( adp2, L"cn");
    bool compareResult3 = adp3.LexicographicalCompareStringAttrs( adp1, adbook::Attributes::CommonName );
    bool compareResult4 = adp2.LexicographicalCompareStringAttrs( adp3, adbook::Attributes::CommonName );

    // Assert
    ASSERT_FALSE( compareResult1 );
    ASSERT_FALSE( compareResult2 );
    ASSERT_FALSE( compareResult3 );
    ASSERT_TRUE( compareResult4 );
}

TEST( AdPersonDescTests, Specifying_writable_attributes )
{
    // Arrange
    adbook::AdPersonDesc adp;
    adbook::AdPersonDesc::AttrIds attrIds;
    attrIds.insert( adbook::Attributes::AttrId::CommonName );
    attrIds.insert( adbook::Attributes::AttrId::Company );
    attrIds.insert( adbook::Attributes::AttrId::Department );

    // Act
    adp.SetWritableAttributes( attrIds );

    // Assert
    ASSERT_TRUE( adp.IsAttributeWritable( adbook::Attributes::AttrId::CommonName ) );
    ASSERT_TRUE( adp.IsAttributeWritable( adbook::Attributes::AttrId::Company ) );
    ASSERT_TRUE( adp.IsAttributeWritable( adbook::Attributes::AttrId::Department ) );
    ASSERT_FALSE( adp.IsAttributeWritable( adbook::Attributes::AttrId::Email ) );
    ASSERT_FALSE( adp.IsAttributeWritable( adbook::Attributes::AttrId::GivenName ) );
    ASSERT_EQ( adp.GetWritableAttributes(), attrIds );
}

TEST( AdPersonDescTests, Reading_distinguishedName_attr )
{
    // Arrange
    adbook::AdPersonDesc adp;
    adp.SetStringAttr( adbook::AdAttrDn, L"some_value" );

    // Act
    auto dn = adp.GetDn();

    // Assert
    ASSERT_EQ( dn, L"some_value" );
}

