#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

TEST( WstringFromCharPtrTests, Returns_wstring_which_is_equal_to_the_non_empty_argument )
{
    const char * str = "abc";

    std::wstring wstr = adbook::WstringFromCharPtr( str );

    ASSERT_EQ( wstr, L"abc" );
}

TEST( WstringFromCharPtrTests, Returns_empty_wstring_when_arg_is_null_ptr )
{
    const char * str = nullptr;

    std::wstring wstr = adbook::WstringFromCharPtr( str );

    ASSERT_TRUE( wstr.empty() );
}

TEST( WstringFromCharPtrTests, Returns_empty_wstring_when_arg_is_zero_len_str )
{
    const char * str = "";

    std::wstring wstr = adbook::WstringFromCharPtr( str );

    ASSERT_TRUE( wstr.empty() );
}


