#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

//////////////////////
class AdPathsTests : public ::testing::Test
{
public:
    void SetUp() override {
        CoInitialize( NULL );
    }
    void TearDown() override {
        CoUninitialize();
    }
};

TEST_F( AdPathsTests, IsCommonNameValid_returns_true_when_arg_is_valid )
{
    // Arrange
    adbook::AdPaths sut;

    // Act & Assert
    ASSERT_TRUE( sut.IsCommonNameValid( L"name" ) );
    ASSERT_TRUE( sut.IsCommonNameValid( L"na#me" ) );
    ASSERT_TRUE( sut.IsCommonNameValid( L"name#" ) );
    ASSERT_TRUE( sut.IsCommonNameValid( L"na me" ) );
    ASSERT_TRUE( sut.IsCommonNameValid( L"na1me" ) );
    ASSERT_TRUE( sut.IsCommonNameValid( L"na_me" ) );
    ASSERT_TRUE( sut.IsCommonNameValid( L"_name" ) );
    ASSERT_TRUE( sut.IsCommonNameValid( L"name_" ) );
    ASSERT_TRUE( sut.IsCommonNameValid( L"1name" ) );
}

TEST_F( AdPathsTests, IsCommonNameValid_returns_false_when_arg_is_invalid )
{
    // Arrange
    adbook::AdPaths sut;

    // Act & Assert
    // I don't like parameterised tests in GoogleTest framework
    ASSERT_FALSE( sut.IsCommonNameValid( L" " ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"\n" )  );
    ASSERT_FALSE( sut.IsCommonNameValid( L"\t" )  );
    ASSERT_FALSE( sut.IsCommonNameValid( L"\r" )  );
    ASSERT_FALSE( sut.IsCommonNameValid( L"" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"name_length_greater_then_64_characters________________________________________" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"#name" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"na<me" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"<name" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"name<" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"na>me" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"name>" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L">name" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"na+me" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"+name" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"name+" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"na,me" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L",name" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"name," ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"na\\me" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"\\name" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"name\\" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"na<me" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"<name" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"name<" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"na\"me" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"\"name" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"name\"" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"na;me" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L";name" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"name;" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"name/" ) );
    ASSERT_FALSE( sut.IsCommonNameValid( L"na/me" ) );
}

TEST_F( AdPathsTests, IsLdapPath_returns_true_when_arg_is_valid )
{
    // Arrange
    adbook::AdPaths sut;

    // Act & Assert
    ASSERT_TRUE( sut.IsLdapPath( L"LDAP://server" ) );
    ASSERT_TRUE( sut.IsLdapPath( L"LDAP://server:389" ) );
    ASSERT_TRUE( sut.IsLdapPath( L"LDAP://server:389/cn=bla,dc=bla" ) );
    ASSERT_TRUE( sut.IsLdapPath( L"LDAP://server/CN=bla,DC=bla" ) );
    ASSERT_TRUE( sut.IsLdapPath( L"LDAP://localhost/CN=bla,DC=bla" ) );
    ASSERT_TRUE( sut.IsLdapPath( L"LDAP://localhost/cn=users,DC=npx,DC=local" ) );
    ASSERT_TRUE( sut.IsLdapPath( L"LDAP://CN=pencil,desk" ) );
}

TEST_F( AdPathsTests, IsLdapPath_returns_false_when_arg_is_invalid )
{
    // Arrange
    adbook::AdPaths sut;

    // Act & Assert
    ASSERT_FALSE( sut.IsLdapPath( L"server" ) );
    ASSERT_FALSE( sut.IsLdapPath( L"ldap://server" ) );
    ASSERT_FALSE( sut.IsLdapPath( L"LDAP://server/" ) );
}

TEST_F( AdPathsTests, BuildLdapPath_returns_valid_ldap_path_when_args_are_correct )
{
    // Arrange
    adbook::AdPaths sut;
    std::wstring dn = L"dc=biophor,dc=local";

    auto makeConnectionParams = []( const std::wstring & address, bool connectDomainYouLoggedIn ) {
        adbook::ConnectionParams cp;
        cp.Set_ConnectDomainYouLoggedIn( connectDomainYouLoggedIn );
        cp.SetAddress( address );
        return cp;
    };


    // Act & Assert
    ASSERT_EQ(
        sut.BuildLdapPath( makeConnectionParams( L"", true ), dn ),
        L"LDAP://dc=biophor,dc=local"
    );

    ASSERT_EQ(
        sut.BuildLdapPath( makeConnectionParams( L"server:389", false ), dn),
        L"LDAP://server:389/dc=biophor,dc=local"
    );

    ASSERT_EQ(
        sut.BuildLdapPath( makeConnectionParams( L"LDAP://server:389", false ), dn ),
        L"LDAP://server:389/dc=biophor,dc=local"
    );

    ASSERT_EQ(
        sut.BuildLdapPath( makeConnectionParams( L"LDAP://server:389/cn=bla,dc=bla", false ), dn ),
        L"LDAP://server:389/dc=biophor,dc=local"
    );

    ASSERT_EQ(
        sut.BuildLdapPath( makeConnectionParams( L"LDAP://cn=bla,dc=bla", false ), dn ),
        L"LDAP://dc=biophor,dc=local"
    );
}

TEST_F( AdPathsTests, BuildLdapPath_throws_exception_when_dn_is_invalid )
{
    // Arrange
    adbook::AdPaths sut;
    std::wstring emptyDn1 = L"";
    std::wstring emptyDn2 = L" \n \r \t ";
    std::wstring invalidDn3 = L"blablabla";

    adbook::ConnectionParams cp;
    cp.Set_ConnectDomainYouLoggedIn( false );
    cp.SetAddress( L"LDAP://server:389/cn=bla,dc=bla" );

    // Act & Assert
    ASSERT_ANY_THROW( sut.BuildLdapPath( cp, emptyDn1 ) );
    ASSERT_ANY_THROW( sut.BuildLdapPath( cp, emptyDn2 ) );
    ASSERT_ANY_THROW( sut.BuildLdapPath( cp, invalidDn3 ) );
}

TEST_F( AdPathsTests, BuildLdapPath_throws_exception_when_connection_params_are_invalid )
{
    // Arrange
    adbook::AdPaths sut;
    std::wstring dn = L"cn=bla,dc=bla";
    std::wstring emptyAddress = L"";
    std::wstring incorrectAddress = L"3434 | 'incorrect address &//";

    adbook::ConnectionParams cp1, cp2;
    cp1.Set_ConnectDomainYouLoggedIn( false );
    cp1.SetAddress( emptyAddress );
    cp2.Set_ConnectDomainYouLoggedIn( false );
    cp2.SetAddress( incorrectAddress );

    // Act & Assert
    ASSERT_ANY_THROW( sut.BuildLdapPath( cp1, dn ) );
    ASSERT_ANY_THROW( sut.BuildLdapPath( cp2, dn ) );
}


