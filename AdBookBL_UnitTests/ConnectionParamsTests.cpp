/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

using namespace adbook;

TEST( ConnectionParamsTests, Leading_and_trailing_spaces_must_be_removed_from_login )
{
    ConnectionParams sut;
    std::wstring loginWithSpaces = L" login ";
    std::wstring loginWithoutSpaces = Trim(loginWithSpaces);

    sut.SetLogin( loginWithSpaces );

    ASSERT_EQ( loginWithoutSpaces, sut.GetLogin() );
}

TEST( ConnectionParamsTests, Checking_credentials_inconsistency )
{
    // Arrange
    ConnectionParams sut;
    sut.Set_ConnectAsCurrentUser( false );
    sut.SetLogin( L"" );
    sut.SetPassword( L"" );

    // Act & Assert
    ASSERT_FALSE( sut.IsConsistent() );
}

TEST( ConnectionParamsTests, Checking_credentials_inconsistency2 )
{
    // Arrange
    ConnectionParams sut;
    sut.Set_ConnectAsCurrentUser( false );
    sut.SetLogin( L"" );
    sut.SetPassword( L"password" );

    // Act & Assert
    ASSERT_FALSE( sut.IsConsistent() );
}

TEST( ConnectionParamsTests, Checking_credentials_inconsistency3 )
{
    // Arrange
    ConnectionParams sut;
    sut.Set_ConnectAsCurrentUser( false );
    sut.SetLogin( L"login" );
    sut.SetPassword( L"" );

    // Act & Assert
    ASSERT_FALSE( sut.IsConsistent() );
}

TEST( ConnectionParamsTests, Checking_creadentials_consistency )
{
    // Arrange
    ConnectionParams sut;
    sut.Set_ConnectAsCurrentUser( false );
    sut.SetLogin( L"Login" );
    sut.SetPassword( L"password" );

    // Act & Assert
    ASSERT_TRUE( sut.IsConsistent() );
}

TEST( ConnectionParamsTests, Checking_creadentials_consistency2 )
{
    // Arrange
    ConnectionParams sut;
    sut.Set_ConnectAsCurrentUser( true );
    sut.SetLogin( L"" );
    sut.SetPassword( L"" );

    // Act & Assert
    ASSERT_TRUE( sut.IsConsistent() );
}

TEST( ConnectionParamsTests, Checking_creadentials_consistency3 )
{
    // Arrange
    ConnectionParams sut;
    sut.Set_ConnectAsCurrentUser( true );

    // Act & Assert
    ASSERT_TRUE( sut.IsConsistent() );
}

TEST( ConnectionParamsTests, Checking_address_consistency )
{
    // Arrange
    ConnectionParams sut;
    sut.Set_ConnectDomainYouLoggedIn( false );
    sut.SetAddress( L"address" );

    // Act & Assert
    ASSERT_TRUE( sut.IsConsistent() );
}

TEST( ConnectionParamsTests, Checking_address_consistency2 )
{
    // Arrange
    ConnectionParams sut;
    sut.Set_ConnectDomainYouLoggedIn( true );
    sut.SetAddress( L"address" );

    // Act & Assert
    ASSERT_TRUE( sut.IsConsistent() );
}

TEST( ConnectionParamsTests, Checking_address_consistency3 )
{
    // Arrange
    ConnectionParams sut;
    sut.Set_ConnectDomainYouLoggedIn( true );

    // Act & Assert
    ASSERT_TRUE( sut.IsConsistent() );
}

