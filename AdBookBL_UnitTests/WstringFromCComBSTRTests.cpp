#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

TEST( WstringFromCComBSTRTests, Returns_wstring_which_is_equal_to_the_non_empty_argument )
{
    CComBSTR str = "abc";

    std::wstring wstr = adbook::WstringFromCComBSTR( str );

    ASSERT_EQ( wstr.compare( str ), 0 );
}


TEST( WstringFromCComBSTRTests, Returns_empty_wstring_when_arg_is_default_constructed )
{
    CComBSTR str;

    std::wstring wstr = adbook::WstringFromCComBSTR( str );

    ASSERT_TRUE( wstr.empty() );
}

TEST( WstringFromCComBSTRTests, Returns_empty_wstring_when_arg_is_null_ptr_initialized )
{
    CComBSTR str(nullptr);

    std::wstring wstr = adbook::WstringFromCComBSTR( str );

    ASSERT_TRUE( wstr.empty() );
}

TEST( WstringFromCComBSTRTests, Returns_empty_wstring_when_arg_is_initialized_with_zero_len_str )
{
    CComBSTR str( "" );

    std::wstring wstr = adbook::WstringFromCComBSTR( str );

    ASSERT_TRUE( wstr.empty() );
}


