#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"


TEST( ToLowerTests, ToLower_X_x )
{
    // Arrange
    std::wstring sut = L"AAA";
    // Act
    sut = adbook::ToLower( sut );
    // Assert
    ASSERT_EQ( sut, L"aaa" );
}


TEST( ToLowerTests, Returns_empty_string_when_SourceArg_is_empty )
{
    // Arrange
    std::wstring sut = L"";
    // Act
    sut = adbook::ToLower( sut );
    // Assert
    ASSERT_EQ( sut, L"" );
}


