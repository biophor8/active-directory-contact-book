/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

TEST(HrErrorTests, Using_HrError_with_custom_error_desc )
{
    // Arrange
    const wchar_t * error = L"Something went wrong";
    adbook::HrError sut( E_INVALIDARG, error, __FUNCTIONW__ );

    // Act & Assert
    ASSERT_EQ( sut.GetHR(), E_INVALIDARG );
    ASSERT_STREQ( sut.What(), error );
    ASSERT_STREQ( sut.Where(), __FUNCTIONW__ );
    ASSERT_NE( sut.what(), nullptr );
    ASSERT_NE( sut.GetHrDescription(), nullptr );
}

TEST( HrErrorTests, Using_HrError_without_custom_error_desc )
{
    // Arrange
    adbook::HrError sut( E_INVALIDARG, __FUNCTIONW__ );

    // Act & Assert
    ASSERT_EQ( sut.GetHR(), E_INVALIDARG );
    ASSERT_NE( sut.What(), nullptr );
    ASSERT_NE( sut.what(), nullptr );
    ASSERT_STREQ( sut.Where(), __FUNCTIONW__ );
    ASSERT_NE( sut.GetHrDescription(), nullptr );
}
