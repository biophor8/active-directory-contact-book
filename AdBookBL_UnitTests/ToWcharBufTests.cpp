#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

class ToWcharBufTests : public  testing::TestWithParam< std::wstring >
{
public:

};

TEST_P( ToWcharBufTests, Returns_writeable_buffer_which_is_identical_to_string_argument )
{
    // Arrange
    std::wstring str = GetParam();

    // Act
    adbook::WcharBuf buf = adbook::ToWcharBuf( str );

    // Assert
    ASSERT_TRUE(std::equal( str.cbegin(), str.cend(), buf.cbegin() ));
    ASSERT_EQ( buf.back(), L'\0' );
}


INSTANTIATE_TEST_SUITE_P( Default, ToWcharBufTests, testing::Values( L"", L"abc" ) );
