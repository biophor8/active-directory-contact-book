/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

TEST(AttributesTests, Retrieving_the_list_of_supported_attributes)
{
    // Arrange
    const auto & attrs = adbook::Attributes::GetInstance();
    auto attrIds = attrs.GetAttrIds();
    auto numAttrs = attrs.GetAttrCount();
    auto hasAttrId = [&attrIds]( auto attrId ) {
        return std::find( attrIds.begin(), attrIds.end(), attrId ) != attrIds.end();
    };

    // Act & Assert
    ASSERT_GT( numAttrs, 0 );
    ASSERT_EQ( numAttrs, attrIds.size() );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::CommonName) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::Email) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::Company) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::Title) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::Department) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::State) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::EmpId) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::GivenName) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::SurName) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::WorkPhone) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::DisplayName) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::Locality) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::ThumbnailPhoto) );
    ASSERT_TRUE( hasAttrId( adbook::Attributes::Dn) );
}

TEST(AttributesTests, Obtaining_AttrId_by_AttrName_and_vice_versa )
{
    // Arrange
    const auto & attrs = adbook::Attributes::GetInstance();
    auto attrIds = attrs.GetAttrIds();

    // Act
    for (auto attrId : attrIds)
    {
        auto ldapAttrName = attrs.GetLdapAttrNamePtr( attrId );
        const auto attrIdCopy = attrs.GetAttrId( ldapAttrName );

        // Assert
        ASSERT_EQ( attrIdCopy, attrId );
    }
}

TEST(AttributesTests, Checking_whether_specified_attr_is_supported)
{
    // Arrange
    const auto & attrs = adbook::Attributes::GetInstance();
    auto attrIds = attrs.GetAttrIds();

    // Act
    for (auto attrId : attrIds) {
        auto attrName = attrs.GetLdapAttrNamePtr(attrId);
        // Assert
        ASSERT_TRUE( attrs.IsAttrSupported( attrName ) );
    }

    // Assert
    ASSERT_FALSE(attrs.IsAttrSupported(L"InvalidAttrName"));
}

TEST(AttributesTests, Checking_attribute_type)
{
    // Arrange
    const auto & attrs = adbook::Attributes::GetInstance();
    auto attrIds = attrs.GetAttrIds();

    // Act & Assert
    for (auto attrId : attrIds)
    {
        if (attrId == adbook::Attributes::AttrId::ThumbnailPhoto) {
            ASSERT_FALSE( attrs.IsString(attrId) );
        }
        else {
            ASSERT_TRUE( attrs.IsString(attrId) );
        }
    }
}

TEST(AttributesTests, Checking_whether_specified_attr_can_be_changed_directly)
{
    // Arrange
    const auto & attrs = adbook::Attributes::GetInstance();
    auto attrIds = attrs.GetAttrIds();

    // Act & Assert
    for (auto attrId : attrIds) {
        if (attrId == adbook::Attributes::AttrId::Dn) {
            ASSERT_FALSE( attrs.IsEditable(attrId) );
        }
        else {
            ASSERT_TRUE( attrs.IsEditable(attrId) );
        }
    }
}

TEST(AttributesTests, Checking_whether_specified_string_attr_can_be_changed_directly )
{
    // Arrange
    const auto & attrs = adbook::Attributes::GetInstance();
    auto attrIds = attrs.GetAttrIds();

    // Act & Assert
    for (auto attrId : attrIds) {
        if (attrId == adbook::Attributes::AttrId::Dn || attrId == adbook::Attributes::AttrId::ThumbnailPhoto) {
            ASSERT_FALSE(attrs.IsEditableString(attrId));
        }
        else {
            ASSERT_TRUE(attrs.IsEditableString(attrId));
        }
    }
}

TEST(AttributesTests, Getting_oids)
{
    // Arrange
    const auto & attrs = adbook::Attributes::GetInstance();
    auto attrIds = attrs.GetAttrIds();

    // Act & Assert
    for (auto attrId : attrIds) {
        std::wstring oid = attrs.GetAttrOid(attrId);
        ASSERT_FALSE( oid.empty() );
    }
}

TEST(AttributesTests, Getting_user_friendly_names)
{
    // Arrange
    const auto & attrs = adbook::Attributes::GetInstance();
    auto attrIds = attrs.GetAttrIds();

    // Act & Assert
    for (auto attrId : attrIds) {
        std::wstring uiName = attrs.GetUiAttrName(attrId);
        ASSERT_FALSE( uiName.empty() );
    }
}

