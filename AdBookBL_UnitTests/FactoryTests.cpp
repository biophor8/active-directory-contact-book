/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/


#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

class FactoryTests : public ::testing::Test
{
public:
    void SetUp() override {
        CoInitialize( NULL );
    }
    void TearDown() override {
        CoUninitialize();
    }
};


TEST_F(FactoryTests, Factory_can_create_Connector)
{
    auto factory = adbook::GetFactory();

    auto connector = factory->CreateConnector();

    ASSERT_TRUE( connector != nullptr );
}

TEST_F( FactoryTests, Factory_can_create_ConnectorRawPtr )
{
    auto factory = adbook::GetFactory();

    auto connectorRawPointer = factory->CreateConnectorRawPtr();

    ASSERT_TRUE( connectorRawPointer != nullptr );
    delete connectorRawPointer;
}

TEST_F( FactoryTests, Factory_can_create_Searcher )
{
    auto factory = adbook::GetFactory();

    auto searcher = factory->CreateSearcher();

    ASSERT_TRUE( searcher != nullptr );
}

TEST_F( FactoryTests, Factory_can_create_SearcherRawPtr )
{
    auto factory = adbook::GetFactory();

    auto searcherRawPointer = factory->CreateSearcherRawPtr();

    ASSERT_TRUE( searcherRawPointer != nullptr );
    delete searcherRawPointer;
}

TEST_F( FactoryTests, Factory_can_retrieve_PersonDescKeeper )
{
    auto factory = adbook::GetFactory();

    auto personDescKeeper = factory->GetPersonDescKeeper();

    ASSERT_TRUE( personDescKeeper != nullptr );
}

TEST_F( FactoryTests, Factory_can_retrieve_PersonDescKeeperRawPtr )
{
    auto factory = adbook::GetFactory();

    auto personDescKeeperRawPtr = factory->GetPersonDescKeeperRawPtr();

    ASSERT_TRUE( personDescKeeperRawPtr != nullptr );
}



