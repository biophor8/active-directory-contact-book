#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

class TrimTests : public  testing::TestWithParam< std::wstring >
{ };

TEST_P( TrimTests, Returns_empty_string_when_argument_contains_spaces_only )
{
    std::wstring originalString = GetParam();

    std::wstring trimmedString = adbook::Trim( originalString );

    ASSERT_TRUE( trimmedString.empty() );
}

INSTANTIATE_TEST_SUITE_P( SpacesOnly, TrimTests,
    testing::Values(
        L"",
        L"    ",
        L"\t \n \r",
        L" \t\n\r "
    )
);

TEST(TrimTests, Returns_trimmed_string_when_argument_contains_non_spaces)
{
    // Arrange
    std::wstring originalString1 = L" z";
    std::wstring originalString2 = L"  z   ";
    std::wstring originalString3 = L"\tZ\nZ\r";
    std::wstring originalString4 = L" ZZ\t\n\r ";
    std::wstring originalString5 = L"abc";

    // Act
    std::wstring trimmedString1 = adbook::Trim(originalString1);
    std::wstring trimmedString2 = adbook::Trim(originalString2);
    std::wstring trimmedString3 = adbook::Trim(originalString3);
    std::wstring trimmedString4 = adbook::Trim(originalString4);
    std::wstring trimmedString5 = adbook::Trim(originalString5);

    // Assert
    ASSERT_EQ(trimmedString1, L"z");
    ASSERT_EQ(trimmedString2, L"z");
    ASSERT_EQ(trimmedString3, L"Z\nZ");
    ASSERT_EQ(trimmedString4, L"ZZ");
    ASSERT_EQ(trimmedString5, L"abc");
}

