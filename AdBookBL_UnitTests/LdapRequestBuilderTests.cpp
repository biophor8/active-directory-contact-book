#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

using namespace adbook;

TEST(LdapRequestBuilderTests, OneAttribute_ExactMatch)
{
    LdapRequestBuilder sut;

    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"John Dow");
    std::wstring request = sut.Get();

    ASSERT_EQ(request, L"(cn=John Dow)");
}

TEST(LdapRequestBuilderTests, OneAttribute_BeginWith)
{
    LdapRequestBuilder sut;

    sut.AddRule(L"cn", LdapRequestBuilder::BeginWith, L"John");
    std::wstring request = sut.Get();

    ASSERT_EQ(request, L"(cn=John*)");
}

TEST(LdapRequestBuilderTests, OneAttribute_EndWith)
{
    LdapRequestBuilder sut;

    sut.AddRule(L"cn", LdapRequestBuilder::EndWith, L"Dow");
    std::wstring request = sut.Get();

    ASSERT_EQ(request, L"(cn=*Dow)");
}

TEST(LdapRequestBuilderTests, OneAttribute_Contains)
{
    LdapRequestBuilder sut;

    sut.AddRule(L"cn", LdapRequestBuilder::Contains, L"Dow");
    std::wstring request = sut.Get();

    ASSERT_EQ(request, L"(cn=*Dow*)");
}

TEST(LdapRequestBuilderTests, OneAttribute_ExactMatch_NotRule)
{
    LdapRequestBuilder sut;

    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"John Dow");
    sut.AddNOT();
    std::wstring request = sut.Get();

    ASSERT_EQ(request, L"(!(cn=John Dow))");
}

TEST(LdapRequestBuilderTests, TwoAttributes_AndRule)
{
    LdapRequestBuilder sut;

    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"John Dow");
    sut.AddRule(L"company", LdapRequestBuilder::ExactMatch, L"MSFT");
    sut.AddAND();
    std::wstring request = sut.Get();

    ASSERT_EQ(request, L"(&(cn=John Dow)(company=MSFT))");
}

TEST(LdapRequestBuilderTests, TwoAttributes_OrRule)
{
    LdapRequestBuilder sut;

    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"John Dow");
    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"Jane Dow");
    sut.AddOR();
    std::wstring request = sut.Get();

    ASSERT_EQ(request, L"(|(cn=John Dow)(cn=Jane Dow))");
}

TEST(LdapRequestBuilderTests, TwoAttributes_OrRule_NotRule)
{
    LdapRequestBuilder sut;

    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"John Dow");
    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"Jane Dow");
    sut.AddOR();
    sut.AddNOT();
    std::wstring request = sut.Get();

    ASSERT_EQ(request, L"(!(|(cn=John Dow)(cn=Jane Dow)))");
}

TEST(LdapRequestBuilderTests, ThreeAttributes_OrRule_AndRule)
{
    LdapRequestBuilder sut;

    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"John Dow");
    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"Jane Dow");
    sut.AddOR();
    sut.AddRule(L"company", LdapRequestBuilder::ExactMatch, L"MSFT");
    sut.AddAND();
    std::wstring request = sut.Get();

    ASSERT_EQ(request, L"(&(|(cn=John Dow)(cn=Jane Dow))(company=MSFT))");
}

TEST( LdapRequestBuilderTests, OneAnyOfRule )
{
    LdapRequestBuilder sut;
    std::vector<adbook::Attributes::AttrId> attrIds;
    attrIds.push_back( adbook::Attributes::CommonName );
    attrIds.push_back( adbook::Attributes::DisplayName );
    attrIds.push_back( adbook::Attributes::GivenName );
    attrIds.push_back( adbook::Attributes::SurName );

    sut.AddAnyOfRule( attrIds, LdapRequestBuilder::BeginWith, L"John" );
    std::wstring request = sut.Get();

    ASSERT_EQ( request, L"(|(cn=John*)(displayName=John*)(givenName=John*)(sn=John*))" );
}

TEST( LdapRequestBuilderTests, TwoAnyOfRule )
{
    LdapRequestBuilder sut;

    sut.AddRule( L"Department", LdapRequestBuilder::ExactMatch, L"Finance" );
    std::vector<adbook::Attributes::AttrId> attrIds;
    attrIds.push_back( adbook::Attributes::CommonName );
    attrIds.push_back( adbook::Attributes::DisplayName );
    attrIds.push_back( adbook::Attributes::GivenName );
    attrIds.push_back( adbook::Attributes::SurName );
    sut.AddAnyOfRule( attrIds, LdapRequestBuilder::BeginWith, L"John" );
    attrIds.clear();
    attrIds.push_back( adbook::Attributes::HomePhone );
    attrIds.push_back( adbook::Attributes::WorkPhone );
    attrIds.push_back( adbook::Attributes::MobilePhone );
    sut.AddAnyOfRule( attrIds, LdapRequestBuilder::Contains, L"555" );
    sut.AddRule( L"Title", LdapRequestBuilder::ExactMatch, L"Manager" );
    sut.AddAND();

    std::wstring request = sut.Get();

    ASSERT_EQ( request, L"(&(Department=Finance)(|(cn=John*)(displayName=John*)(givenName=John*)(sn=John*))(|(homePhone=*555*)(telephoneNumber=*555*)(mobile=*555*))(Title=Manager))" );
}



TEST(LdapRequestBuilderTests, Special_symbols_processing)
{
    LdapRequestBuilder sut;

    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"\\");
    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"*");
    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"/");
    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L"(");
    sut.AddRule(L"cn", LdapRequestBuilder::ExactMatch, L")");
    sut.AddOR();
    sut.AddObjectCategoryRule();
    sut.AddAND();
    std::wstring request = sut.Get();

    ASSERT_EQ(request, L"(&(|(cn=\\5c)(cn=\\2a)(cn=\\2f)(cn=\\28)(cn=\\29))(objectCategory=person))");
}

