#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

TEST(ReplaceAllInPlaceTests, Returns_original_string_when_WhatToReplaceArg_could_not_be_found)
{
    // Arrange
    std::wstring sut = L"aaa";
    // Act
    adbook::ReplaceAllInPlace( sut, L"bb", L"cc" );
    // Assert
    ASSERT_EQ( sut, L"aaa" );
}

TEST( ReplaceAllInPlaceTests, Throws_exception_when_WhatToReplaceArg_is_empty )
{
    // Arrange
    std::wstring sut = L"aaa";
    // Act & Assert
    ASSERT_ANY_THROW( adbook::ReplaceAllInPlace( sut, L"", L"a" ) );
}

TEST(ReplaceAllInPlaceTests, ReplaceAllInPlace_X_XX)
{
    // Arrange
    std::wstring sut = L"aaa";
    // Act
    adbook::ReplaceAllInPlace(sut, L"a", L"aa");
    // Assert
    ASSERT_EQ(sut, L"aaaaaa");
}

TEST(ReplaceAllInPlaceTests, ReplaceAllInPlace_X_Y)
{
    // Arrange
    std::wstring sut = L"aaa";
    // Act
    adbook::ReplaceAllInPlace(sut, L"a", L"b");
    // Assert
    ASSERT_EQ(sut, L"bbb");
}

TEST(ReplaceAllInPlaceTests, ReplaceAllInPlace_XX_X)
{
    // Arrange
    std::wstring sut = L"aaaaaa";
    // Act
    adbook::ReplaceAllInPlace(sut, L"aa", L"a");
    // Assert
    ASSERT_EQ(sut, L"aaa");
}

TEST(ReplaceAllInPlaceTests, ReplaceAllInPlace_X_)
{
    // Arrange
    std::wstring sut = L"aaaaaa";
    // Act
    adbook::ReplaceAllInPlace(sut, L"a", L"");
    // Assert
    ASSERT_EQ(sut, L"");
}

