/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

class ExtractDirFromFilePathTests : public  testing::TestWithParam< std::wstring >
{};

TEST_P( ExtractDirFromFilePathTests, Throws_exceptin_when_FilePathArg_is_invalid )
{
    // Arrange
    std::wstring str = GetParam();

    // Act & Assert
    ASSERT_ANY_THROW( adbook::ExtractDirFromFilePath( str ) );
}

INSTANTIATE_TEST_SUITE_P( InvalidPaths, ExtractDirFromFilePathTests,
    testing::Values(
        L"c:/dir/file/",
        L"c:\\dir\\file\\",
        L"c|dir|file",
        L"",
        L"abc",
        L" / / ",
        L" / /file.txt",
        L"c:",
        L"c:\\"
    )
);


TEST(ExtractDirFromFilePathTests, Returns_correct_dir_path_when_FilePathArg_is_correct_file_path)
{
    // Arrange
    std::wstring filePath1 = L"c:/dir/file.txt";
    std::wstring filePath2 = L"c:\\dir\\file.txt";

    // Act
    std::wstring dirPath1 = adbook::ExtractDirFromFilePath(filePath1);
    std::wstring dirPath2 = adbook::ExtractDirFromFilePath(filePath2);

    // Assert
    ASSERT_EQ(dirPath1, std::wstring(L"c:/dir/"));
    ASSERT_EQ(dirPath2, std::wstring(L"c:\\dir\\"));
}

