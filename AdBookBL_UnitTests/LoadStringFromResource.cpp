#include "AdBookBL/export_for_unit_tests.h"
#include "gtest/gtest.h"

class LoadStringFromResourceTests : public  testing::TestWithParam< UINT >
{ };

TEST_P( LoadStringFromResourceTests, Returns_non_empty_string_when_arg_is_valid_resId )
{
    UINT resId = GetParam();

    std::wstring str = adbook::LoadStringFromResource( resId );

    ASSERT_FALSE( str.empty() );
}


INSTANTIATE_TEST_SUITE_P( Default, LoadStringFromResourceTests,
    testing::Values (
        IDS_ATTR_CN,
        IDS_ATTR_EMAIL,
        IDS_ATTR_COMPANY,
        IDS_ATTR_TITLE,
        IDS_ATTR_DEPARTMENT,
        IDS_ATTR_STATE_PROVINCE,
        IDS_ATTR_EMPLOYEE_ID,
        IDS_ATTR_LOCALITY_CITY,
        IDS_ATTR_ANY,
        IDS_ATTR_SURNAME,
        IDS_ATTR_GIVENNAME,
        IDS_ATTR_DISPLAYNAME,
        IDS_ATTR_TELEPHONE,
        IDS_ATTR_HOMEPHONE,
        IDS_ATTR_MOBILE_PHONE,
        IDS_ATTR_THUMBNAIL_PHOTO,
        IDS_ATTR_DN,
        IDS_APP_TITLE
    )
);

TEST( LoadStringFromResourceTests, Throws_exception_when_resid_is_invalid)
{
    UINT invalidResId = 12345678;

    ASSERT_THROW( adbook::LoadStringFromResource( invalidResId ), adbook::HrError);
}
