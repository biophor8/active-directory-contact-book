// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include "adsi.h"
#include "error.h"
#include "shared.h"
#include "AdPaths.h"
#include "Attributes.h"

namespace adbook
{

AdPaths::AdPaths()
{
    HRESULT hr = _adsName.CoCreateInstance( CLSID_Pathname, NULL, CLSCTX_INPROC_SERVER );
    if (FAILED( hr )) {
        throw HrError( hr, L"CoCreateInstance() failed." __FUNCTIONW__ );
    }
}

AdPaths::AdPaths( const AdPaths & ) = default;
AdPaths::AdPaths( AdPaths && ) noexcept = default;
AdPaths & AdPaths::operator = ( const AdPaths & ) = default;
AdPaths & AdPaths::operator = ( AdPaths && ) noexcept = default;

bool AdPaths::IsCommonNameValid (
    const std::wstring & commonName
) const
{
    if (commonName.empty()) {
        return false;
    }
    if (commonName != Trim( commonName )) {
        // leading and trailing spaces are disallowed.
        return false;
    }
    if (HasAuxilarySymbols( commonName )) {
        return false;
    }
    const size_t maxLenInChars = Attributes::GetInstance().GetAttrMaxLength( Attributes::AttrId::CommonName );
    if (commonName.length() > maxLenInChars) {
        return false;
    }
    auto firstNonPrintableCharIter = std::find_if( commonName.cbegin(), commonName.cend(),
        []( auto ch ) { return ch < L' '; }
    );
    if (firstNonPrintableCharIter != commonName.cend()) {
        // non-printable characters are not allowed.
        // ldap standard allows non-printable characters inside CN, but AdBook doesn't.
        return false;
    }
    return true;
}

bool AdPaths::IsLdapPath (
    const std::wstring & str
) const
{
    if (str.empty() || (str != Trim( str ))) {
        return false;
    }
    const HRESULT hr = _adsName->Set( CComBSTR( str.c_str() ), ADS_SETTYPE_FULL );
    return SUCCEEDED( hr );
}

bool AdPaths::IsDistinguishedName (
    const std::wstring & str
) const
{
    if (str.empty() || (str != Trim( str )) || (str.find( L'=' ) == std::wstring::npos)) {
        return false;
    }
    const HRESULT hr = _adsName->Set( CComBSTR( str.c_str() ), ADS_SETTYPE_DN );
    return SUCCEEDED( hr );
}

std::wstring AdPaths::ReplaceDistinguishedName (
    const std::wstring & ldapPath,
    const std::wstring & distinguishedName
) const
{
    if (!IsLdapPath( ldapPath )) {
        throw HrError( E_INVALIDARG, L"ldapPath", __FUNCTIONW__ );
    }
    if (!IsDistinguishedName( distinguishedName )) {
        throw HrError( E_INVALIDARG, L"distinguishedName", __FUNCTIONW__ );
    }
    const bool ldapPathContainsDn = (ldapPath.find( L'=' ) != std::wstring::npos);

    std::wstring resultLdapPath;
    if (ldapPathContainsDn) {
        resultLdapPath = ldapPath.substr( 0, ldapPath.rfind( L'/' ) + 1 ) + distinguishedName;
    }
    else
    {
        if (ldapPath.back() == L'/') {
            resultLdapPath = ldapPath + distinguishedName;
        }
        else {
            resultLdapPath = ldapPath + L'/' + distinguishedName;
        }
    }
    return resultLdapPath;
}

std::wstring AdPaths::BuildLdapPath (
    const ConnectionParams & connectionSettings,
    const std::wstring & distinguishedName
) const
{
    if (distinguishedName.empty() || (distinguishedName != Trim( distinguishedName ))) {
        throw HrError( E_INVALIDARG, L"distinguishedName", __FUNCTIONW__ );
    }
    std::wstring ldapPath = L"LDAP://";
    const bool addressSpecified = !connectionSettings.Get_ConnectDomainYouLoggedIn();

    if (addressSpecified)
    {
        const std::wstring address = Trim( connectionSettings.GetAddress() );
        if (address.empty()) {
            throw HrError( E_INVALIDARG, L"connectionSettings.address", __FUNCTIONW__ );
        }

        if (IsLdapPath( address ))
        {
            ldapPath = ReplaceDistinguishedName( address, distinguishedName );
            if (IsLdapPath( ldapPath )) {
                return ldapPath;
            }
            throw HrError( E_INVALIDARG, L"distinguishedName", __FUNCTIONW__ );
        }

        ldapPath += address + L"/";
    }
    ldapPath += distinguishedName;

    if (IsLdapPath( ldapPath )) {
        return ldapPath;
    }
    throw HrError( E_INVALIDARG, L"the result is not an ldap path", __FUNCTIONW__ );
}

bool AdPaths::HasAuxilarySymbols (
    const std::wstring & commonName
)
{
    if (commonName.empty()) {
        return false;
    }
    if (Trim( commonName ).empty()) {
        return false;
    }
    for (size_t i = 0, maxI = commonName.size(); i < maxI; ++i)
    {
        switch (const wchar_t ch = commonName[i]; ch)
        {
            case L'#':
                if (0 == i) {
                    return true;
                }
                break;

            case L'<': case L',': case L';': case L'+': case L'\\': case L'"': case L'>': case L'/':
                return true;

            default:
                break;
        }
    }
    return false;
}

}   // namespace adbook
