/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "AdBookBLExport.h"
#include "APersonDescKeeper.h"
#include "SqliteSimpleWrapper.h"

namespace adbook
{
// It is used to keep the local copy of search results
class ADBOOKBL_API AdPersonDescSqliteKeeper : public APersonDescKeeper
{
public:
    AdPersonDescSqliteKeeper();

    // set the filename where data is saved.
    void SetInternalNamePrefix( const std::wstring & prefix );
    void SetNameByConnectionParams( const ConnectionParams & connectionParams ) override;
    void Load( std::vector<AdPersonDesc> & ) override;
    void Save( const std::vector<AdPersonDesc> & ) override;
private:
    void InternalLoad( std::vector<AdPersonDesc> & people );
    void InternalSave( const std::vector<AdPersonDesc> & people );
private:
    std::wstring _internalNamePrefix;
    ConnectionParams _connectionParams;
};

}
