// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/


#include "stdafx.h"
#include "debug.h"
#include "error.h"
#include "shared.h"
#include "SqliteSimpleWrapper.h"
#include "SimpleInterprocessSync.h"

namespace adbook
{
size_t SqliteSimpleWrapper::numInstances{ 0 };

void SqliteSimpleWrapper::InitSqlite()
{
    if (const int rc = sqlite3_initialize(); rc != SQLITE_OK) {
        throw Sqlite3Error( rc, L"sqlite3_initialize()", __FUNCTIONW__ );
    }
}

void SqliteSimpleWrapper::DeinitSqlite()
{
    if (const int rc = sqlite3_shutdown(); rc != SQLITE_OK) {
        throw Sqlite3Error( rc, L"sqlite3_shutdown()", __FUNCTIONW__ );
    }
}

SqliteSimpleWrapper::SqliteSimpleWrapper (
    const std::wstring & dbFileName,
    DWORD flags
)
{
    if (0 == numInstances++) {
        InitSqlite();
    }
    const int rc = sqlite3_open_v2( CW2A( dbFileName.c_str() ), &_db, flags, nullptr );
    if (rc != SQLITE_OK) {
        throw Sqlite3Error( rc, L"sqlite3_open_v2()", __FUNCTIONW__ );
    }
}

SqliteSimpleWrapper::SqliteSimpleWrapper( SqliteSimpleWrapper && that ) noexcept
    : _db{ std::exchange( that._db, nullptr ) }, _stmt{ std::exchange( that._stmt, nullptr ) }
{ }

SqliteSimpleWrapper & SqliteSimpleWrapper::operator = ( SqliteSimpleWrapper && that ) noexcept
{
    if (this == &that) {
        return *this;
    }
    _db = std::exchange( that._db, nullptr );
    _stmt = std::exchange( that._stmt, nullptr );
    return *this;
}

void SqliteSimpleWrapper::CloseDb()
{
    if (_db != nullptr)
    {
        const int rc = sqlite3_close( _db );
        if (rc != SQLITE_OK) {
            throw Sqlite3Error( rc, L"sqlite3_close()", __FUNCTIONW__ );
        }
        _db = nullptr;
    }
}

void SqliteSimpleWrapper::PrepareStatement (
    const std::wstring & sql
)
{
    const int rc = sqlite3_prepare16_v2( _db, sql.c_str(), -1, &_stmt, nullptr );
    if (rc != SQLITE_OK) {
        const std::wstring m = std::wstring( L"sqlite3_prepare16_v2(). sql: " ) + sql;
        throw Sqlite3Error( rc, m.c_str(), __FUNCTIONW__ );
    }
}

int SqliteSimpleWrapper::Step()
{
    if (nullptr == _stmt) {
        throw HrError( E_UNEXPECTED, __FUNCTIONW__ );
    }
    const int rc = sqlite3_step( _stmt );
    if (rc != SQLITE_DONE && rc != SQLITE_ROW) {
        throw Sqlite3Error( rc, L"sqlite3_step()", __FUNCTIONW__ );
    }
    return rc;
}

void SqliteSimpleWrapper::FinalizeStatement()
{
    if (nullptr == _stmt) {
        throw HrError( E_UNEXPECTED, __FUNCTIONW__ );
    }
    const int rc = sqlite3_finalize( _stmt );
    _stmt = nullptr;
    if (rc != SQLITE_OK) {
        throw Sqlite3Error( rc, L"sqlite3_finalize()", __FUNCTIONW__ );
    }
}

void SqliteSimpleWrapper::ResetStatement()
{
    if (nullptr == _stmt) {
        throw HrError( E_UNEXPECTED, __FUNCTIONW__ );
    }
    if (const int rc = sqlite3_reset( _stmt ); rc != SQLITE_OK) {
        throw Sqlite3Error( rc, L"sqlite3_reset().", __FUNCTIONW__ );
    }
}

void SqliteSimpleWrapper::ClearBindings()
{
    if (nullptr == _stmt) {
        throw HrError( E_UNEXPECTED, __FUNCTIONW__ );
    }
    const int rc = sqlite3_clear_bindings( _stmt );
    if (rc != SQLITE_OK) {
        throw Sqlite3Error( rc, L"sqlite3_clear_bindings().", __FUNCTIONW__ );
    }
}

int SqliteSimpleWrapper::GetParameterIndex (
    const std::wstring & paramName
)
{
    if (nullptr == _stmt) {
        throw HrError( E_UNEXPECTED, __FUNCTIONW__ );
    }
    const int paramIndex = sqlite3_bind_parameter_index( _stmt, CW2A( paramName.c_str() ) );
    if (0 == paramIndex) {
        throw HrError( E_INVALIDARG, L"sqlite3_bind_parameter_index().", __FUNCTIONW__ );
    }
    return paramIndex;
}

void SqliteSimpleWrapper::BindText (
    const std::wstring & paramName,
    const wchar_t * value
)
{
    const int paramIndex = GetParameterIndex( paramName );
    const int rc = sqlite3_bind_text16( _stmt, paramIndex, value, -1, SQLITE_STATIC );
    if (rc != SQLITE_OK) {
        throw Sqlite3Error( rc, L"sqlite3_bind_text16()", __FUNCTIONW__ );
    }
}

void SqliteSimpleWrapper::BindBlob (
    const std::wstring & paramName,
    const BYTE * value, int len
)
{
    const int paramIndex = GetParameterIndex( paramName );
    const int rc = sqlite3_bind_blob( _stmt, paramIndex, value, len, SQLITE_STATIC );
    if (rc != SQLITE_OK)
    {
        throw Sqlite3Error( rc, L"sqlite3_bind_blob()", __FUNCTIONW__ );
    }
}

int SqliteSimpleWrapper::GetColumnCount()
{
    if (nullptr == _stmt) {
        throw HrError( E_UNEXPECTED, __FUNCTIONW__ );
    }
    if (const int rc = sqlite3_column_count( _stmt ); 0 == rc) {
        throw Sqlite3Error( rc, L"sqlite3_column_count()", __FUNCTIONW__ );
    }
    else {
        return rc;
    }
}

std::wstring SqliteSimpleWrapper::GetColName (
    int index
)
{
    if (nullptr == _stmt) {
        throw HrError( E_UNEXPECTED, __FUNCTIONW__ );
    }
    const wchar_t * p = reinterpret_cast<const wchar_t *>(sqlite3_column_name16( _stmt, index ));
    if (nullptr == p) {
        throw Sqlite3Error( SQLITE_FAIL, L"sqlite3_column_name16()", __FUNCTIONW__ );
    }
    return std::wstring( p );
}

int SqliteSimpleWrapper::GetColumnType (
    int index
)
{
    if (nullptr == _stmt) {
        throw HrError( E_UNEXPECTED, __FUNCTIONW__ );
    }
    switch (const int rc = sqlite3_column_type( _stmt, index ); rc)
    {
        case SQLITE_INTEGER: case SQLITE_FLOAT: case SQLITE_TEXT: case SQLITE_BLOB: case SQLITE_NULL:
            return rc;
        default:
            throw Sqlite3Error( rc, L"sqlite3_column_type()", __FUNCTIONW__ );
    }
}

std::wstring SqliteSimpleWrapper::GetColumnText (
    int index
)
{
    if (nullptr == _stmt) {
        throw HrError( E_UNEXPECTED, __FUNCTIONW__ );
    }
    const wchar_t * text = reinterpret_cast<const wchar_t *>(sqlite3_column_text16( _stmt, index ));
    const int sizeInBytes = sqlite3_column_bytes16( _stmt, index );
    if (0 == sizeInBytes || nullptr == text) {
        return std::wstring();
    }
    return std::wstring( text, text + static_cast<size_t>(sizeInBytes) / sizeof( wchar_t ) );
}

std::vector<BYTE> SqliteSimpleWrapper::GetColumnBlob (
    int index
)
{
    const BYTE * data = reinterpret_cast<const BYTE *>(sqlite3_column_blob( _stmt, index ));
    int sizeInBytes = sqlite3_column_bytes( _stmt, index );
    if (nullptr == data || 0 == sizeInBytes)
    {
        return std::vector<BYTE>();
    }
    return std::vector<BYTE>( data, data + static_cast<size_t>(sizeInBytes) );
}

SqliteSimpleWrapper::~SqliteSimpleWrapper()
{
    if (_stmt != nullptr)
    {
        sqlite3_finalize( _stmt );
    }
    if (_db != nullptr)
    {
        sqlite3_close( _db );
    }
    if (--numInstances == 0) {
        DeinitSqlite();
    }
}
}


