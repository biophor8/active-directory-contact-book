// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include "error.h"
#include "shared.h"
#include "dllmain.h"
#include "resource.h"

namespace adbook
{

WcharBuf ToWcharBuf (
    const std::wstring & s
)
{
    std::vector<wchar_t> buf( s.begin(), s.end() );
    buf.push_back( 0 );   // trailing zero
    return buf;
}

std::wstring WstringFromCComBSTR (
    const CComBSTR & bstr
)
{
    if (bstr.Length() == 0) {
        return {};
    }
    return std::wstring( static_cast<LPCWSTR>(bstr), bstr.Length() );
}

std::wstring WstringFromCharPtr (
    const char * s
)
{
    if (nullptr == s) {
        return {};
    }

    const size_t slen = strlen( s );
    if (0 == slen) {
        return {};
    }

    constexpr size_t stackBufSize = 256;
    size_t numCharsConverted{};

    if (slen < stackBufSize) {
        wchar_t wBuf[stackBufSize]{};
        mbstowcs_s( &numCharsConverted, wBuf, s, _TRUNCATE );
        return wBuf;
    }
    WcharBuf buf( slen + 1, 0 );
    mbstowcs_s( &numCharsConverted, buf.data(), buf.size(), s, _TRUNCATE );
    return buf.data();
}

std::wstring LoadStringFromResource (
    UINT resId
)
{
    wchar_t appName[256];
    if (!LoadStringW( moduleHandle, resId, appName, _countof( appName ) )) {
        throw HrError( HRESULT_FROM_WIN32( ERROR_RESOURCE_NOT_FOUND ), __FUNCTIONW__ );
    }
    return appName;
}

std::wstring ExtractDirFromFilePath (
    const std::wstring & filePath
)
{
    if (filePath.empty()) {
        throw HrError( E_INVALIDARG, __FUNCTIONW__ );
    }
    const wchar_t lastSym = filePath.back();
    if (lastSym == L'\\' || lastSym == L'/') {
        throw HrError( E_INVALIDARG, __FUNCTIONW__ );
    }
    std::vector<wchar_t> buf( filePath.cbegin(), filePath.cend() );
    buf.push_back( L'\0' );
    LPWSTR namePtr = PathFindFileName( &buf[0] );
    if (namePtr == &buf[0] || !namePtr) {
        throw HrError( E_INVALIDARG, __FUNCTIONW__ );
    }
    *namePtr = L'\0';
    const std::wstring dirPath = &buf[0];
    const std::wstring trimmedDirPath = Trim( dirPath );
    if (trimmedDirPath.empty()) {
        throw HrError( E_INVALIDARG, __FUNCTIONW__ );
    }
    if (dirPath != trimmedDirPath) {
        throw HrError( E_INVALIDARG, __FUNCTIONW__ );
    }
    return dirPath;
}

std::wstring Trim (
    const std::wstring & s
)
{
    if (s.empty()) {
        return {};
    }
    const wchar_t spaces[] = L" \t\n\r";
    const size_t startIndex = s.find_first_not_of( spaces );
    if (std::wstring::npos == startIndex) {
        return {};
    }
    const size_t endIndex = s.find_last_not_of( spaces );
    const std::wstring ret( s.substr( startIndex, endIndex - startIndex + 1 ) );
    return ret;
}

std::wstring_view Trim (
    const std::wstring_view & s
)
{
    if (s.empty()) {
        return s;
    }
    const wchar_t spaces[] = L" \t\n\r";

    const size_t startIndex = s.find_first_not_of( spaces );
    if (std::wstring::npos == startIndex) {
        return s.substr(0, 0);
    }
    const size_t endIndex = s.find_last_not_of( spaces );
    return s.substr( startIndex, endIndex - startIndex + 1 );
}

std::wstring & ReplaceAllInPlace (
    std::wstring & sourceString,
    const std::wstring & whatToReplace,
    const std::wstring & replacement
)
{
    if (sourceString.empty()) {
        return sourceString;
    }
    if (whatToReplace == replacement) {
        return sourceString;
    }
    if (whatToReplace.empty()) {
        throw HrError( E_INVALIDARG, L"whatToReplace is empty.", __FUNCTIONW__ );
    }
    size_t pos = sourceString.find( whatToReplace );
    const size_t whatToReplaceSize = whatToReplace.size();
    const size_t replacementSize = replacement.size();
    while (pos != std::wstring::npos)
    {
        sourceString.replace( pos, whatToReplaceSize, replacement );
        pos = sourceString.find( whatToReplace, pos + replacementSize );
    }
    return sourceString;
}

std::wstring ToLower (
    const std::wstring & s
)
{
    if (s.empty()) {
        return s;
    }
    WcharBuf tmp = ToWcharBuf( s );
    CharLowerBuffW( tmp.data(), static_cast<DWORD>(tmp.size()) );
    return tmp.data();
}

}   // namespace adbook
