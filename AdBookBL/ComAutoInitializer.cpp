#include "stdafx.h"
#include "ComAutoInitializer.h"

namespace adbook
{
ComAutoInitializer::ComAutoInitializer()
{
    HRESULT hr = CoInitialize( nullptr );
    if (FAILED( hr )) {
        throw adbook::HrError( hr, L"CoInitialize() failed", __FUNCTIONW__ );
    }
}

ComAutoInitializer::~ComAutoInitializer()
{
    CoUninitialize();
}

}

