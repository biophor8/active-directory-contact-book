// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/


#include "stdafx.h"
#include "debug.h"
#include "error.h"
#include "shared.h"
#include "dllmain.h"
#include "resource.h"
#include "ConnectionParams.h"
#include "SqliteSimpleWrapper.h"
#include "SimpleInterprocessSync.h"
#include "AdPersonDescSqliteKeeper.h"

namespace
{

const wchar_t peopleTableName[] = L"people";
const wchar_t indexColName[] = L"position";
const wchar_t writableAttrsColName[] = L"writableAttributes";
const wchar_t mutexName[] = L"{F22EEC20-56B6-443A-8C65-5D1DC38F1D78}";

std::wstring GetDbFileNameSuffix (
    const adbook::ConnectionParams & connectionParams
)
{
    if (connectionParams.Get_ConnectAsCurrentUser() && connectionParams.Get_ConnectDomainYouLoggedIn()) {
        return {};
    }
    std::hash<std::wstring> hasher;
    std::wstring address = adbook::ToLower( connectionParams.GetAddress() );

    size_t hashValue = hasher( address );
    if (!connectionParams.Get_ConnectAsCurrentUser()) {
        const std::wstring login = adbook::ToLower( connectionParams.GetLogin() );
        hashValue ^= hasher( login );
    }
#ifdef _DEBUG
    hashValue ^= hasher( L"DEBUG");
#endif
    return std::to_wstring( hashValue );
}

std::wstring GetSqliteDbFileName (
    const adbook::ConnectionParams & connectionParams
)
{
    wchar_t dbFileNameBuff[MAX_PATH] = { 0 };
    wcscat_s( dbFileNameBuff, L"SearchResult" );
    const std::wstring dbFileNameSuffix = GetDbFileNameSuffix( connectionParams );
    if (!dbFileNameSuffix.empty()) {
        wcscat_s( dbFileNameBuff, dbFileNameSuffix.c_str() );
    }
    wcscat_s( dbFileNameBuff, L".sqlite3" );
    return dbFileNameBuff;
}

std::wstring GetSqliteDbFilePath (
    const adbook::ConnectionParams & connectionParams,
    const std::wstring & filePrefix
)
{
    const std::wstring appName = adbook::LoadStringFromResource( IDS_APP_TITLE );
    wchar_t dbFilePathBuff[MAX_PATH]{};
    const HRESULT hr = SHGetFolderPathAndSubDirW( nullptr, CSIDL_FLAG_CREATE | CSIDL_APPDATA,
        nullptr, SHGFP_TYPE_CURRENT, appName.c_str(), dbFilePathBuff );
    if (FAILED( hr )) {
        throw adbook::HrError( hr, L"SHGetFolderPathAndSubDirW() failed.", __FUNCTIONW__ );
    }
    const std::wstring internalDbFileName = filePrefix + GetSqliteDbFileName( connectionParams );
    if (!PathAppendW( dbFilePathBuff, internalDbFileName.c_str() )) {
        throw adbook::HrError( E_UNEXPECTED, L"PathAppendW() failed.", __FUNCTIONW__ );
    }
    return dbFilePathBuff;
}

void EnsureFileNotExists (
    const std::wstring & filePath
)
{
    if (PathFileExistsW( filePath.c_str() )) {
        if (!DeleteFileW( filePath.c_str() )) {
            throw adbook::HrError( HRESULT_FROM_WIN32( GetLastError() ), L"DeleteFile() failed.", __FUNCTIONW__ );
        }
    }
}

void MoveOrRenameFile (
    const std::wstring & filePath,
    const std::wstring & newFilePath
)
{
    constexpr DWORD moveFlags = MOVEFILE_REPLACE_EXISTING | MOVEFILE_COPY_ALLOWED;
    if (!MoveFileEx( filePath.c_str(), newFilePath.c_str(), moveFlags )) {
        throw adbook::HrError( HRESULT_FROM_WIN32( GetLastError() ), L"MoveFile() failed.", __FUNCTIONW__ );
    }
}

adbook::SqliteSimpleWrapper StartWriting (
    const std::wstring & fileName
)
{
    adbook::SqliteSimpleWrapper sqlite( fileName, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE );
    std::wstring sql = L"BEGIN TRANSACTION";
    sqlite.PrepareStatement( sql );
    sqlite.Step();
    sqlite.FinalizeStatement();
    return sqlite;
}

std::wstring ComposeSqlCommand_CreateTable (
    const adbook::Attributes & attributes
)
{
    const auto attrIds = attributes.GetAttrIds();
    std::wstring sql = L"CREATE TABLE contacts(rowid INTEGER PRIMARY KEY NOT NULL";
    for (auto id : attrIds)
    {
        const std::wstring colName = attributes.GetLdapAttrName( id );
        if (attributes.IsString( id )) {
            sql += L", " + colName + L" TEXT";
        }
        else {
            sql += L", " + colName + L" BLOB";
        }
    }
    sql += L", ";
    sql += writableAttrsColName;
    sql += L" BLOB";
    sql += L");";
    return sql;
}

void CreateTable (
    adbook::SqliteSimpleWrapper & sqlite,
    const adbook::Attributes & attributes
)
{
    sqlite.PrepareStatement( ComposeSqlCommand_CreateTable( attributes ) );
    sqlite.Step();
    sqlite.FinalizeStatement();
}

std::vector<BYTE> GetWritableAttributesIdsAsByteArray (
    const adbook::AdPersonDesc & personDesc
)
{
    auto writableAttributes = personDesc.GetWritableAttributes();
    std::vector<BYTE> waVec;
    std::transform (
        writableAttributes.cbegin(), writableAttributes.cend(),
        std::back_inserter( waVec ),
        []( adbook::Attributes::AttrId attrId ) {
            return static_cast<BYTE>(attrId);
        }
    );
    return waVec;
}

void BindPersonAttributeValue (
    const adbook::AdPersonDesc & personDesc,
    adbook::SqliteSimpleWrapper & sqlite,
    const adbook::Attributes & attributes,
    const adbook::Attributes::AttrId attrId
)
{
    const auto attrName = attributes.GetLdapAttrName( attrId );
    if (!personDesc.IsAttributeSet( attrName )) {
        return;
    }
    const std::wstring paramName = std::wstring( L":" ) + attrName;
    if (attributes.IsString( attrId ))
    {
        const wchar_t * attrValue = personDesc.GetStringAttrPtr( attrName );
        if (attrValue) {
            sqlite.BindText( paramName, attrValue );
        }
    }
    else
    {
        size_t attrSize = 0;
        const BYTE * attrValue = personDesc.GetBinaryAttrPtr( attrName, attrSize );
        if (attrValue) {
            sqlite.BindBlob( paramName, attrValue, static_cast<int>(attrSize) );
        }
    }
};

void BindPersonAttributeValues (
    adbook::SqliteSimpleWrapper & sqlite,
    const adbook::Attributes & attributes,
    const adbook::AdPersonDesc & personDesc,
    std::vector<BYTE> & buffer
)
{
    buffer = GetWritableAttributesIdsAsByteArray( personDesc );
    if (!buffer.empty())
    {
        const std::wstring paramName = std::wstring( L":" ) + writableAttrsColName;
        sqlite.BindBlob( paramName, &buffer[0], static_cast<int>(buffer.size()) );
    }
    for (auto attrId : attributes.GetAttrIds()) {
        BindPersonAttributeValue( personDesc, sqlite, attributes, attrId );
    }
}

std::wstring ComposeSqlCommand_Insert (
    const adbook::Attributes & attributes
)
{
    const auto attrIds = attributes.GetAttrIds();

    std::wstring sql = L"INSERT INTO contacts (";
    for (auto id : attrIds)
    {
        const std::wstring colName = attributes.GetLdapAttrName( id );
        sql += colName + L",";
    }
    sql += writableAttrsColName;
    sql += L") VALUES (";
    for (auto id : attrIds)
    {
        const std::wstring colName = attributes.GetLdapAttrName( id );
        sql += L":" + colName + L",";
    }
    sql += L":";
    sql += writableAttrsColName;
    sql += L")";
    return sql;
}

void PopulateTable (
    adbook::SqliteSimpleWrapper & sqlite,
    const adbook::Attributes & attributes,
    const std::vector<adbook::AdPersonDesc> & people
)
{
    sqlite.PrepareStatement( ComposeSqlCommand_Insert( attributes ) );

    for (auto iter = people.cbegin(), end = people.cend(); iter != end; ++iter)
    {
        std::vector<BYTE> buffer;
        const auto & personDesc = *iter;
        BindPersonAttributeValues( sqlite, attributes, personDesc, buffer );
        sqlite.Step();
        sqlite.ResetStatement();
        sqlite.ClearBindings();
    }
    sqlite.FinalizeStatement();
}

void Commit (
    adbook::SqliteSimpleWrapper & sqlite
)
{
    sqlite.PrepareStatement( L"COMMIT" );
    sqlite.Step();
    sqlite.FinalizeStatement();
}

void FinishWriting (
    adbook::SqliteSimpleWrapper & sqlite
)
{
    Commit( sqlite );
    sqlite.CloseDb();
}

adbook::SqliteSimpleWrapper StartReading (
    const std::wstring & sqliteDbFilePath
)
{
    adbook::SqliteSimpleWrapper sqlite( sqliteDbFilePath, SQLITE_OPEN_READONLY );
    sqlite.PrepareStatement( L"SELECT * from contacts ORDER BY rowid ASC;" );
    return sqlite;
}

adbook::AdPersonDesc ReadPerson (
    adbook::SqliteSimpleWrapper & sqlite,
    const adbook::Attributes & attributes
)
{
    const int columnCount = sqlite.GetColumnCount();
    adbook::AdPersonDesc personDesc;
    for (int i = 0; i < columnCount; ++i)
    {
        const int colType = sqlite.GetColumnType( i );
        const std::wstring colName = sqlite.GetColName( i );

        if (colName == L"rowid") {
            continue;
        }

        if (SQLITE_BLOB == colType)
        {
            if (colName == writableAttrsColName)
            {
                const std::vector<BYTE> attrValue = sqlite.GetColumnBlob( i );
                adbook::AdPersonDesc::AttrIds wa;
                std::transform( attrValue.cbegin(), attrValue.cend(), std::inserter( wa, wa.end() ),
                    []( const BYTE b ) { return static_cast<adbook::Attributes::AttrId>(b); } );
                personDesc.SetWritableAttributes( std::move( wa ) );
            }
            else if (attributes.IsAttrSupported( colName.c_str() ))
            {
                const std::vector<BYTE> attrValue = sqlite.GetColumnBlob( i );
                personDesc.SetBinaryAttr( colName, attrValue );
            }
        }
        else if (SQLITE_TEXT == colType)
        {
            if (attributes.IsAttrSupported( colName.c_str() ))
            {
                const std::wstring attrValue = sqlite.GetColumnText( i );
                personDesc.SetStringAttr( colName, attrValue );
            }
        }
    }
    return personDesc;
}

void ReadPeople (
    adbook::SqliteSimpleWrapper & sqlite,
    const adbook::Attributes & attributes,
    std::vector<adbook::AdPersonDesc> & people
)
{
    people.clear();

    while (sqlite.Step() == SQLITE_ROW)
    {
        adbook::AdPersonDesc personDesc{ ReadPerson( sqlite, attributes ) };
        people.push_back( std::move( personDesc ) );
    }
}


void FinishReading (
    adbook::SqliteSimpleWrapper & sqlite
)
{
    sqlite.FinalizeStatement();
}

}

namespace adbook
{

AdPersonDescSqliteKeeper::AdPersonDescSqliteKeeper() = default;

void AdPersonDescSqliteKeeper::SetInternalNamePrefix (
    const std::wstring & prefix
)
{
    _internalNamePrefix = prefix;
}

void AdPersonDescSqliteKeeper::SetNameByConnectionParams (
    const ConnectionParams & connectionParams
)
{
    _connectionParams = connectionParams;
}

void AdPersonDescSqliteKeeper::InternalSave (
    const std::vector<AdPersonDesc> & people
)
{
    const std::wstring sqliteDbFilePath = GetSqliteDbFilePath( _connectionParams, _internalNamePrefix );
    const std::wstring tmpSqliteDbFilePath = sqliteDbFilePath + L".tmp";
    EnsureFileNotExists( tmpSqliteDbFilePath );

    try {
        SqliteSimpleWrapper sqlite{ StartWriting( tmpSqliteDbFilePath ) };
        const auto & attributes = Attributes::GetInstance();
        CreateTable( sqlite, attributes );
        PopulateTable( sqlite, attributes, people );
        FinishWriting( sqlite );
        MoveOrRenameFile( tmpSqliteDbFilePath, sqliteDbFilePath );
    }
    catch (const std::exception &) {
        EnsureFileNotExists( tmpSqliteDbFilePath );
        throw;
    }
}

void AdPersonDescSqliteKeeper::InternalLoad (
    std::vector<AdPersonDesc> & people
)
{
    people.clear();
    const std::wstring sqliteDbFilePath = GetSqliteDbFilePath( _connectionParams, _internalNamePrefix );
    if (!PathFileExistsW( sqliteDbFilePath.c_str() )) {
        return; // That's OK
    }
    SqliteSimpleWrapper sqlite{ StartReading( sqliteDbFilePath ) };
    ReadPeople( sqlite, Attributes::GetInstance(), people );
    FinishReading( sqlite );
}

void AdPersonDescSqliteKeeper::Load (
    std::vector<AdPersonDesc> & people
)
{
    SimpleInterprocessSync simpleSync( mutexName );
    InternalLoad( people );
}

void AdPersonDescSqliteKeeper::Save (
    const std::vector<AdPersonDesc> & people
)
{
    SimpleInterprocessSync simpleSync( mutexName );
    InternalSave( people );
}

}   // namespace adbook


