// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/


#include "stdafx.h"
#include "Searcher.h"
#include "Connector.h"
#include "Attributes.h"
#include "AdPersonDescSqliteKeeper.h"
#include "AFactory.h"
#include "AdPaths.h"

namespace adbook
{

AFactory::~AFactory() = default;

class RealFactory : public AFactory
{
public:
    RealFactory() {
        _adPersonDescKeeper = std::make_shared<AdPersonDescSqliteKeeper>();
    }
    std::unique_ptr<AConnector> CreateConnector() override {
        return std::make_unique<Connector>();
    }
    std::unique_ptr<ASearcher> CreateSearcher() override {
        return std::make_unique<Searcher>();
    }
    std::shared_ptr<APersonDescKeeper> GetPersonDescKeeper() override {
        return _adPersonDescKeeper;
    }

    AConnector * CreateConnectorRawPtr() override {
        return new Connector();
    }
    ASearcher * CreateSearcherRawPtr() override {
        return new Searcher();
    }
    APersonDescKeeper * GetPersonDescKeeperRawPtr() override {
        static AdPersonDescSqliteKeeper sqliteKeeper;
        return &sqliteKeeper;
    }

    virtual ~RealFactory() = default;

    RealFactory( const RealFactory & factory ) = delete;
    RealFactory( RealFactory && factory ) = delete;

    RealFactory & operator = ( const RealFactory & factory ) = delete;
    RealFactory & operator = ( RealFactory && factory ) = delete;
private:
    std::shared_ptr<AdPersonDescSqliteKeeper> _adPersonDescKeeper;
};

std::shared_ptr<AFactory> globalFactory;

std::shared_ptr<AFactory> GetFactory()
{
    if (!globalFactory) {
        globalFactory = std::make_shared< RealFactory>();
    }
    return globalFactory;
}

AFactory * CreateFactoryRawPtr()
{
    return new RealFactory();
}

void SetFactory( std::shared_ptr<AFactory> factory )
{
    globalFactory = factory;
}

}   // namespace adbook

