// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include "debug.h"
#include "error.h"
#include "SimpleInterprocessSync.h"

namespace adbook
{
SimpleInterprocessSync::SimpleInterprocessSync( const wchar_t * mtxName )
{
    _mutexHandle = CreateMutexW( nullptr, FALSE, mtxName );
    if (nullptr == _mutexHandle) {
        throw HrError( HRESULT_FROM_WIN32( GetLastError() ), __FUNCTIONW__ );
    }
    const DWORD waitResult = WaitForSingleObject( _mutexHandle, INFINITE );
    if (WAIT_OBJECT_0 != waitResult) {
        throw Error( L"unexpected waitResult.", __FUNCTIONW__ );
    }
}

SimpleInterprocessSync::~SimpleInterprocessSync()
{
    if (!ReleaseMutex( _mutexHandle )) {
        MY_TRACE( "ReleaseMutex() failed. 0x%X", GetLastError() );
    }
    if (!CloseHandle( _mutexHandle )) {
        MY_TRACE( "CloseHandle() failed. 0x%X", GetLastError() );
    }
}
}

