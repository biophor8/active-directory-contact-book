/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "AdBookBLExport.h"
#include "ConnectionParams.h"

namespace adbook
{

class ADBOOKBL_API AdPaths final
{
public:
    AdPaths();
    AdPaths( const AdPaths & );
    AdPaths( AdPaths && ) noexcept;
    AdPaths & operator = ( const AdPaths & );
    AdPaths & operator = ( AdPaths && ) noexcept;

    bool IsCommonNameValid( const std::wstring & commonName ) const;
    bool IsLdapPath( const std::wstring & str ) const;

    std::wstring BuildLdapPath( const ConnectionParams & connectionSettings,
                                const std::wstring & distinguishedName ) const;
private:
    bool IsDistinguishedName( const std::wstring & str ) const;

    std::wstring ReplaceDistinguishedName( const std::wstring & ldapPath,
                                           const std::wstring & distinguishedName ) const;

    static bool HasAuxilarySymbols( const std::wstring & commonName );
private:
    IADsPathNamePtr _adsName;
};

} //namespace adbook

