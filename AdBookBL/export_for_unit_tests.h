#pragma once
#include <activeds.h>
#include <atlbase.h>
#include <set>
#include <map>
#include <vector>
#include <deque>
#include <list>
#include <memory>
#include <string>
#include <functional>

#include "AdBookBLExport.h"
#include "resource.h"
#include "debug.h"
#include "error.h"
#include "adsi.h"
#include "shared.h"
#include "AdPaths.h"
#include "Attributes.h"
#include "AdPersonDesc.h"
#include "ConnectionParams.h"
#include "ComAutoInitializer.h"
#include "LdapRequestBuilder.h"
#include "ASearcher.h"
#include "AConnector.h"
#include "AFactory.h"
#include "APersonDescKeeper.h"

// for tests only.
#include "Connector.h"
#include "Searcher.h"
#include "AdPersonDescSqliteKeeper.h"
