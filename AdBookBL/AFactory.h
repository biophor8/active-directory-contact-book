/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "AdBookBLExport.h"
#include "ConnectionParams.h"
#include "AConnector.h"
#include "ASearcher.h"
#include "APersonDescKeeper.h"

namespace adbook
{

class ADBOOKBL_API AFactory
{
public:
    virtual std::unique_ptr<AConnector> CreateConnector() = 0;
    virtual std::unique_ptr<ASearcher> CreateSearcher() = 0;
    virtual std::shared_ptr<APersonDescKeeper> GetPersonDescKeeper() = 0;

    virtual AConnector * CreateConnectorRawPtr() = 0;
    virtual ASearcher * CreateSearcherRawPtr() = 0;
    virtual APersonDescKeeper * GetPersonDescKeeperRawPtr() = 0;

    virtual ~AFactory() = 0;
};

ADBOOKBL_API std::shared_ptr<AFactory> GetFactory();

ADBOOKBL_API AFactory * CreateFactoryRawPtr();

ADBOOKBL_API void SetFactory (
    std::shared_ptr<AFactory> factory
);

}   // namespace adbook

