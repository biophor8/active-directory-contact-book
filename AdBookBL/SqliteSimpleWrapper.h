/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "sqlite3/sqlite3.h"
namespace adbook
{
class SqliteSimpleWrapper final
{
public:
    SqliteSimpleWrapper( const SqliteSimpleWrapper & ) = delete;
    SqliteSimpleWrapper & operator = ( const SqliteSimpleWrapper & ) = delete;
    SqliteSimpleWrapper( SqliteSimpleWrapper && ) noexcept;
    SqliteSimpleWrapper & operator = ( SqliteSimpleWrapper && ) noexcept;
    ~SqliteSimpleWrapper();

    SqliteSimpleWrapper( const std::wstring & dbFileName, DWORD flags );

    void CloseDb();
    void PrepareStatement( const std::wstring & sql );
    int Step();
    void FinalizeStatement();
    void ResetStatement();
    void ClearBindings();
    int GetParameterIndex( const std::wstring & paramName );
    void BindText( const std::wstring & paramName, const wchar_t * value );
    void BindBlob( const std::wstring & paramName, const BYTE * value, int len );
    int GetColumnCount();
    std::wstring GetColName( int index );
    int GetColumnType( int index );
    std::wstring GetColumnText( int index );
    std::vector<BYTE> GetColumnBlob( int index );
private:
    static void InitSqlite();
    static void DeinitSqlite();
private:
    static size_t numInstances;
    sqlite3 * _db = nullptr;
    sqlite3_stmt * _stmt = nullptr;
};

}   // namespace adbook
