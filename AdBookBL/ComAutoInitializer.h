#pragma once
#include "AdBookBLExport.h"
#include "error.h"

namespace adbook
{

class ADBOOKBL_API ComAutoInitializer final
{
public:
    ComAutoInitializer();
    ~ComAutoInitializer();

    ComAutoInitializer( const ComAutoInitializer & ) = delete;
    ComAutoInitializer( ComAutoInitializer && ) = delete;

    ComAutoInitializer & operator=( const ComAutoInitializer & ) = delete;
    ComAutoInitializer & operator=( ComAutoInitializer && ) = delete;
};

}   // namespace adbook

