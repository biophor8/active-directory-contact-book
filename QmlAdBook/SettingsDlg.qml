import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.3 as Dialogs
import QtQuick.Controls 1.4 as Controls
import Qt.labs.settings 1.0
import biophor.local 1.0

Window
{
    id: settingsDlg
    title: qsTr("Settings")
    modality: Qt.WindowModal
    flags: Qt.Dialog
    minimumWidth: 300
    minimumHeight: 400

    Settings
    {
        id: settings
        category: "SettingsDlg"
        property alias x: settingsDlg.x
        property alias y: settingsDlg.y
        property alias width: settingsDlg.width
        property alias height: settingsDlg.height
    }

    SettingsDlgViewModel {
        id: settingsDlgViewModel
    }

    Component.onCompleted:
    {
        load()

        if (settingsDlgViewModel.lastError.length !== 0)
        {
            messageDialog.text = settingsDlgViewModel.lastError
            messageDialog.informativeText = ""
            messageDialog.icon = Dialogs.StandardIcon.Warning
            messageDialog.open()
        }
    }

    function load()
    {
        settingsDlgViewModel.load()

        loginTextField.text = settingsDlgViewModel.login;
        passwordTextField.text = settingsDlgViewModel.password
        addressTextField.text = settingsDlgViewModel.address

        connectCurrentDomainRadioButton.checked = settingsDlgViewModel.connectCurrentDomain
        connectSpecifiedAddress.checked = !settingsDlgViewModel.connectCurrentDomain

        connectAsCurrentUserRadioButton.checked = settingsDlgViewModel.connectAsCurrentUser
        connectAsSpecifiedUserRadioButton.checked = !settingsDlgViewModel.connectAsCurrentUser
    }

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins: 10
        focus: true
        spacing: 10

        Controls.GroupBox
        {
            Layout.alignment: Qt.AlignTop
            Layout.fillWidth: true
            title: qsTr("Connection:")

            ColumnLayout
            {
                anchors.fill: parent

                Controls.ExclusiveGroup {
                    id: connectionRadioGroup
                }

                Controls.RadioButton
                {
                    id: connectCurrentDomainRadioButton
                    text: qsTr("Connect the domain you logged on to")
                    checked: settingsDlgViewModel.connectCurrentDomain
                    exclusiveGroup: connectionRadioGroup
                }

                Controls.RadioButton {
                    id: connectSpecifiedAddress
                    checked: !settingsDlgViewModel.connectCurrentDomain
                    text: qsTr("Connect the following domain:")
                    exclusiveGroup: connectionRadioGroup
                }

                Controls.TextField
                {
                    id: addressTextField
                    text: settingsDlgViewModel.address
                    enabled: connectSpecifiedAddress.checked
                    maximumLength: 200
                    Layout.fillWidth: true
                    Layout.leftMargin: 15
                }
            }
        }

        Controls.GroupBox
        {
            Layout.fillWidth: true
            title: qsTr("Authentication:")

            ColumnLayout
            {
                anchors.fill: parent

                Controls.ExclusiveGroup {
                    id: authenticationRadioGroup
                }

                Controls.RadioButton
                {
                    id: connectAsCurrentUserRadioButton
                    text: qsTr("Use the current user's credentials")
                    checked: settingsDlgViewModel.connectAsCurrentUser
                    exclusiveGroup: authenticationRadioGroup
                }

                Controls.RadioButton {
                    id: connectAsSpecifiedUserRadioButton
                    text: qsTr("Connect using the following credentials:")
                    checked: !settingsDlgViewModel.connectAsCurrentUser
                    exclusiveGroup: authenticationRadioGroup
                }

                Controls.Label {
                    enabled: !connectAsCurrentUserRadioButton.checked
                    Layout.leftMargin: 15
                    text: qsTr("Login:")
                }

                Controls.TextField
                {
                    enabled: !connectAsCurrentUserRadioButton.checked
                    Layout.fillWidth: true
                    Layout.leftMargin: 15
                    maximumLength: 50
                    id: loginTextField
                    text: settingsDlgViewModel.login
                }

                Controls.Label {
                    text: qsTr("Password:")
                    enabled: !connectAsCurrentUserRadioButton.checked
                    Layout.leftMargin: 15
                }

                Controls.TextField {
                    id: passwordTextField
                    echoMode: TextInput.Password
                    enabled: !connectAsCurrentUserRadioButton.checked
                    text: settingsDlgViewModel.password
                    maximumLength: 50
                    Layout.fillWidth: true
                    Layout.leftMargin: 15
                }

                Controls.CheckBox {
                    text: qsTr("Display the password")
                    onCheckedChanged: {
                        if (checked === true) {
                            passwordTextField.echoMode = TextInput.Normal
                        }
                        else{
                            passwordTextField.echoMode = TextInput.Password
                        }
                    }
                }

                Controls.Button
                {
                    id: testSettingsButton
                    text: qsTr("Test");
                    Layout.minimumWidth: 100

                    enabled:
                    {
                        var loginSpecified = loginTextField.text.trim().length > 0
                        var passwordSpecified = passwordTextField.text.length > 0
                        var addressSpecified = addressTextField.text.trim().length > 0

                        if (!connectCurrentDomainRadioButton.checked && !addressSpecified) {
                            return false;
                        }
                        if (!connectAsCurrentUserRadioButton.checked && (!loginSpecified || !passwordSpecified)) {
                            return false;
                        }
                        return true;
                    }
                    onClicked: {
                        testSettings()
                    }
                }
            }
        }
        Item {
            Layout.fillHeight: true
        }

        RowLayout
        {
            Layout.margins: 10
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignBottom

            Item {
                // empty space between left and right groups of buttons
                Layout.fillWidth: true
            }

            Controls.Button
            {
                id: okButton
                text: qsTr("OK")
                isDefault: true
                Layout.minimumWidth: 100
                Layout.alignment: Qt.AlignRight

                enabled:
                {
                    enabled:
                    {
                        var loginSpecified = loginTextField.text.trim().length > 0
                        var passwordSpecified = passwordTextField.text.length > 0
                        var addressSpecified = addressTextField.text.trim().length > 0

                        if (!connectCurrentDomainRadioButton.checked && !addressSpecified) {
                            return false;
                        }
                        if (!connectAsCurrentUserRadioButton.checked && (!loginSpecified || !passwordSpecified)) {
                            return false;
                        }
                        return true;
                    }
                }

                onClicked: {
                    applyChangesAndClose()
                }
            }

            Controls.Button {
                id: cancelButton
                text: qsTr("Cancel")
                Layout.minimumWidth: 100
                Layout.alignment: Qt.AlignRight

                onClicked: {
                    revertChangesAndClose()
                }
            }
        }

        Keys.onEscapePressed: {
            revertChangesAndClose()
        }

        Keys.onReturnPressed: {
            if (okButton.enabled === true) {
                applyChangesAndClose()
            }
        }
    }

    Dialogs.MessageDialog
    {
        id: messageDialog
        title: qsTr("Active Directory Contact Book")
        modality: Qt.WindowModal
        standardButtons: Dialogs.StandardButton.Ok
    }

    function applyChangesAndClose()
    {
        settingsDlgViewModel.lastError = ""

        settingsDlgViewModel.login = loginTextField.text;
        settingsDlgViewModel.password = passwordTextField.text
        settingsDlgViewModel.address = addressTextField.text
        settingsDlgViewModel.connectCurrentDomain = connectCurrentDomainRadioButton.checked
        settingsDlgViewModel.connectAsCurrentUser = connectAsCurrentUserRadioButton.checked

        if (settingsDlgViewModel.lastError.length !== 0)
        {
            messageDialog.text = qsTr("An error occurred during the save operation.")
            messageDialog.informativeText = settingsDlgViewModel.lastError
            messageDialog.icon = Dialogs.StandardIcon.Warning
            messageDialog.open()
            return
        }

        settingsDlgViewModel.save()
        settingsDlg.close()
    }

    function revertChangesAndClose()
    {
        load()
        settingsDlg.close()
    }

    function testSettings()
    {
        settingsDlgViewModel.login = loginTextField.text;
        settingsDlgViewModel.password = passwordTextField.text
        settingsDlgViewModel.address = addressTextField.text
        settingsDlgViewModel.connectCurrentDomain = connectCurrentDomainRadioButton.checked
        settingsDlgViewModel.connectAsCurrentUser = connectAsCurrentUserRadioButton.checked

        if (settingsDlgViewModel.testSettings())
        {
            messageDialog.text = qsTr("Connection successful")
            messageDialog.informativeText = settingsDlgViewModel.testSettingsDetails
            messageDialog.icon = Dialogs.StandardIcon.Information
        }
        else
        {
            messageDialog.text = qsTr("Failed to connect")
            messageDialog.informativeText = settingsDlgViewModel.testSettingsDetails
            messageDialog.icon = Dialogs.StandardIcon.Warning
        }
        messageDialog.open()
    }
}
