﻿import QtQuick 2.3
import QtQuick.Controls 1.4 as Controls

Controls.TableViewColumn {
    resizable: true
    width: 100
    delegate: TableViewItemDelegate{}
}