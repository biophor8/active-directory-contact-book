/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"

void SetupApplicationProperties()
{
#ifdef _DEBUG
    QGuiApplication::setApplicationName( "Active Directory Contact Book [DEBUG]" );
#else
    QGuiApplication::setApplicationName( "Active Directory Contact Book" );
#endif
    QGuiApplication::setOrganizationName( "biophor" );
    QGuiApplication::setOrganizationDomain( "biophor.local" );
    QGuiApplication::setWindowIcon( QIcon( ":/app.ico" ) );
}

void RegisterCustomTypesForQML()
{
    qmlRegisterType<qmladbook::SettingsDlgViewModel>("biophor.local", 1, 0, "SettingsDlgViewModel");
    qmlRegisterType<qmladbook::MainWindowViewModel>("biophor.local", 1, 0, "MainWindowViewModel");
    qmlRegisterType<qmladbook::AttrEditorViewModel>("biophor.local", 1, 0, "AttrEditorViewModel");
    qmlRegisterType<qmladbook::ContactsModel>("biophor.local", 1, 0, "ContactsModel");
    qmlRegisterType<qmladbook::ContactDetailsModel>("biophor.local", 1, 0, "ContactDetailsModel");
    qmlRegisterType<qmladbook::SearchFilterTypesModel>("biophor.local", 1, 0, "SearchFilterTypesModel");
    qmlRegisterType<qmladbook::SearchFilterRulesModel>("biophor.local", 1, 0, "SearchFilterRulesModel");
    qmlRegisterType<qmladbook::SearchFiltersModel>("biophor.local", 1, 0, "SearchFiltersModel");
}

void LoadApplicationSettings()
{
    auto dependencyResolver = qmladbook::GetDependencyResolver();
    dependencyResolver->GetSettingsKeeper()->Load(
        QGuiApplication::organizationName(),
        QGuiApplication::applicationName()
    );
}

void SetupUiStyleProperties()
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QQuickStyle::setStyle( "Default" ); // Fusion, Universal
}

void SaveApplicationSettings()
{
    auto dependencyResolver = qmladbook::GetDependencyResolver();
    dependencyResolver->GetSettingsKeeper()->Save(
        QGuiApplication::organizationName(),
        QGuiApplication::applicationName()
    );
}

void FixTheBlurredFontProblem()
{
    QQuickWindow::setTextRenderType( QQuickWindow::NativeTextRendering );
    //QQuickWindow::setTextRenderType( QQuickWindow::QtTextRendering ); // It can cause blurred text
}

void SetupContactPhotoImageProvider(QQmlApplicationEngine & engine)
{
    auto dependencyResolver = qmladbook::GetDependencyResolver();
    qmladbook::AContactPhotoProvider * contactPhotoProvider = dependencyResolver->GetContactPhotoProvider();
    try {
        engine.addImageProvider( contactPhotoProvider->GetProviderName(), contactPhotoProvider );
        // Now the QML engine owns contactPhotoProvider.
    }
    catch (std::exception & e) {
        delete contactPhotoProvider;
        MY_TRACE_STDEXCEPTION( e );
        throw;
    }
}

bool CreateMainWindow(QQmlApplicationEngine & engine)
{
    engine.load(QUrl(QStringLiteral("qrc:/MainWindow.qml")));
    if (engine.rootObjects().isEmpty()) {
        return false;
    }
    return true;
}

int main(int argc, char *argv[])
{
    SetThreadUILanguage( MAKELANGID( LANG_ENGLISH, SUBLANG_ENGLISH_US ) );
    SetupUiStyleProperties();

    QApplication app(argc, argv);

    adbook::ComAutoInitializer comAutoInitializer;

    FixTheBlurredFontProblem();
    SetupApplicationProperties();
    RegisterCustomTypesForQML();
    LoadApplicationSettings();

    QQmlApplicationEngine engine;
    SetupContactPhotoImageProvider(engine);

    if (!CreateMainWindow(engine)) {
        return -1;
    }
    const int exitCode = app.exec();
    SaveApplicationSettings();
    qmladbook::GetDependencyResolver()->Reset();
    return exitCode;
}

