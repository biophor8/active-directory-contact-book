import QtQuick 2.12
import QtQuick.Controls 1.4 as QC1
import QtQuick.Controls 2.12 as QC2

Item {
    id: tableViewItemDelegate
    implicitWidth: parent.width
    implicitHeight: parent.height

    HoverHandler {
        id: hoverHandler
        enabled: true
    }

    Text
    {
        id: wtx
        anchors.fill: parent
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        color: styleData.textColor
        elide: styleData.elideMode
        text: styleData.value
        clip: true

        QC2.ToolTip.delay: 250
        QC2.ToolTip.timeout: -1
        QC2.ToolTip.visible: hoverHandler.hovered && wtx.truncated && (wtx.text.length > 0)
        QC2.ToolTip.text: wtx.text
    }

}



