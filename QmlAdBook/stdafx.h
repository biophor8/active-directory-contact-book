#pragma once

#pragma warning(push)
#pragma warning(disable: 4251)
#pragma warning(disable: 4127)
#pragma warning(disable: 4275)

#include <memory>
#include <future>
#include <mutex>
#include <map>
#include <variant>
#include <QObject>
#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QSettings>
#include <QString>
#include <QTime>
#include <QRect>
#include <QVector>
#include <QList>
#include <QPainter>
#include <QIcon>
#include <QQuickWindow>
#include <QIcon>


#include "AdBookBL/export_for_unit_tests.h"
#include "QmlAdBookBL/export.h"
#pragma warning(pop)