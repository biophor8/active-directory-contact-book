﻿/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.3 as Dialogs
import QtQuick.Controls 1.4 as Controls
import biophor.local 1.0

Window
{
    id: root
    title: qsTr("Attribute Editor")
    modality: Qt.WindowModal
    flags: Qt.Dialog
    minimumWidth: 300
    minimumHeight: 400

    signal accepted

    property var contactDetailsModel
    property var contactDetailsModelRow: 0

    function load() {
        viewModel.load(contactDetailsModel, contactDetailsModelRow)
    }

    AttrEditorViewModel {
        id: viewModel
    }

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins: 10
        focus: true

        Controls.Label {
            text: qsTr("Current value:")
        }
        Controls.TextField {
            id: currentValue
            enabled: true
            readOnly: true
            text: viewModel.currentValue;
            maximumLength: viewModel.maxLength
            Layout.fillWidth: true
        }
        Controls.Label {
            text: qsTr("New value:")
            Layout.topMargin: 10
        }
        Controls.TextField {
            id: newValue
            enabled: true
            text: viewModel.newValue;
            readOnly: false
            maximumLength: viewModel.maxLength
            Layout.fillWidth: true
        }

        Controls.Label {
            text: qsTr("Leading and trailing spaces are removed.")
        }

        Controls.Label {
            text: qsTr("Contact's DN:")
            Layout.topMargin: 10
        }
        Controls.TextField {
            id: dn
            readOnly: true
            text: viewModel.ownerDn
            Layout.fillWidth: true
        }
        Controls.Label {
            text: qsTr("Attribute's details:")
            Layout.topMargin: 10
        }
        Controls.TextArea {
            id: attrInfo
            readOnly: true
            text: viewModel.attrDescription
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        RowLayout
        {
            Layout.margins: 10
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignBottom

            Item {
                Layout.fillWidth: true
            }

            Controls.Button
            {
                id: okButton
                isDefault: true
                Layout.minimumWidth: 100
                Layout.alignment: Qt.AlignRight
                text: qsTr("OK")

                onClicked: {
                    acceptChangesAndClose()
                }
            }

            Controls.Button {
                id: cancelButton
                Layout.minimumWidth: 100
                text: qsTr("Cancel")
                Layout.alignment: Qt.AlignRight

                onClicked: {
                    close()
                }
            }
        }

        Keys.onReturnPressed: {
            acceptChangesAndClose()
        }

        Keys.onEscapePressed: {
            root.close()
        }
    }

    function acceptChangesAndClose()
    {
        if (!viewModel.isValid(newValue.text.trim()))
        {
            messageDialog.text = qsTr("The value is incorrect.")
            messageDialog.icon = Dialogs.StandardIcon.Warning
            messageDialog.open()
            return;
        }
        viewModel.newValue = newValue.text.trim()
        accepted()
        root.close()
    }

    Dialogs.MessageDialog {
        id: messageDialog
        title: qsTr("Active Directory Contact Book")
        modality: Qt.WindowModal
        standardButtons: Dialogs.StandardButton.Ok
    }
}


