/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.3 as Dialogs
import QtQuick.Controls.Styles 1.4 as Styles
import QtQuick.Controls 1.4 as Controls
import biophor.local 1.0
import Qt.labs.settings 1.0

Window
{
    id: root
    visible: true
    minimumWidth: 750
    minimumHeight: 550
    title: qsTr("Active Directory Contact Book (BETA)")

    property bool appExitConfirmed: false

    Settings {
        id: mainWindowSettings
        category: "MainWindowPosition"
        property alias x: root.x
        property alias y: root.y
        property alias width: root.width
        property alias height: root.height
        property bool maximized: false
    }

	AboutDlg {
        id: aboutDlg
    }

	SettingsDlg {
        id: settingsDlg
    }

    MainWindowViewModel
    {
        id: mainWindowViewModel

        onSearchFinished: {
            startSearching.enabled = true
            stopSearching.enabled = false
            busyIndicator.running = false;

            if (errorOccurred === true) {
                messageDialog.text = qsTr("Error occurred during loading")
                messageDialog.informativeText = mainWindowViewModel.lastError
                messageDialog.icon = Dialogs.StandardIcon.Warning
                messageDialog.open()
            }
        }
    }

    Dialogs.MessageDialog {
        id: exitConfirmationDialog
        title: qsTr("Active Directory Contact Book")
        icon: Dialogs.StandardIcon.Question
        text: qsTr("Do you really want to exit?");
        modality: Qt.WindowModal
        standardButtons: Dialogs.StandardButton.Ok | Dialogs.StandardButton.Cancel

        onAccepted: {
            appExitConfirmed = true
            root.close()
        }
    }
    onClosing: {
        if (appExitConfirmed) {
            mainWindowViewModel.handleWindowClosing()
            mainWindowSettings.maximized = (root.visibility === Window.Maximized)
        }
        else {
            exitConfirmationDialog.open()
            close.accepted = false
        }
    }

    Component.onCompleted:
    {
        if (mainWindowSettings.maximized === true)
        {
            root.width = Screen.desktopAvailableWidth * 0.66
            root.height = Screen.desktopAvailableHeight * 0.66
            root.x = Screen.desktopAvailableWidth / 2 - root.width / 2
            root.y = Screen.desktopAvailableHeight / 2 - root.height / 2;
            root.visibility = Window.Maximized
        }
    }


   	ColumnLayout
    {
        anchors.fill: parent
        anchors.margins: 10

        Controls.Label
        {
            Layout.alignment: Qt.AlignTop
            Layout.fillWidth: true
            font.bold: true
            font.pointSize: 10
            text: qsTr("Click the 'Load' button to load contacts.")
        }
        SearchResultFragment {
            id: searchResultFragment
            Layout.fillHeight: true
            Layout.fillWidth: true
            mainWindowViewModel: mainWindowViewModel
            searchResultModel: mainWindowViewModel.contactsModel
            contactDetailsModel: mainWindowViewModel.contactDetailsModel
            contactPhotoUrn: mainWindowViewModel.contactPhotoUrn

            onSelectionChanged: {
                mainWindowViewModel.handleContactSelected(rowIndex)
            }
        }
        Item {
            Layout.alignment: Qt.AlignTop
            height: 10
        }

        Controls.CheckBox {
            id: useSearchFilters
            Layout.alignment: Qt.AlignTop
            checked: mainWindowViewModel.searchFiltersEnabled

            Binding {
                target: mainWindowViewModel;
                property: "searchFiltersEnabled";
                value: useSearchFilters.checked
                delayed: true
            }
            style: Styles.CheckBoxStyle {
                label: Controls.Label {
                    font.bold: true
                    font.pointSize: 10
                    text: qsTr("Load contacts matching these conditions:")
                }
            }
        }

        FiltersFragment {
            id: filtersFragment
            enabled: useSearchFilters.checked
            Layout.alignment: Qt.AlignTop
            Layout.fillWidth: true
            filterTypesModel: mainWindowViewModel.searchFilterTypesModel
            filterRulesModel: mainWindowViewModel.searchFilterRulesModel
            filtersModel: mainWindowViewModel.searchFiltersModel
            allRules: mainWindowViewModel.allFilteringRulesMustBeMet
            Binding {
                target: mainWindowViewModel;
                property: "allFilteringRulesMustBeMet";
                value: filtersFragment.allRules
                delayed: true
            }
            onAddingFilterRequested: {
                mainWindowViewModel.addNewFilter(filterTypeIndex, filterRuleIndex, filterValue)
            }
            onRemovingFilterRequested: {
                mainWindowViewModel.removeSelectedFilter(selectedFilterIndex)
            }
        }

        RowLayout {
            Layout.margins: 10
            Layout.alignment: Qt.AlignBottom

            Controls.Button {
                id: displayAboutBox
                Layout.minimumWidth: 100
                text: qsTr("About...")
                onClicked: {
                    aboutDlg.open()
                }
            }
            Controls.Button {
                id: displaySettingsDialog
                Layout.minimumWidth: 100
                text: qsTr("Settings...")
                onClicked: {
                    settingsDlg.show()
                }
            }

            Item {
                // empty space between left and right groups of buttons
                Layout.fillWidth: true
            }

            Controls.BusyIndicator {
                id: busyIndicator
                running: false
                Layout.alignment: Qt.AlignRight
            }

            Controls.Button {
                id: startSearching
                Layout.alignment: Qt.AlignRight
                Layout.minimumWidth: 100
                objectName: "startSearching"
                text: qsTr("Load")
                onClicked: {
                    busyIndicator.running = true;
                    if (mainWindowViewModel.startSearching()){
                        enabled = false
                        stopSearching.enabled = true
                        searchResultFragment.clearSelection()
                        searchResultFragment.clearSortingColumn()
                    }
                    else {
                        busyIndicator.running = false;
                        messageDialog.text = qsTr("Failed to load contacts")
                        messageDialog.informativeText = mainWindowViewModel.lastError
                        messageDialog.icon = Dialogs.StandardIcon.Warning
                        messageDialog.open()
                    }
                }
            }

            Controls.Button {
                objectName: "stopSearching"
                Layout.alignment: Qt.AlignRight
                Layout.minimumWidth: 100
                id: stopSearching
                text: qsTr("Stop")
                enabled: false
                onClicked: {
                    mainWindowViewModel.stopSearching()
                    busyIndicator.running = false;
                    enabled = false;
                }
            }

            Controls.Button {
                Layout.alignment: Qt.AlignRight
                Layout.minimumWidth: 100
                id: exitApp
                text: qsTr("Exit...")
                onClicked: {
                    root.close()
                }
            }
        }
	}

    Dialogs.MessageDialog {
        id: messageDialog
        title: qsTr("Active Directory Contact Book")
        modality: Qt.WindowModal
        standardButtons: Dialogs.StandardButton.Ok
    }
}
