﻿/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4 as Controls
import Qt.labs.settings 1.0

RowLayout
{
    Layout.fillWidth: true

    property alias filterTypesModel: filterTypesComboBox.model
    property alias filterRulesModel: filterRulesComboBox.model
    property alias filtersModel: filtersTableView.model
    property alias allRules: allRulesRadioButton.checked

    signal addingFilterRequested(int filterTypeIndex, int filterRuleIndex, string filterValue)
    signal removingFilterRequested(int selectedFilterIndex)

    Connections
    {
        target: addFilterButton

        function onClicked()
        {
            var filterTypeIndex = filterTypesComboBox.currentIndex
            var filterRuleIndex = filterRulesComboBox.currentIndex
            var value = filterValueEditBox.text
            addingFilterRequested(filterTypeIndex, filterRuleIndex, value)

            reevalAddButtonEnableProperty()
            reevalRemoveButtonEnableProperty()
        }
    }

    Connections {
        target: removeSelectedFilterButton

        function onClicked()
        {
            if (filtersTableView.rowCount === 1) {
                removingFilterRequested(0)
            }
            else {
                var row = filtersTableView.currentRow

                if (row >= 0)
                {
                    filtersTableView.selection.deselect(row, row)

                    if (row === (filtersTableView.rowCount - 1))
                    {
                        filtersTableView.selection.select(row - 1, row - 1)
                        removingFilterRequested(row)
                    }
                    else
                    {
                        removingFilterRequested(row)
                        filtersTableView.selection.select(row, row)
                    }
                }
            }
            reevalAddButtonEnableProperty()
            reevalRemoveButtonEnableProperty()
        }
    }


    ColumnLayout
    {
        Layout.fillWidth: true

        RowLayout
        {
            spacing: 10

            Controls.ComboBox {
                id: filterTypesComboBox
                Layout.minimumWidth: 100
                model: filterTypesModel
            }
            Controls.ComboBox {
                id: filterRulesComboBox
                Layout.minimumWidth: 100
                model: filterRulesModel
            }
            Controls.TextField {
                id: filterValueEditBox
                focus: true
                Layout.fillWidth: true
                Layout.minimumWidth: 100
                height: parent.childrenRect.height
                onTextChanged: {
                    addFilterButton.enabled =
                         (text.trim().length > 0) && (filtersModel.rowCount() < filtersModel.maxNumFilters)
                }
            }
        }
        RowLayout {
            Controls.TableView {
                id: filtersTableView
                implicitHeight: 100
                model: filtersModel
                Layout.fillWidth: true
                selectionMode: Controls.SelectionMode.SingleSelection

                Connections {
                    target: filtersTableView.selection

                    function onSelectionChanged() {
                        if (filtersTableView.rowCount == 0){
                            return;
                        }
                        filtersTableView.selection.forEach(
                            function(rowIndex) {
                                removeSelectedFilterButton.enabled = filtersTableView.selection.count > 0
                            }
                        )
                    }
                }
                TableViewColumn { id: filterTypeColumn; title: qsTr("Filter"); role: "FilterType"; movable: false }
                TableViewColumn { id: filterRuleColumn; title: qsTr("Matching rule"); role: "FilterRule"; movable: false }
                TableViewColumn { id: filterValueColumn; title: qsTr("Value"); role: "FilterValue"; movable: false
                    width: filtersTableView.viewport.width - filterRuleColumn.width - filterTypeColumn.width
                }
            }
        }
        Controls.ExclusiveGroup {
            id: rulesRadioGroup
        }
        RowLayout {
            Controls.RadioButton {
                id: allRulesRadioButton
                exclusiveGroup: rulesRadioGroup
                text: qsTr("All the matching rules should be met")
            }
            Controls.RadioButton {
                id: oneRuleRadioButton
                checked: !allRules
                exclusiveGroup: rulesRadioGroup
                text: qsTr("At least one matching rule should be met")
            }
        }
    }

    ColumnLayout {
        Layout.alignment: Qt.AlignTop
        spacing: 5
        Controls.Button {
            id: addFilterButton
            Layout.minimumWidth: 100
            text: qsTr("Add")
            enabled: false
        }
        Controls.Button {
            id: removeSelectedFilterButton
            Layout.minimumWidth: 100
            text: qsTr("Remove")
            enabled: false
        }
    }

    Settings {
        category: "FiltersTableColumnSizes"
        property alias filterTypeColumnWidth: filterTypeColumn.width
        property alias filterRuleColumnWidth: filterRuleColumn.width
        property alias filterValueColumnWidth: filterValueColumn.width
    }

    function reevalAddButtonEnableProperty()
    {
        addFilterButton.enabled = (filterValueEditBox.text.trim().length > 0) &&
                                  (filtersModel.rowCount() < filtersModel.maxNumFilters)
    }

    function reevalRemoveButtonEnableProperty()
    {
        if ((filtersModel.rowCount() === 0) || (filtersTableView.currentRow < 0) ) {
            removeSelectedFilterButton.enabled = false
        }
        else {
            removeSelectedFilterButton.enabled = true
        }
    }
}

