/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "AdBookBL/export_for_unit_tests.h"
using namespace adbook;

std::vector<AdPersonDesc> CreateAdPersonList( const std::wstring & prefix = L"", size_t num = 8 )
{
    std::vector<AdPersonDesc> ret;
    for (size_t i = 0; i < num; ++i)
    {
        AdPersonDesc adp;
        auto & attrTraits = Attributes::GetInstance();
        auto attrIds = attrTraits.GetAttrIds();
        for (auto id : attrIds)
        {
            if (id == Attributes::CommonName)
            {
                adp.SetStringAttr( L"cn", std::to_wstring( i ) );
                continue;
            }
            if (attrTraits.IsString( id ))
            {
                const std::wstring stringAttrValue = std::wstring( L"attr value " ) + prefix + std::to_wstring( id );
                adp.SetStringAttr( attrTraits.GetLdapAttrNamePtr( id ), stringAttrValue );
            }
            else
            {
                BinaryAttrVal binaryAttrValue{ id };
                adp.SetBinaryAttr( attrTraits.GetLdapAttrNamePtr( id ), binaryAttrValue );
            }
        }
        ret.push_back( adp );
    }
    return ret;
}

TEST( AdPersonDescSqliteKeeperTests, Saving_and_loading_using_named_configuration )
{
    // Arrange
    AdPersonDescSqliteKeeper sut;
    sut.SetInternalNamePrefix( L"unit_tests" );

    ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( L"LDAP://localhost:55555/CN=fake_users,DC=testdata,DC=adbook,DC=local" );
    // internal db file name depends on connection params
    sut.SetNameByConnectionParams( connectionParams );

    std::vector<AdPersonDesc> adpList = CreateAdPersonList();
    std::vector<AdPersonDesc> adpListCopy = adpList;

    // Act
    std::vector<AdPersonDesc> emptyList;
    sut.Save( emptyList );
    sut.Load( emptyList );

    sut.Save( adpList );
    adpList.clear();
    sut.Load( adpList );

    // Assert
    ASSERT_TRUE( emptyList.empty() );
    ASSERT_EQ( adpList, adpListCopy );
}

TEST( AdPersonDescSqliteKeeperTests, Saving_and_loading_using_default_configuration )
{
    // Arrange
    AdPersonDescSqliteKeeper sut;
    sut.SetInternalNamePrefix( L"unit_tests" );
    std::vector<AdPersonDesc> adpList = CreateAdPersonList();
    std::vector<AdPersonDesc> adpListCopy = adpList;

    // Act
    std::vector<AdPersonDesc> emptyList;
    sut.Save( emptyList );
    sut.Load( emptyList );

    sut.Save( adpList );
    adpList.clear();
    sut.Load( adpList );

    // Assert
    ASSERT_TRUE( emptyList.empty() );
    ASSERT_EQ( adpList, adpListCopy );
}

TEST( AdPersonDescSqliteKeeperTests, Managing_several_configurations_simultaneously )
{
    // Arrange
    AdPersonDescSqliteKeeper sut;
    sut.SetInternalNamePrefix( L"unit_tests" );

    ConnectionParams connectionParams1;
    connectionParams1.Set_ConnectDomainYouLoggedIn( false );
    connectionParams1.Set_ConnectAsCurrentUser( true );
    connectionParams1.SetAddress( L"LDAP://localhost:55555/CN=fake_users1,DC=testdata,DC=adbook,DC=local" );

    ConnectionParams connectionParams2;
    connectionParams2.Set_ConnectDomainYouLoggedIn( true );
    connectionParams2.Set_ConnectAsCurrentUser( false );
    connectionParams2.SetAddress( L"LDAP://localhost:55555/CN=fake_users2,DC=testdata,DC=adbook,DC=local" );

    // Act
    //  Save config 1
    sut.SetNameByConnectionParams( connectionParams1 );
    std::vector<AdPersonDesc> emptyList;
    sut.Save( emptyList );
    sut.Load( emptyList );
    std::vector<AdPersonDesc> adpList1 = CreateAdPersonList( L"config1" );
    std::vector<AdPersonDesc> adpList1Copy = adpList1;
    sut.Save( adpList1 );

    //  Save config 2
    sut.SetNameByConnectionParams( connectionParams2 );
    sut.Save( emptyList );
    sut.Load( emptyList );
    std::vector<AdPersonDesc> adpList2 = CreateAdPersonList( L"config2" );
    std::vector<AdPersonDesc> adpList2Copy = adpList2;
    sut.Save( adpList2 );

    // Load config 1
    sut.SetNameByConnectionParams( connectionParams1 );
    adpList1.clear();
    sut.Load( adpList1 );

    // Load config 2
    sut.SetNameByConnectionParams( connectionParams2 );
    adpList2.clear();
    sut.Load( adpList2 );

    // Assert
    ASSERT_EQ( adpList1, adpList1Copy );
    ASSERT_EQ( adpList2, adpList2Copy );
}


