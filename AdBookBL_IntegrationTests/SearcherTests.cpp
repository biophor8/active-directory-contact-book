/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "AdBookBL/export_for_unit_tests.h"
#include "TestSuiteBase.h"

class SearcherTests : public TestSuiteBase
{
public:
    void SetUp() override {
        CoInitialize( NULL );
    }
    void TearDown() override {
        CoUninitialize();
    }
};

TEST_F( SearcherTests, Searching_for_existing_person )
{
    // Arrange
    adbook::Searcher sut;

    adbook::LdapRequestBuilder requestBuilder;
    requestBuilder.AddObjectCategoryRule();
    requestBuilder.AddRule( L"cn", adbook::LdapRequestBuilder::ExactMatch, L"Ramon Linden" );
    requestBuilder.AddAND();

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestAdLdsInstance() );

    bool startedCallbackCalled = false;
    bool finishedCallbackCalled = false;
    bool itemFoundCallbackCalled = false;
    adbook::AdPersonDesc person;
    sut.SetCallbacks(
        [&itemFoundCallbackCalled, &person]( adbook::AdPersonDesc && rp ) {
            itemFoundCallbackCalled = true; person = std::move( rp );
        },
        [&startedCallbackCalled]() {startedCallbackCalled = true; },
        [&finishedCallbackCalled]() {finishedCallbackCalled = true; }
    );

    // Act
    sut.Start( requestBuilder.Get(), connectionParams );
    sut.Wait();

    // Assert
    ASSERT_NO_THROW( sut.PropogateSearchException() );
    ASSERT_TRUE( person.GetStringAttr( L"cn" ) == L"Ramon Linden" );
    ASSERT_TRUE( startedCallbackCalled );
    ASSERT_TRUE( finishedCallbackCalled );
    ASSERT_TRUE( itemFoundCallbackCalled );
}

TEST_F( SearcherTests, Enumerating_all_the_people_in_db )
{
    // Arrange
    adbook::Searcher sut;

    adbook::LdapRequestBuilder requestBuilder;
    requestBuilder.AddObjectCategoryRule();

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestAdLdsInstance() );

    bool startedCallbackCalled = false;
    bool finishedCallbackCalled = false;
    bool itemFoundCallbackCalled = false;
    std::vector<adbook::AdPersonDesc> people;

    sut.SetCallbacks(
        [&itemFoundCallbackCalled, &people]( adbook::AdPersonDesc && rp ) {
            itemFoundCallbackCalled = true; people.push_back( std::move( rp ) );
        },
        [&startedCallbackCalled]() {startedCallbackCalled = true; },
        [&finishedCallbackCalled]() {finishedCallbackCalled = true; }
    );

    // Act
    sut.Start( requestBuilder.Get(), connectionParams );
    sut.Wait();

    // Assert
    ASSERT_NO_THROW( sut.PropogateSearchException() );
    ASSERT_EQ( people.size(), 5 );    // test adlds instance contains 5 people
    ASSERT_TRUE( startedCallbackCalled );
    ASSERT_TRUE( finishedCallbackCalled );
    ASSERT_TRUE( itemFoundCallbackCalled );
}

TEST_F( SearcherTests, Searching_must_not_throw_exception_when_specified_people_could_not_be_found )
{
    // Arrange
    adbook::Searcher sut;

    adbook::LdapRequestBuilder requestBuilder;
    requestBuilder.AddObjectCategoryRule();
    requestBuilder.AddRule( L"cn", adbook::LdapRequestBuilder::ExactMatch, L"Invalid Name" );
    requestBuilder.AddAND();

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestAdLdsInstance() );

    bool startedCallbackCalled = false;
    bool finishedCallbackCalled = false;
    bool itemFoundCallbackCalled = false;

    sut.SetCallbacks(
        [&itemFoundCallbackCalled]( adbook::AdPersonDesc && ) {
            itemFoundCallbackCalled = true;
        },
        [&startedCallbackCalled]() {startedCallbackCalled = true; },
        [&finishedCallbackCalled]() {finishedCallbackCalled = true; }
    );

    // Act
    sut.Start( requestBuilder.Get(), connectionParams );
    sut.Wait();

    // Assert
    ASSERT_NO_THROW( sut.PropogateSearchException() );
    ASSERT_TRUE( startedCallbackCalled );
    ASSERT_TRUE( finishedCallbackCalled );
    ASSERT_FALSE( itemFoundCallbackCalled );
}

TEST_F( SearcherTests, Must_throw_exception_when_ldap_filter_has_invalid_format )
{
    // Arrange
    adbook::Searcher sut;

    std::wstring invalidLdapRequest = L"invalid ldap request";

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestAdLdsInstance() );

    // Act
    sut.Start( invalidLdapRequest, connectionParams );
    sut.Wait();

    // Assert
    ASSERT_THROW( sut.PropogateSearchException(), adbook::HrError );
}

