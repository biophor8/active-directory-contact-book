/*
Copyright (C) Andrei Goncharov.

This file is part of the 'Active Directory Contact Book'.
'Active Directory Contact Book' is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Active Directory Contact Book' is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with
'Active Directory Contact Book'. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gtest/gtest.h"
#include "AdBookBL/export_for_unit_tests.h"
#include "TestSuiteBase.h"

class ConnectorTests : public TestSuiteBase
{
public:
    void SetUp() override {
        CoInitialize( NULL );
    }
    void TearDown() override {
        CoUninitialize();
    }
};

TEST_F( ConnectorTests, Successful_connection_when_AdLdsInstance_is_accessible )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestAdLdsInstance() );

    // Act
    sut.Connect( connectionParams );

    // Assert
    ASSERT_TRUE( sut.IsConnected() );
    sut.Disconnect();
    ASSERT_FALSE( sut.IsConnected() );
}

TEST_F( ConnectorTests, Connection_failure_when_AdLdsInstance_is_inaccessible )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    std::wstring invalidLdapUrl = L"LDAP://localhost:44444/CN=users,DC=testdata,DC=adbook,DC=local";
    connectionParams.SetAddress( invalidLdapUrl );

    // Act & Assert
    ASSERT_THROW( sut.Connect( connectionParams ), adbook::HrError );
}

TEST_F( ConnectorTests, Downloading_string_attribute_using_valid_name )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestAdLdsInstance() );
    sut.Connect( connectionParams );

    // Act
    std::wstring cnAttrValue = sut.DownloadStringAttr( L"cn" );

    // Assert
    ASSERT_EQ( cnAttrValue, L"users" );
}

TEST_F( ConnectorTests, Downloading_fails_when_string_attribute_name_is_invalid )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestAdLdsInstance() );
    sut.Connect( connectionParams );

    // Act & Assert
    ASSERT_THROW( sut.DownloadStringAttr( L"invalidname" ), adbook::HrError );
}

TEST_F( ConnectorTests, Downloading_returns_empty_string_when_valid_string_attribute_has_no_value )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestAdLdsInstance() );
    sut.Connect( connectionParams );

    // Act
    std::wstring descriptionAttrValue = sut.DownloadStringAttr( L"description" );

    // Assert
    ASSERT_EQ( descriptionAttrValue.length(), 0 );
}

TEST_F( ConnectorTests, Uploading_string_attribute_value_using_valid_name )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance() );
    sut.Connect( connectionParams );

    // Act
    sut.UploadStringAttr( L"comment", L"" );
    std::wstring emptyCommentValue = sut.DownloadStringAttr( L"comment" );

    wchar_t commentValue[] = L"This is a comment";
    sut.UploadStringAttr( L"comment", commentValue );

    // Assert
    ASSERT_EQ( emptyCommentValue.length(), 0 );
    ASSERT_EQ( sut.DownloadStringAttr( L"comment" ),  commentValue );
}

TEST_F( ConnectorTests, Uploading_fails_when_string_attribute_name_is_invalid )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance() );
    sut.Connect( connectionParams );

    wchar_t commentValue[] = L"This is a comment";

    // Act & Assert

    ASSERT_THROW( sut.UploadStringAttr( L"invalidname", commentValue ), adbook::HrError );
}

TEST_F( ConnectorTests, Downloading_binary_attribute_using_valid_name )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance() );
    sut.Connect( connectionParams );

    // Act
    // this attribute contains predefined data. See the project 'AdBookBL_IntegrationTestsPrepareEnvironment'
    adbook::BinaryAttrVal value = sut.DownloadBinaryAttr( L"thumbnailPhoto" );

    // Assert
    ASSERT_EQ( value.size(), 1335 );
    ASSERT_EQ( value[0], 0xFF );
    ASSERT_EQ( value[1], 0xD8 );
    ASSERT_EQ( value[2], 0xFF );
}

TEST_F( ConnectorTests, Downloading_fails_when_binary_attribute_name_is_invalid )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance() );
    sut.Connect( connectionParams );

    // Act & Assert
    // this attribute contains predefined data. See the project 'AdBookBL_IntegrationTestsPrepareEnvironment'
    ASSERT_THROW( sut.DownloadBinaryAttr( L"invalidname" ), adbook::HrError );
}

TEST_F( ConnectorTests, Uploading_binary_attribute_using_valid_name )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance() );
    sut.Connect( connectionParams );

    // Act
    sut.UploadBinaryAttr( L"photo", adbook::BinaryAttrVal{} );
    adbook::BinaryAttrVal emptyTestData = sut.DownloadBinaryAttr( L"photo" );
    adbook::BinaryAttrVal testData = { 0x1,0x2,0x3 };
    sut.UploadBinaryAttr( L"photo", testData );

    // Assert
    ASSERT_EQ( sut.DownloadBinaryAttr( L"photo" ), testData );
    ASSERT_TRUE( emptyTestData.empty() );
}

TEST_F( ConnectorTests, Uploading_fails_when_binary_attribute_name_is_invalid )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance() );
    sut.Connect( connectionParams );

    // Act & Assert
    adbook::BinaryAttrVal testData = { 0x1,0x2,0x3 };
    ASSERT_THROW( sut.UploadBinaryAttr( L"invalidname", testData ), adbook::HrError );
}

TEST_F( ConnectorTests, Renaming_existing_object )
{
    // Arrange
    adbook::Connector sut;

    adbook::ConnectionParams connectionParams;
    connectionParams.Set_ConnectDomainYouLoggedIn( false );
    connectionParams.Set_ConnectAsCurrentUser( true );
    connectionParams.SetAddress( GetLdapUrlForTestPersonOnAdLdsInstance() );
    sut.Connect( connectionParams );

    // Act
    std::wstring currentCN = sut.DownloadStringAttr( L"cn" );
    sut.Rename( L"Kristie Etherton XYZ" );
    std::wstring newCN = sut.DownloadStringAttr( L"cn" );
    sut.Rename( currentCN );

    // Assert
    ASSERT_EQ( currentCN, L"Kristie Etherton" );
    ASSERT_EQ( newCN, L"Kristie Etherton XYZ" );
}

